const path = require('path');
const fs = require('fs');

module.exports = {
    packagerConfig: {
        icon: 'D:\\Knight Vision\\electron-app\\assets\\Knight_Vision_Icon.ico',
        ignore: [/\/python/],
    },
    rebuildConfig: {},
    makers: [
        {
            name: '@electron-forge/maker-squirrel',
            config: {
                name: 'knight_vision',
                iconUrl:
                    'D:\\Knight Vision\\electron-app\\assets\\Knight_Vision_Icon.ico', // Replace with your app's icon path
                setupIcon:
                    'D:\\Knight Vision\\electron-app\\assets\\Knight_Vision_Icon.ico', // Replace with your app's icon path
                platforms: ['win32'],
            },
        },
        {
            name: '@electron-forge/maker-zip',
            platforms: ['darwin'],
        },
        {
            name: '@electron-forge/maker-deb',
            config: {},
        },
    ],
    hooks: {
        packageAfterCopy: async (config, buildPath, electronVersion, platform, arch) => {
            var src = path.join(__dirname, '../React-App/build/');
            console.log('building for platform: ', platform);
            var pydist = path.join(__dirname, `./pydist/`);
            var dst = buildPath;
            fs.cpSync(src, dst, { recursive: true });
            fs.cpSync(pydist, path.join(dst, './pydist/'), { recursive: true });
        },
    },
};
