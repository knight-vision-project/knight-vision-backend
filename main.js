const { app, BrowserWindow } = require('electron');
const path = require('path');
const fs = require('fs');
const socketIOClient = require('socket.io-client');
const { dialog } = require('electron');
let options = {
    properties: ['openDirectory'],
};
const { ipcMain, shell } = require('electron');
// Create a write stream to a log file
const logStream = fs.createWriteStream(path.join(app.getPath('userData'), 'log.txt'), {
    flags: 'a',
});
console.log('Logging outputs to: ', logStream.path);
let currWin;

// Global variables
var mainWindow = null;
var pyProcess;

const connectSIO = () => {
    console.log('main.js test');
    socket = socketIOClient('http://localhost:56789', {
        transports: ['websocket'],
    });
    socket.on('connect_error', (error) => {
        console.error('Connection error:', error);
        setTimeout(() => {
            socket.connect();
        }, 5000);
    });

    socket.on('disconnect', () => {
        console.log('Disconnected from server main.js');
    });
    socket.on('connect', () => {
        socket.emit('join_room', 'jsroom');
        console.log('main.js connected');
        socket.on('folder', function (data) {
            dialog
                .showOpenDialog(currWin, options)
                .then((result) => {
                    if (!result.canceled) {
                        socket.emit('message_rerouter', {
                            target: 'pyroom',
                            handler: 'export_table',
                            data: [result.filePaths[0], data],
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        });
        socket.on('export_folder', function (data) {
            //   console.log(config);
            dialog
                .showOpenDialog(currWin, options)
                .then((result) => {
                    if (!result.canceled) {
                        socket.emit('message_rerouter', {
                            target: 'pyroom',
                            handler: 'export_config',
                            data: [result.filePaths[0], data[0], data[1]],
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        });
        socket.on('flash_folder', function (data) {
            //   console.log(config);
            dialog
                .showOpenDialog(currWin, options)
                .then((result) => {
                    if (!result.canceled) {
                        socket.emit('message_rerouter', {
                            target: 'pyroom',
                            handler: 'flash_extraction',
                            data: result.filePaths[0],
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        });
        socket.on('warranty_export', function (data) {
            //   console.log(config);
            dialog
                .showOpenDialog(currWin, options)
                .then((result) => {
                    if (!result.canceled) {
                        socket.emit('message_rerouter', {
                            target: 'pyroom',
                            handler: 'export_warranty',
                            data: result.filePaths[0],
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        });
        socket.on('template_export', function (data) {
            //   console.log(config);
            dialog
                .showOpenDialog(currWin, options)
                .then((result) => {
                    if (!result.canceled) {
                        socket.emit('message_rerouter', {
                            target: 'pyroom',
                            handler: 'export_template',
                            data: result.filePaths[0],
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        });
        socket.on('trace_conv', function (data) {
            // console.log(data);
            dialog
                .showOpenDialog(currWin, options)
                .then((result) => {
                    if (!result.canceled) {
                        socket.emit('message_rerouter', {
                            target: 'pyroom',
                            handler: 'convert_trace_to_csv',
                            data: [result.filePaths[0], data],
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        });
        socket.on('advanced_folder', function (data) {
            dialog
                .showOpenDialog(currWin, options)
                .then((result) => {
                    if (!result.canceled) {
                        socket.emit('message_rerouter', {
                            target: 'pyroom',
                            handler: 'export_adv_table',
                            data: [result.filePaths[0], data],
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                });
        });
        socket.on('Logging', function (data) {
            if (data[1] == 'Start') {
                dialog
                    .showOpenDialog(currWin, options)
                    .then((result) => {
                        if (!result.canceled) {
                            socket.emit('message_rerouter', {
                                target: 'pyroom',
                                handler: 'logging',
                                data: [result.filePaths[0], data[0], data[1]],
                            });
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            } else {
                socket.emit('message_rerouter', {
                    target: 'pyroom',
                    handler: 'logging',
                    data: ['result.filePaths[0]', data[0], data[1]],
                });
            }
        });
    });
};

// Create new browser window
const createWindow = () => {
    mainWindow = new BrowserWindow({
        frame: true,
        show: true,
        movable: true,
        darkTheme: true,
        thickFrame: true,
        resizable: true,
        autoHideMenuBar: true,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            //   devTools: false,
        },
    });
    mainWindow.maximize();
    mainWindow.setMenuBarVisibility(false);
    // mainWindow.on('ready-to-show', () => {
    //     mainWindow.show();
    //     // 删除窗口的菜单栏。(Remove the window's menu bar.)
    //     mainWindow.removeMenu();
    // });
    //   mainWindow.setResizable(false);

    if (app.isPackaged) {
        // Redirect stdout messages to the log file
        process.stderr.write = logStream.write.bind(logStream);
        mainWindow.loadFile('index.html');
        // mainWindow.setIcon('./assets/White_Knight.ico');
    } else {
        // mainWindow.webContents.openDevTools();
        mainWindow.loadURL('http://localhost:3000/dashboard/default');
    }
};

const createChildProcess = () => {
    console.log('started the child process');
    const options = { stdio: ['ignore', logStream, logStream] };
    if (app.isPackaged) {
        var main;
        if (process.platform == 'win32') {
            main = 'pydist/main.exe';
        } else if (process.platform == 'linux') {
            main = 'pydist/main';
        }
        const script = path.join(__dirname, main);
        pyProcess = require('child_process').spawn(script, options);
    } else {
        console.log('# Starting dev instance...');
        const script = path.join(__dirname, 'python', 'main.py');
        pyProcess = require('child_process').spawn('python', [script], {
            stdio: 'inherit',
        });
    }
    setTimeout(() => {
        connectSIO();
    }, 5000);
};

const additionalData = { myKey: 'myValue' };
const gotTheLock = app.requestSingleInstanceLock(additionalData);

if (!gotTheLock) {
    app.quit();
} else {
    app.on('second-instance', (event, commandLine, workingDirectory, additionalData) => {
        // Someone tried to run a second instance, we should focus our window.
        if (mainWindow) {
            if (mainWindow.isMinimized()) mainWindow.restore();
            mainWindow.focus();
        }
    });

    app.whenReady().then(() => {
        createWindow();
        app.on('activate', () => {
            // for Mac
            if (BrowserWindow.getAllWindows().length === 0) createWindow();
        });
        setTimeout(() => {
            createChildProcess();
        }, 1000);
    });
}

ipcMain.on('open-external', (event, url) => {
    shell.openExternal(url);
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit();
    if (pyProcess) pyProcess.kill();
});

app.on('before-quit', () => {
    if (pyProcess) {
        console.log('killing child process');
        pyProcess.kill('SIGKILL');
        pyProcess.kill();
    }
});

if (require('electron-squirrel-startup')) app.quit();
