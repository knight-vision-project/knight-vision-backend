import sys
import sqlite3
from cx_Freeze import setup, Executable

conn = sqlite3.connect("../data.db")
curr = conn.cursor()
curr.execute("""
    CREATE TABLE IF NOT EXISTS req_data (
        id TEXT PRIMARY KEY,
        data TEXT
    )
""")
curr.execute(
    """
        INSERT OR REPLACE INTO req_data (id, data)
        VALUES (?, ?)
    """,
    ("auth_status", "No Access"),
)
conn.commit()
curr.close()
conn.close()
# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {
    "build_exe": "../pydist",  # Location to create executable
    "excludes": ["tkinter", "unittest"],
    "zip_include_packages": ["encodings", "PySide6"],
    "packages": [
        # "aiohttp",
        # "websocket_server",
        "socketio",
        "engineio",
        "nest_asyncio",
        "asyncio",
        "can.interfaces.pcan",
        "can.interfaces.canalystii",
        "engineio",
        "socketio",
        "threading",
        "time",
        "queue",
    ],
}

# base="Win32GUI" should be used only for Windows GUI app
base = "Win32GUI" if sys.platform == "win32" else None
if sys.platform == "win32":
    build_exe_options["include_files"] = [
        (sys.executable, "python3.dll"),
    ]

setup(
    name="guifoo",
    version="0.1",
    description="Websocket for Knight Vision",
    options={
        "build_exe": build_exe_options,
    },
    executables=[Executable("main.py", base=base)],
)
