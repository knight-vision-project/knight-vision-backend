from threading import Thread, Event
from aiohttp import web
from utils.common_utils import sio, app
import can_interaction.upstream_event_handler as handler


@sio.event
def connect(sid, environ):
    print("connected: ", sid)


@sio.event
async def message(sid, data):
    print("message: ", data, sid)
    await sio.emit("my_message", f"Your message was: {data}", room=sid)


@sio.event
def disconnect(sid):
    print("disconnected: ", sid)


@sio.event
def get_config(sid, data):
    print("get_config requested: ", sid, data)
    handler.handle_get_config()


stop_thread = Event()


@sio.event
def logging(sid, data):
    print("logging state changed", sid, data)
    global stop_thread
    if data[1] == "start":
        thread = Thread(
            target=handler.handle_logging, args=(data[0], stop_thread, data[2])
        )
        stop_thread.clear()
        thread.start()
    elif data[1] == "stop":
        stop_thread.set()


@sio.event
def set_config(sid, data):
    print("set_config requested: ", sid, data)
    handler.handle_set_config(data)


@sio.event
def set_warranty_config(sid, data):
    print("set_config requested: ", sid, data)
    handler.handle_set_waranty_config(data)


@sio.event
def get_warranty_config(sid, data):
    print("get_warranty_config requested: ", sid, data)
    handler.handle_get_warranty_config()


@sio.event
def get_chemistry_config(sid, data):
    print("get_chemistry_config requested: ", sid, data)
    handler.handle_chemistry_get_config(data)


@sio.event
def set_chemistry_config(sid, data):
    print("set_chemistry_config requested: ", sid, data)
    handler.handle_chemistry_set_config(data)


@sio.event
def soft_reset(sid, data):
    print("soft reset requested: ", sid, data)
    handler.handle_soft_reset()


@sio.event
def rtc_sync(sid, data):
    print("RTC sync requested: ", sid, data)
    handler.handle_rtc_sync()


@sio.event
def read_firmware_file(sid, data):
    print("firmware sent requested: ", sid, data)
    handler.handle_read_firmware_file(data)


def start_async_server(host="localhost", port=56789) -> None:
    web.run_app(app, host=host, port=port)


if __name__ == "__main__":
    web.run_app(app, port=56789)
