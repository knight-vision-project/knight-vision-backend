import json
import requests


def download_from_airtable() -> dict | None:
    """Downloads schema from Airtable and converts it to JSON

    Returns:
        dict|None: Schema return
    """
    # Airtable configuration
    AIRTABLE_API_KEY = "patVLGxqA6sv0Uqlt.ef9e3d77deac32c6e4d17c12c5e3889d20559ad4336dbf0de7aad41703c4295a"
    AIRTABLE_BASE_ID = "appZE96vAbSi2U9gF"
    AIRTABLE_TABLE_NAME = "JSON Export"
    RECORD_NAME = "JSON Exported"  # Value in the 'Name' field

    # Airtable API URL
    airtable_url = (
        f"https://api.airtable.com/v0/{AIRTABLE_BASE_ID}/{AIRTABLE_TABLE_NAME}"
    )

    # Set up headers with the API key
    headers = {
        "Authorization": f"Bearer {AIRTABLE_API_KEY}",
        "Content-Type": "application/json",
    }

    # Make a request to find the record with the specified name
    params = {"filterByFormula": f"{{Name}}='{RECORD_NAME}'"}
    response = requests.get(airtable_url, headers=headers, params=params)

    if response.status_code == 200:
        records: list[dict[str, dict]] = response.json().get("records", [])
        if records:
            # Get the contents of the 'Cont' field from the record
            json_data_from_airtable = json.loads(
                records[0]["fields"].get("JSON Exporter", "{}")
            )

            print("Downloaded from Airtable.")
            # print(json_data_from_airtable)
            return json_data_from_airtable
        else:
            print(f"Error: Record with name '{RECORD_NAME}' not found in Airtable.")
            return None
    else:
        print(
            f"Failed to fetch data from Airtable. Status code: {response.status_code}, Response: {response.json()}"
        )
        return None
