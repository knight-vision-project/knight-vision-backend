import can
import time
from intelhex import IntelHex
from ctypes import c_ubyte
from utils.common_utils import config_queue, sio, usb_can_a
from utils.can_singleton import CanBusSingleton


class FirmwareUpdate:
    def __init__(self) -> None:
        self.arbitration_id = 0x701
        self.timeout = 2
        self.bus = CanBusSingleton.get_instance()
        self.ih = IntelHex()
        self.mainSeq = 1
        self.per = 0
        self.imgLen = 0
        self.time = time.time()

    messages = {
        0x03: "> Entered App Programming Mode",
        0xA0: "> Erasing Flash Completed",
        0xA1: "> Flash Info Acknowledged",
        0xA5: "> Verifying App Image Successful",
    }

    def message_broadcaster(self, data: str) -> None:
        """Sends a message to frontend regarding firmware update status info

        Args:
            data (str): Message to be sent
        """
        sio.emit(
            "message_rerouter",
            {"target": "jsroom", "handler": "firmware_status_info", "data": data},
        )

    def read_file(self, data_len: int, path: str, type: str) -> None:
        """Reads the firmware file."""
        if data_len > 1024 * 512 and type[-3:] == "bin":
            self.message_broadcaster("> File is too large.")
            return
        else:
            self.message_broadcaster(f"> Received file of size {data_len // 1024} kb.")
        try:
            self.message_broadcaster("> Reading FW file")
            if type[-3:] == "bin":
                self.ih = IntelHex()
                bin = open(path, "rb").read()
                self.ih.frombytes(bin)
            else:
                self.ih = IntelHex(path)
        except:  # noqa: E722
            self.message_broadcaster("> Invalid File")

    def message_sender(self, arbitration_id: int, data: list[int]) -> None:
        """Sends message on bus witgh given parameters"""
        message = can.Message(
            is_extended_id=False,
            arbitration_id=arbitration_id,
            dlc=8,
            data=(c_ubyte * 8)(*data),
        )
        try:
            self.bus.send(message)
        except Exception as e:
            self.message_broadcaster(f"> Error sending firmware message: {e}")
        if usb_can_a.is_set():
            time.sleep(0.005)

    def read_message(self) -> can.Message | None:
        """Return message from config queue. None in case of error."""
        try:
            msg = config_queue.get(timeout=1)
            return msg
        except Exception:
            return None

    def request_sender(self, msg_data: int) -> bool | can.Message:
        """Sends various messages to BMS."""
        if msg_data == 0xA1:
            self.message_sender(
                0x701, [msg_data, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00]
            )
            self.data_length_sender()
        else:
            self.message_sender(
                0x701, [msg_data, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
            )
        return self.confirm_response(msg_data)

    def confirm_response(
        self, msg_data: int = 0x00, return_conf: bool = False, id: int = 0x710
    ) -> bool | can.Message:
        """Waits for confirmation from CAN."""
        start_time = time.time()
        while (time.time() - start_time) < self.timeout:
            message = self.read_message()
            if not message:
                continue
            if message.arbitration_id == id and message.data[0] == msg_data:
                if msg_data in self.messages:
                    self.message_broadcaster(self.messages[msg_data])
                return message
            if message.arbitration_id == id and return_conf is True:
                return message
        self.message_broadcaster(
            f"> No response from CAN while confirming response {msg_data}."
        )
        return False

    def data_length_sender(self) -> None:
        """Sends the length of data to BMS."""
        imgLen = self.ih.segments()[-1][1] - self.ih.segments()[0][0]
        self.imgLen = imgLen
        self.message_sender(
            0x702,
            [
                0x00,
                0x00,
                0xC0,
                0x00,
                ((imgLen >> 24) & 0xFF),
                ((imgLen >> 16) & 0xFF),
                ((imgLen >> 8) & 0xFF),
                (imgLen & 0xFF),
            ],
        )

    def data_sender(
        self,
    ) -> bool:
        """Sends firmware data to BMS."""
        for segment in self.ih.segments():
            start_addr, end_addr = segment
            response = self.data_shaper(
                start_addr,
                end_addr,
            )
            if not response:
                return False
        return True

    def data_shaper(
        self,
        strt_addr: int,
        end_addr: int,
    ) -> bool:
        """Shapes the data into segments to send to BMS

        Args:
            strt_addr (int): Starting address
            end_addr (int): Ending address

        Returns:
            bool: Success or failure
        """
        for start_addr in range(strt_addr, end_addr, 256):
            data = []
            if start_addr + 256 < end_addr:
                data = self.ih.tobinarray(start=start_addr, size=256)
            else:
                leng = end_addr - start_addr
                data = self.ih.tobinarray(start=start_addr, size=leng)
                pad = 16 - (leng % 16)
                leng = leng + pad
                for i in range(0, pad):
                    data.append(0xFF)
            dataLen = len(data)

            self.message_sender(
                0x701,
                [
                    0xA4,
                    (dataLen >> 8) & 0xFF,
                    dataLen & 0xFF,
                    0x00,
                    0x00,
                    0x00,
                    0x00,
                    0x00,
                ],
            )
            response = self.segment_sender(
                dataLen,
                data,
            )
            if not response:
                return response
        return True

    def segment_sender(
        self,
        dataLen: int,
        data: list[int],
    ) -> bool:
        """Sends a shaped segment to BMS

        Args:
            dataLen (int): Length of the segment
            data (list[int]): Data of the segment

        Returns:
            bool: Success or failure
        """
        seq = 0
        for i in range(0, dataLen, 7):
            byteArr = bytearray([seq])
            K = 7
            if i + 7 > dataLen:
                K = dataLen - i
            for j in range(0, K):
                byteArr.append(data[seq * 7 + j])
            self.message_sender(0x702, byteArr)
            seq += 1

        res = self.confirm_response(0xA4)
        if res:
            dat = self.confirm_response(return_conf=True, id=0x711)
            if not dat:
                return False
            if ((((dat.data[1]) << 8) & 0xFF00) + dat.data[2]) == self.mainSeq:
                self.mainSeq = self.mainSeq + 1
                self.per = min([self.per + (dataLen / self.imgLen) * 100, 100])
                if time.time() - self.time > 1 or self.per == 100:
                    self.time = time.time()
                    sio.emit(
                        "message_rerouter",
                        {
                            "target": "jsroom",
                            "handler": "firmware_progress",
                            "data": f"{self.per:3.0f}",
                        },
                    )
                # print(f"Firmware update in progress {self.per:3.2f} %", end="\r")
                return True
            else:
                print(
                    "error cause",
                    ((((dat.data[1]) << 8) & 0xFF00) + dat.data[2]),
                    self.mainSeq,
                )
        else:
            return False
