import csv
import can
import time
import math
import json
import pytz
import sqlite3
from ctypes import c_ubyte
from datetime import datetime
from collections import deque
from utils.common_utils import config_queue, is_bundled, sio
from utils.can_singleton import CanBusSingleton

PACKET_COUNT = 0


class FlashExtraction:
    """Handles the entire process of extracting flash"""

    def __init__(self) -> None:
        self.timestamp_str = datetime.now().strftime("%Y%m%d%H%M%S")
        self.bus = CanBusSingleton.get_instance()
        self.can_queue = deque([])
        self.data_dict = {}
        self.csv_dict = {}
        self.path = ""
        self.sio = sio
        self.config_queue = config_queue
        self.time = datetime.now()
        self.buffer = deque([])
        self.lines = 0
        self.cell_temp_itr = 0
        self.cell_volt_itr = 0
        self.cell_volt_array = []
        self.cell_temp_array = []
        self.id_tracker = {}
        self.columns_to_delete = []

    def cal_checksum(self, Arr, len):
        """
        Calculates the checksum of the array.

        Args:
            Arr (list): Array in which checksum has to be calculated.
            len (int): length

        Returns:
            str: The found checksum.
        """
        global PACKET_COUNT
        Sum = 0
        Checksum = 0
        for i in range(0, len):
            Sum = Sum + int(Arr[i], base=16)
        Checksum = Sum ^ 0xFF
        Checksum = Checksum + 1
        Checksum = Checksum & 0xFF
        Checksum = (str(hex(Checksum))).split("x")[1]
        Checksum = str(Checksum).zfill(2)
        PACKET_COUNT = PACKET_COUNT + 1
        return Checksum

    def broadcast_to_frontend(self, handle, data) -> None:
        # print(data)
        self.sio.emit(
            "message_rerouter", {"target": "jsroom", "handler": handle, "data": data}
        )

    def progress_bar(self, progress, total):
        """
        Calculates percentage progress and broadcasts to frontend

        Args:
            progress (int): How many elements are processed.
            total (int): Total number of elements to be processed.
        """
        percent = f"{100 * (progress / float(total)):.2f}"
        # print((percent, progress, total), end="\r")
        if (datetime.now() - self.time).total_seconds() > 1:
            self.broadcast_to_frontend(handle="flash_progress", data=percent)
            self.time = datetime.now()

    def UintToInt(self, a):
        """
        Converts Uint to int.

        Args:
            a (uint): Uint input.

        Returns:
            int: Returns int.
        """
        if a > 128:
            a = 256 - a
            a = a * (-1)
        return a

    def Parse_Data(self, BattID, BMSId):
        """
        Parsing collected raw data into csv.

        Args:
            BattID (str): Battery serial number for creating csv and writing in it.
            BMSId (str): BMS serial number to write in csv.

        Returns:
            bool: Returns whether this function was executed successfully or not.
        """

        # Create a filename with the timestamp
        self.buffer = deque([])
        self.csv_file = f"{self.path}\\{BattID}_{self.timestamp_str}.csv"
        conn = sqlite3.connect(
            "./resources/app/data.db" if is_bundled() else "./data.db"
        )
        curr = conn.cursor()
        curr.execute("SELECT data FROM req_data WHERE id = ?", ("interface",))
        row = curr.fetchone()
        if row:
            json_data_str = row[0]
            self.data_dict = json.loads(json_data_str)
        else:
            print("No data found in the config table.")
        conn.close()
        self.init_csv_dict()
        self.init_tracker()
        self.init_csv(self.csv_file, BattID, BMSId)
        self.cell_temp_itr = 0
        self.cell_volt_itr = 0
        # Returns JSON object as a dictionary
        enum = 0
        array_size = 0
        bytesize = 0
        jsonMemSize = 0
        for maxpacketlen in self.data_dict:
            jsonMemSize = (
                jsonMemSize
                + (
                    self.data_dict[maxpacketlen]["bytes"]
                    * self.data_dict[maxpacketlen]["array_size"]
                )
                + 1
            )
        jsonMemSize = jsonMemSize + 9

        datalen = 0
        Timestamp = 0
        itr = 2
        lenarray = len(self.can_queue)
        self.can_queue = list(self.can_queue)
        self.broadcast_to_frontend(handle="flash_status_info", data="Decoding Data")
        while itr < lenarray:
            try:
                if (self.can_queue[itr] == "24") and (
                    (self.can_queue[itr - 1] == "23")
                    or (self.can_queue[itr - 1] == "23\n")
                ):
                    self.lines += 1
                    datalen = int(self.can_queue[itr + 1], base=16) * 256 + int(
                        self.can_queue[itr + 2], base=16
                    )
                    if datalen < jsonMemSize:
                        if (self.can_queue[itr + datalen + 4] == "23") or (
                            self.can_queue[itr + datalen + 4] == "23\n"
                        ):
                            dum = []
                            dum.extend(self.can_queue[itr + 3 : itr + 3 + datalen])
                            # self.raw_data_csv(self.timestamp_str, dum)
                            if (
                                self.cal_checksum(dum, datalen)
                                == (self.can_queue[itr + datalen + 3])
                            ):
                                EpochTimestamp = dum[3] + dum[2] + dum[1] + dum[0]
                                epoch_timestamp = int(
                                    EpochTimestamp, 16
                                )  # January 1, 2021, 00:00:00 (GMT)
                                # Convert epoch timestamp to a datetime object
                                datetime_object = datetime.fromtimestamp(
                                    epoch_timestamp, tz=pytz.timezone("UTC")
                                )
                                # Format the datetime object as a string
                                Timestamp = datetime_object.strftime(
                                    "%Y-%m-%d %H:%M:%S"
                                )
                                dum2 = []
                                v = 4
                                while v < len(dum):
                                    enum = int(dum[v], 16)
                                    enum_ID = "ID_" + str(enum)
                                    bytesize = self.data_dict["ID_" + str(enum)][
                                        "bytes"
                                    ]
                                    array_size = self.data_dict["ID_" + str(enum)][
                                        "array_size"
                                    ]
                                    # for c in range(0,array_size*bytesize):
                                    dum2.extend(
                                        dum[v + 1 : array_size * bytesize + v + 1]
                                    )
                                    if bytesize > 1:
                                        if array_size > 1:
                                            dum3 = []
                                            dum4 = []
                                            var = 0
                                            for z in range(0, array_size):
                                                for x in range(bytesize, 0, -1):
                                                    dum3.append(
                                                        int(
                                                            dum2[z * bytesize + x - 1],
                                                            16,
                                                        )
                                                    )
                                                if (
                                                    (
                                                        self.data_dict[
                                                            "ID_" + str(enum)
                                                        ]["data_type"]
                                                        == "int8_t"
                                                    )
                                                    or (
                                                        self.data_dict[
                                                            "ID_" + str(enum)
                                                        ]["data_type"]
                                                        == "int16_t"
                                                    )
                                                    or (
                                                        self.data_dict[
                                                            "ID_" + str(enum)
                                                        ]["data_type"]
                                                        == "int32_t"
                                                    )
                                                    or (
                                                        self.data_dict[
                                                            "ID_" + str(enum)
                                                        ]["data_type"]
                                                        == "int64_t"
                                                    )
                                                ):
                                                    var = int.from_bytes(
                                                        dum3,
                                                        byteorder="big",
                                                        signed=True,
                                                    )
                                                else:
                                                    var = int.from_bytes(
                                                        dum3, byteorder="big"
                                                    )
                                                if (
                                                    "_".join(
                                                        (
                                                            (
                                                                (
                                                                    self.data_dict.get(
                                                                        "ID_"
                                                                        + str(enum)
                                                                    )
                                                                ).get("name")
                                                            ).split("_")
                                                        )[2:4]
                                                    )
                                                    == "CELL_VOLTS"
                                                ):
                                                    dum4.append(var / 10000)
                                                else:
                                                    dum4.append(var)
                                                var = 0
                                                dum3 = []
                                            self.write_data(Timestamp, enum_ID, dum4)
                                            dum4 = []
                                        else:
                                            dum3 = []
                                            var = 0
                                            for x in range(bytesize, 0, -1):
                                                dum3.append(int(dum2[x - 1], 16))
                                            if (
                                                (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int8_t"
                                                )
                                                or (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int16_t"
                                                )
                                                or (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int32_t"
                                                )
                                                or (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int64_t"
                                                )
                                            ):
                                                var = int.from_bytes(
                                                    dum3, byteorder="big", signed=True
                                                )
                                            else:
                                                var = int.from_bytes(
                                                    dum3, byteorder="big"
                                                )
                                            if (
                                                "_".join(
                                                    (
                                                        (
                                                            (
                                                                self.data_dict.get(
                                                                    "ID_" + str(enum)
                                                                )
                                                            ).get("name")
                                                        ).split("_")
                                                    )[2:4]
                                                )
                                                == "BATT_AMPS"
                                            ):
                                                var = var / 1000
                                            self.write_data(Timestamp, enum_ID, var)
                                            var = 0
                                            dum3 = []
                                    else:
                                        if array_size > 1:
                                            dum3 = []
                                            dum4 = []
                                            var = 0
                                            ver = ""
                                            for z in range(0, array_size):
                                                a = int(dum2[z], 16)
                                                if str(enum) == "9":
                                                    ver = ver + (chr(int(dum2[z], 16)))
                                                elif str(enum) == "10":
                                                    ver = ver + (chr(int(dum2[z], 16)))
                                                if str(enum) == "11":
                                                    ver = ver + (chr(int(dum2[z], 16)))
                                                elif (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int8_t"
                                                ):
                                                    a = self.UintToInt(a)
                                                dum3.append(a)
                                            if (
                                                str(enum) == "9"
                                                or str(enum) == "10"
                                                or str(enum) == "11"
                                            ):
                                                self.write_data(Timestamp, enum_ID, ver)
                                            else:
                                                self.write_data(
                                                    Timestamp, enum_ID, dum3
                                                )
                                            dum3 = []
                                        else:
                                            var = int(dum[v + 1], 16)
                                            if (
                                                self.data_dict["ID_" + str(enum)][
                                                    "data_type"
                                                ]
                                                == "int8_t"
                                            ):
                                                var = self.UintToInt(var)
                                            self.write_data(Timestamp, enum_ID, var)
                                    dum2 = []
                                    v = v + int(bytesize * array_size) + 1
                                self.write_to_csv()
                                itr = itr + datalen + 5
                            else:
                                itr += 1
                        else:
                            itr += 1
                    else:
                        itr += 1
                # itr is kept less than 5 because the first four bytes are empty spaces and start bytes of the sector
                elif (self.can_queue[itr] == "24") and (
                    (
                        (self.can_queue[itr - 1] == "41")
                        and (self.can_queue[itr - 2] == "41")
                    )
                    or (
                        (self.can_queue[itr - 1] == "52")
                        and (self.can_queue[itr - 2] == "52")
                    )
                ):
                    self.lines += 1
                    datalen = int(self.can_queue[itr + 1], base=16) * 256 + int(
                        self.can_queue[itr + 2], base=16
                    )
                    if datalen < jsonMemSize:
                        if (self.can_queue[itr + datalen + 4] == "23") or (
                            self.can_queue[itr + datalen + 4] == "23\n"
                        ):
                            dum = []
                            dum.extend(self.can_queue[itr + 3 : itr + 3 + datalen])
                            if (
                                self.cal_checksum(dum, datalen)
                                == (self.can_queue[itr + datalen + 3])
                            ):
                                EpochTimestamp = dum[3] + dum[2] + dum[1] + dum[0]
                                epoch_timestamp = int(
                                    EpochTimestamp, 16
                                )  # January 1, 2021, 00:00:00 (GMT)
                                # Convert epoch timestamp to a datetime object
                                datetime_object = datetime.fromtimestamp(
                                    epoch_timestamp, tz=pytz.timezone("UTC")
                                )
                                # Format the datetime object as a string
                                Timestamp = datetime_object.strftime(
                                    "%Y-%m-%d %H:%M:%S"
                                )
                                dum2 = []
                                v = 4
                                while v < len(dum):
                                    enum = int(dum[v], 16)
                                    enum_ID = "ID_" + str(enum)
                                    bytesize = self.data_dict["ID_" + str(enum)][
                                        "bytes"
                                    ]
                                    array_size = self.data_dict["ID_" + str(enum)][
                                        "array_size"
                                    ]
                                    dum2.extend(
                                        dum[v + 1 : array_size * bytesize + v + 1]
                                    )
                                    if bytesize > 1:
                                        if array_size > 1:
                                            dum3 = []
                                            dum4 = []
                                            var = 0
                                            for z in range(0, array_size):
                                                for x in range(bytesize, 0, -1):
                                                    dum3.append(
                                                        int(
                                                            dum2[z * bytesize + x - 1],
                                                            16,
                                                        )
                                                    )
                                                var = int.from_bytes(
                                                    dum3, byteorder="big"
                                                )
                                                if (
                                                    "_".join(
                                                        (
                                                            (
                                                                (
                                                                    self.data_dict.get(
                                                                        "ID_"
                                                                        + str(enum)
                                                                    )
                                                                ).get("name")
                                                            ).split("_")
                                                        )[2:4]
                                                    )
                                                    == "CELL_VOLTS"
                                                ):
                                                    dum4.append(var / 10000)
                                                else:
                                                    dum4.append(var)
                                                var = 0
                                                dum3 = []
                                            self.write_data(Timestamp, enum_ID, dum4)
                                            dum4 = []
                                        else:
                                            dum3 = []
                                            var = 0
                                            for x in range(bytesize, 0, -1):
                                                dum3.append(int(dum2[x - 1], 16))
                                            if (
                                                (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int8_t"
                                                )
                                                or (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int16_t"
                                                )
                                                or (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int32_t"
                                                )
                                                or (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int64_t"
                                                )
                                            ):
                                                var = int.from_bytes(
                                                    dum3, byteorder="big", signed=True
                                                )
                                            else:
                                                var = int.from_bytes(
                                                    dum3, byteorder="big"
                                                )
                                            if (
                                                "_".join(
                                                    (
                                                        (
                                                            (
                                                                self.data_dict.get(
                                                                    "ID_" + str(enum)
                                                                )
                                                            ).get("name")
                                                        ).split("_")
                                                    )[2:4]
                                                )
                                                == "BATT_AMPS"
                                            ):
                                                var = var / 1000
                                            self.write_data(Timestamp, enum_ID, var)
                                            var = 0
                                            dum3 = []
                                    else:
                                        if array_size > 1:
                                            dum3 = []
                                            dum4 = []
                                            var = 0
                                            ver = ""
                                            for z in range(0, array_size):
                                                a = int(dum2[z], 16)
                                                if str(enum) == "9":
                                                    ver = ver + (chr(int(dum2[z], 16)))
                                                elif str(enum) == "10":
                                                    ver = ver + (chr(int(dum2[z], 16)))
                                                if str(enum) == "11":
                                                    ver = ver + (chr(int(dum2[z], 16)))
                                                elif (
                                                    self.data_dict["ID_" + str(enum)][
                                                        "data_type"
                                                    ]
                                                    == "int8_t"
                                                ):
                                                    a = self.UintToInt(a)
                                                dum3.append(a)
                                            if (
                                                str(enum) == "9"
                                                or str(enum) == "10"
                                                or str(enum) == "11"
                                            ):
                                                self.write_data(Timestamp, enum_ID, ver)
                                            else:
                                                self.write_data(
                                                    Timestamp, enum_ID, dum3
                                                )
                                            dum3 = []
                                        else:
                                            var = int(dum[v + 1], 16)
                                            if (
                                                self.data_dict["ID_" + str(enum)][
                                                    "data_type"
                                                ]
                                                == "int8_t"
                                            ):
                                                var = self.UintToInt(var)
                                            self.write_data(Timestamp, enum_ID, var)
                                    dum2 = []
                                    v = v + int(bytesize * array_size) + 1
                                self.write_to_csv()
                                itr = itr + datalen + 5
                            else:
                                itr += 1
                        else:
                            itr += 1
                    else:
                        itr += 1
                else:
                    itr += 1
                self.progress_bar(itr, lenarray)
                # print(itr, end="\r")
            except Exception:
                itr = itr + 1
                self.cell_temp_itr = 0
                self.cell_volt_itr = 0
                self.cell_temp_array = []
                self.cell_volt_array = []
                continue
        self.write_to_csv(final=True)
        return True

    def sort_csv(self, BattID):
        """Sorting the generated csv to align the timestamps.

        Args:

            BattId (str): Battery serial number for reading and creation of csv file.
            self.timestamp_str (str): Timestamp for reading and creation of csv file.

        """
        # Read the CSV file
        with open(f"{self.path}\\{BattID}_{self.timestamp_str}.csv", "r") as file:
            csv_reader = csv.reader(file)
            data = list(csv_reader)
        data, header = data[3:], data[:3]

        # Sort the data by the first column
        sorted_data = sorted(data, key=lambda x: x[0])

        # Write the sorted data back to a new CSV file
        with open(
            f"{self.path}\\{BattID}_{self.timestamp_str}.csv", "w", newline=""
        ) as file:
            csv_writer = csv.writer(file)
            csv_writer.writerows(header)
            csv_writer.writerows(sorted_data)

    def read_message(
        self,
    ) -> can.Message | None:
        """Return message from config queue. None in case of error."""
        try:
            msg = self.config_queue.get(timeout=2)
            return msg
        except Exception:
            return None

    # Getting Battery and BMS serial number.
    def get_bms_dats(
        self,
    ):
        """
        Gets the battery and BMS serial numbers from BMS.

        Returns:
            tuple: Returns the serial numbers as a tuple.
        """
        self.message_sender(
            0x701,
            [0xD3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        )
        arr = []
        while True:
            msg = self.read_message()
            if not msg:
                self.broadcast_to_frontend(
                    handle="flash_status_info", data="Serial numbers not coming"
                )
                return
            else:
                if msg.arbitration_id == 0x712:
                    arr.append(msg.data)
                if msg.arbitration_id == 0x713:
                    arr.append(msg.data[:-1])
                if msg.arbitration_id == 0x714:
                    arr.append(msg.data)
                if msg.arbitration_id == 0x715:
                    arr.append(msg.data[:-1])
                    break

        Battid = "".join([chr(j) for i in arr[:2] for j in i])
        BMSId = "".join([chr(j) for i in arr[2:] for j in i])
        self.broadcast_to_frontend(
            handle="flash_status_info", data="Battery ID : " + Battid
        )
        time.sleep(1)
        self.broadcast_to_frontend(handle="flash_status_info", data="BMS ID : " + BMSId)
        return (Battid, BMSId)

    def message_sender(self, arbitration_id, data):
        """Sends message  on bus with given parameters"""
        message = can.Message(
            is_extended_id=False,
            arbitration_id=arbitration_id,
            dlc=8,
            data=(c_ubyte * 8)(*data),
        )
        try:
            self.bus.send(message)
            return True
        except Exception as e:
            self.broadcast_to_frontend(
                handle="flash_status_info", data=f"Error sending config message: {e}"
            )
            try:
                self.bus.send(message)
                return True
            except Exception as e:
                self.broadcast_to_frontend(
                    handle="flash_status_info",
                    data=f"Error sending config message: {e}",
                )
                return False

    def confirm_response(
        self,
    ):
        """
        Confirms the response that BMS has acknowledged.

        Returns:
            Int : Number of segements to be received.
        """
        while True:
            CAN = self.read_message()
            if not CAN:
                self.broadcast_to_frontend(
                    handle="flash_status_info",
                    data="Error in receiving confirmation message",
                )
                return

            if CAN.arbitration_id == 0x710:
                if CAN.data[0] == 0xF0 and CAN.is_extended_id is False:
                    time.sleep(0.1)
                    self.broadcast_to_frontend(
                        handle="flash_status_info", data="Confirmation Received"
                    )
                    bytes = int.from_bytes(CAN.data[1:5])
                    segments = (bytes // 4096) * 3 + -((bytes % 4096) // -1785)
                    segments_info = int.from_bytes(CAN.data[6:])
                    if segments == segments_info:
                        pass
                    else:
                        self.broadcast_to_frontend(
                            handle="flash_status_info",
                            data="Error mismatch between number of segments calculated and received.",
                        )
                        return
                    self.message_sender(
                        0x703,
                        [
                            0xF0,
                            segments >> 8,
                            segments & 0xFF,
                            0x00,
                            0x00,
                            0x00,
                            0x00,
                            0x00,
                        ],
                    )
                    return segments
                else:
                    self.broadcast_to_frontend(
                        handle="flash_status_info",
                        data="Wrong confirmation message received.",
                    )
                    return

    def fill_queue(self, segments):
        """
        Fills the queue with received messages for decoding later.

        Args:
            segments (int): Numnber of segments that are going to come.

        Returns:
            Bool: Returns whether this function was executed successfully or not.
        """
        self.can_queue = deque([])
        num = 0
        cnt = 0
        times = 0
        self.broadcast_to_frontend(handle="flash_status_info", data="Collecting data")
        while True:
            # print("\nHMmmmmm.....")
            CanMessages = self.read_message()
            if not CanMessages:
                self.message_sender(
                    0x704,
                    [0xF0, num >> 8, num & 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00],
                )
                times += 1
                self.broadcast_to_frontend(
                    handle="flash_status_info", data="Times retried : " + str(times)
                )
                print("Times retried : " + str(times))
                continue
            else:
                if CanMessages.is_extended_id is False:
                    if CanMessages.arbitration_id == 0x710:
                        times = 0
                        segment_number = int.from_bytes(CanMessages.data[6:])
                        if segment_number == num:
                            buffer = [0] * int.from_bytes(CanMessages.data[1:3])
                            nummsg = 0
                            while nummsg < math.ceil(
                                int.from_bytes(CanMessages.data[1:3]) / 7
                            ):
                                try:
                                    Messages = self.read_message()
                                    if Messages:
                                        if Messages.arbitration_id == 0x711:
                                            if nummsg < len(buffer) // 7:
                                                buffer[7 * nummsg : 7 * nummsg + 7] = (
                                                    Messages.data[1:]
                                                )
                                            else:
                                                buffer[7 * nummsg : 7 * nummsg + 7] = (
                                                    Messages.data[
                                                        1 : 1 + len(buffer) - nummsg * 7
                                                    ]
                                                )
                                            nummsg += 1
                                            # self.broadcast_to_frontend(handle="flash_status_info",data="nums: " + str(nummsg))
                                        elif Messages.arbitration_id == 0x00:
                                            continue
                                        else:
                                            self.broadcast_to_frontend(
                                                handle="flash_status_info",
                                                data="Data Transfer not complete",
                                            )
                                            print(
                                                "Data Transfer not complete", Messages
                                            )
                                            return
                                except Exception as e:
                                    self.broadcast_to_frontend(
                                        handle="flash_status_info",
                                        data="Error receiving 0x711 data : " + e,
                                    )
                                    print(
                                        "Error receiving 0x711 data : " + e,
                                    )
                                    return
                            self.can_queue.extend([f"{i:02x}" for i in buffer])
                            try:
                                self.bus.reset()
                            except Exception:
                                pass
                            self.message_sender(
                                0x704,
                                [
                                    0xF0,
                                    num >> 8,
                                    num & 0xFF,
                                    0x00,
                                    0x00,
                                    0x00,
                                    0x00,
                                    0x00,
                                ],
                            )
                        else:
                            self.broadcast_to_frontend(
                                handle="flash_status_info",
                                data="Wrong segment received",
                            )
                            print(
                                "Wrong segment received",
                            )
                            self.broadcast_to_frontend(
                                handle="flash_status_info", data=CanMessages
                            )
                            print(int.from_bytes(CanMessages.data[6:]), num)
                            return
                        num += 1
                    # else:
                    #     self.broadcast_to_frontend(handle="flash_status_info",data=CanMessages.arbitration_id)
                    self.progress_bar(num, segments)
                    if num == segments:
                        return True
                else:
                    cnt += 1
                    if cnt >= 200:
                        self.broadcast_to_frontend(
                            handle="flash_status_info",
                            data="Extended messages inturrupted collection",
                        )
                        print("Extended messages inturrupted collection")
                        return

    data = 0

    def init_tracker(
        self,
    ):
        for i in self.csv_dict:
            self.id_tracker[i] = 0

    def init_csv_dict(
        self,
    ):
        self.csv_dict["TIMESTAMP"] = 0
        self.csv_dict["PACK VOLTAGE"] = 0
        self.csv_dict["MIN CELL VOLTAGE arbitration_id"] = 0
        self.csv_dict["MIN CELL VOLTAGE"] = 0
        self.csv_dict["MAX CELL VOLTAGE arbitration_id"] = 0
        self.csv_dict["MAX CELL VOLTAGE"] = 0
        self.csv_dict["MAX TEMP arbitration_id"] = 0
        self.csv_dict["MAX TEMP"] = 0
        for i in self.data_dict:
            for j in range(0, int((self.data_dict.get(i)).get("array_size"))):
                if "bitfields" in self.data_dict.get(i):
                    if (self.data_dict.get(i)).get("array_size") == 1:
                        for k in self.data_dict.get(i).get("bitfields"):
                            name = k
                            self.csv_dict[f"{name}"] = 0
                    else:
                        for k in self.data_dict.get(i).get("bitfields"):
                            name = "_".join((k.split("_"))[2:])
                            self.csv_dict[f"{name}"] = 0
                    break
                else:
                    name = "_".join(
                        (((self.data_dict.get(i)).get("name")).split("_"))[2:4]
                    )
                    if (self.data_dict.get(i)).get("array_size") == 1:
                        self.csv_dict[f"{name}"] = 0
                    else:
                        if i == "ID_9" or i == "ID_10" or i == "ID_11":
                            self.csv_dict[f"{name}"] = 0
                            break
                        elif name == "CELL_VOLTS":
                            self.cell_volt_itr += 1
                            self.csv_dict[f"{name}_{self.cell_volt_itr}"] = 0
                        elif name == "CELL_TEMP":
                            self.cell_temp_itr += 1
                            self.csv_dict[f"{name}_{self.cell_temp_itr}"] = 0
                        else:
                            self.csv_dict[f"{name}_{j + 1}"] = 0
        return self.csv_dict

    def init_csv(self, csv_file, BattID, BMSId):
        rows = [
            ["Battery arbitration_id", "'" + BattID + "'"],
            ["BMS arbitration_id", "'" + BMSId + "'"],
        ]
        with open(csv_file, "w", newline="") as file:
            writer = csv.writer(file)
            writer.writerows(rows)

    def reverse_bits(self, byte, size):
        # Use bin() to convert the byte to a binary string, remove the '0b' prefix, reverse the string, and pad it to 8 bits
        reversed_binary = bin(byte)[2:].zfill(size)[::-1]

        # Convert the reversed binary string back to an integer
        reversed_byte = int(reversed_binary, 2)

        return reversed_byte

    def array_to_binary_string(self, input_array, size):
        if len(input_array) > 1:
            binary_strings = [bin(element)[2:].zfill(size) for element in input_array]
        else:
            binary_strings = [bin(input_array)[2:].zfill(size)]
        result = "".join(binary_strings)
        return result

    def number_to_32_bit_binary(self, number):
        # Convert number to binary string and remove the '0b' prefix
        binary_string = bin(number)[2:]

        # Pad the binary string with leading zeros to make it 32 bits long
        padded_binary_string = binary_string.zfill(32)

        return padded_binary_string

    def numbers_to_reversed_binary(self, numbers):
        # Convert each number to a 32-bit binary string
        binary_strings = [self.number_to_32_bit_binary(num) for num in numbers]

        # Concatenate the binary strings
        concatenated_binary_string = "".join(binary_strings)

        # Reverse the binary string
        reversed_binary_string = concatenated_binary_string[::-1]

        return reversed_binary_string

    def reverse_string(self, s):
        return s[::-1]

    def calculate_values(self):
        if self.cell_volt_itr > 0:
            self.csv_dict["PACK VOLTAGE"] = sum(self.cell_volt_array)
            try:
                self.csv_dict["MIN CELL VOLTAGE"] = min(
                    [i for i in self.cell_volt_array if 0 < i]
                )
                self.csv_dict["MIN CELL VOLTAGE arbitration_id"] = (
                    self.cell_volt_array.index(self.csv_dict["MIN CELL VOLTAGE"]) + 1
                )
            except:  # noqa: E722
                self.csv_dict["MIN CELL VOLTAGE"] = 0
                self.csv_dict["MIN CELL VOLTAGE arbitration_id"] = "inv"

            self.csv_dict["MAX CELL VOLTAGE"] = max(
                [i for i in self.cell_volt_array if 0 < i] + [0]
            )
            if self.csv_dict["MAX CELL VOLTAGE"] == 0 and 0 not in self.cell_volt_array:
                self.csv_dict["MAX CELL VOLTAGE arbitration_id"] = "inv"
            else:
                self.csv_dict["MAX CELL VOLTAGE arbitration_id"] = (
                    self.cell_volt_array.index(self.csv_dict["MAX CELL VOLTAGE"]) + 1
                )

        if self.cell_temp_itr > 0:
            dat = [i for i in self.cell_temp_array if -40 <= i <= 120]
            if len(dat) > 0:
                self.csv_dict["MAX TEMP"] = max(self.cell_temp_array)
                self.csv_dict["MAX TEMP arbitration_id"] = (
                    self.cell_temp_array.index(self.csv_dict["MAX TEMP"]) + 1
                )
            else:
                self.csv_dict["MAX TEMP"] = "inv"
                self.csv_dict["MAX TEMP arbitration_id"] = "inv"

        if self.id_tracker["PACK VOLTAGE"] == 0:
            self.id_tracker["TIMESTAMP"] = 1
            self.id_tracker["PACK VOLTAGE"] = 1
            self.id_tracker["MIN CELL VOLTAGE"] = 1
            self.id_tracker["MIN CELL VOLTAGE arbitration_id"] = 1
            self.id_tracker["MAX CELL VOLTAGE"] = 1
            self.id_tracker["MAX CELL VOLTAGE arbitration_id"] = 1
            self.id_tracker["MAX TEMP"] = 1
            self.id_tracker["MAX TEMP arbitration_id"] = 1

    def write_data(self, timestamp, arbitration_id, data):
        self.csv_dict["TIMESTAMP"] = timestamp

        name = "_".join(
            (((self.data_dict.get(arbitration_id)).get("name")).split("_"))[2:4]
        )

        if name == "CELL_VOLTS":
            for m in data:
                self.cell_volt_array.append(m)
        if name == "CELL_TEMP":
            for m in data:
                self.cell_temp_array.append(m)

        for j in range(0, int((self.data_dict.get(arbitration_id)).get("array_size"))):
            if "bitfields" in self.data_dict.get(arbitration_id):
                if int((self.data_dict.get(arbitration_id)).get("array_size")) == 1:
                    for k in self.data_dict.get(arbitration_id).get("bitfields"):
                        name = k
                        Data = self.reverse_bits(
                            data,
                            8 * int((self.data_dict.get(arbitration_id)).get("bytes")),
                        )
                        binary_strings = self.reverse_string([bin((Data))[2:].zfill(8)])
                        result = "".join(binary_strings)
                        var = result[
                            self.data_dict.get(arbitration_id)
                            .get("bitfields")
                            .get(k)
                            .get("bit_num")
                        ]
                        self.csv_dict[f"{name}"] = var
                        if self.id_tracker[f"{name}"] == 0:
                            self.id_tracker[f"{name}"] = 1
                else:
                    # self.broadcast_to_frontend(handle="flash_status_info",data=data)
                    Data = []
                    bin_result = self.numbers_to_reversed_binary(
                        data
                    )  # reverse_string(temp)
                    for k in self.data_dict.get(arbitration_id).get("bitfields"):
                        var = bin_result[
                            self.data_dict.get(arbitration_id)
                            .get("bitfields")
                            .get(k)
                            .get("bit_num")
                        ]
                        name = "_".join((k.split("_"))[2:])
                        self.csv_dict[f"{name}"] = var
                        if self.id_tracker[f"{name}"] == 0:
                            self.id_tracker[f"{name}"] = 1
                    break
            else:
                name = "_".join(
                    (((self.data_dict.get(arbitration_id)).get("name")).split("_"))[2:4]
                )
                if (self.data_dict.get(arbitration_id)).get("array_size") == 1:
                    self.csv_dict[f"{name}"] = data
                    if self.id_tracker[f"{name}"] == 0:
                        self.id_tracker[f"{name}"] = 1
                else:
                    if arbitration_id == "ID_9":
                        self.csv_dict[f"{name}"] = data[:5]
                        if self.id_tracker[f"{name}"] == 0:
                            self.id_tracker[f"{name}"] = 1
                        break
                    elif arbitration_id == "ID_10" or arbitration_id == "ID_11":
                        self.csv_dict[f"{name}"] = data
                        if self.id_tracker[f"{name}"] == 0:
                            self.id_tracker[f"{name}"] = 1
                        break
                    elif name == "CELL_VOLTS":
                        self.cell_volt_itr += 1
                        self.csv_dict[f"{name}_{self.cell_volt_itr}"] = data[j]
                        if self.id_tracker[f"{name}_{self.cell_volt_itr}"] == 0:
                            self.id_tracker[f"{name}_{self.cell_volt_itr}"] = 1
                    elif name == "CELL_TEMP":
                        self.cell_temp_itr += 1
                        self.csv_dict[f"{name}_{self.cell_temp_itr}"] = data[j]
                        if self.id_tracker[f"{name}_{self.cell_temp_itr}"] == 0:
                            self.id_tracker[f"{name}_{self.cell_temp_itr}"] = 1
                    else:
                        self.csv_dict[f"{name}_{j + 1}"] = data[j]
                        if self.id_tracker[f"{name}_{j + 1}"] == 0:
                            self.id_tracker[f"{name}_{j + 1}"] = 1

    def write_to_csv(self, final=False):
        self.calculate_values()
        if not final:
            self.buffer.append(self.csv_dict.copy())
        self.cell_temp_itr = 0
        self.cell_volt_itr = 0
        self.cell_temp_array = []
        self.cell_volt_array = []
        if final:
            self.broadcast_to_frontend(
                handle="flash_status_info",
                data="Data decoding complete",
            )
            # Delete the extra columns
            for i in self.id_tracker:
                if self.id_tracker[i] == 0:
                    self.columns_to_delete.append(i)
            self.delete_columns()

    # Function to delete specified columns from a CSV file
    def delete_columns(self):
        with open(self.csv_file, mode="a", newline="") as file:
            fieldnames = []
            for field in self.csv_dict.keys():
                if field not in self.columns_to_delete:
                    fieldnames.append(field)

            writer = csv.DictWriter(file, fieldnames=fieldnames)
            writer.writeheader()
            maxl = len(self.buffer)
            numrow = 0
            currtime = time.time()
            # print("\nWriting to file")
            self.broadcast_to_frontend(
                handle="flash_status_info",
                data="Writing to file",
            )
            for row in self.buffer:
                # Delete the specified columns from the row
                row = {
                    key: value
                    for key, value in row.items()
                    if key not in self.columns_to_delete
                }
                # self.buffer.append(row)
                writer.writerow(row)
                numrow += 1
                if time.time() - currtime > 1:
                    self.progress_bar(numrow, maxl)
                    currtime = time.time()
            self.progress_bar(100, 100)
            time.sleep(0.1)
            self.broadcast_to_frontend(
                handle="flash_status_info",
                data="Writing to file complete",
            )


if __name__ == "__main__":
    Version = ""
    data = "."
    try:
        Pcan = can.Bus(channel="PCAN_USBBUS1", interface="pcan")
        print("PCAN initialized | ", Version)
    except Exception:
        raise ConnectionError
    flash = FlashExtraction(Pcan)
    par = False
    t = datetime.now()
    BattId, BMSId = flash.get_bms_dats()
    if BattId:
        flash.message_sender(0x701, [0xF0, 00, 00, 00, 00, 00, 00, 00])
        segments = flash.confirm_response()
        if segments:
            fill = flash.fill_queue(segments)
        else:
            fill = False
            pass
        if fill:
            par = flash.Parse_Data(BattId, BMSId)
        else:
            print("Error in Fill")
        if par:
            print("\nSuccess")
            print("Time Taken: ", (datetime.now() - t))
            Pcan.shutdown()
        # print("Lines decoded = ", flash.lines)
