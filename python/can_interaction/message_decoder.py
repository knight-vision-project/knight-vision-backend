import ctypes
from can import Message
from enum import Enum
from typing import Callable, Type


class Paramater:
    def __init__(
        self,
        name: str,
        size: int,
        multiplier: int = 1,
        type: Type | tuple = None,
        decoder: Callable = None,
        tag: str = None,
    ) -> None:
        self.name = name
        self.size = size
        self.multiplier = multiplier
        self.type = (
            type
            if isinstance(type, tuple)
            else (
                type,
                None,
            )
        )
        self.decoder = decoder
        self.tag = tag

    def tag_parameters(self):
        pass

    def decode_data(self, data: int) -> Enum:
        """Decodes parameter with it's corresponding decoder

        Args:
            data (int): Value of parameter before decoding

        Returns:
            Enum: Decoded value of the parameter
        """
        return self.decoder(data)

    def cast_data(self) -> tuple:
        """Casts data type into a tuple

        Returns:
            tuple: Cast, Subcast
        """
        cast, subcast = self.type
        if self.multiplier != 1:
            cast = float
        # if self.size == 1 and self.name not in ["Cell Bal. Type", "RTC_Type"]:
        #     cast = bool
        return cast, subcast

    def parse_data(self, data: str) -> tuple[Enum | str | int, str]:
        """Parses live data value for a parameter and decodes is accordingly.

        Args:
            data (str): Input live value of parameter as a string.

        Returns:
            tuple[Enum | str | int, str]: Return the decoded value and remaining string
        """
        cast, subcast = self.cast_data()
        value = int(data[: self.size], 2)
        if subcast:
            value = subcast(value).value if subcast.__module__ == "ctypes" else value
        if cast:
            value = cast(value / self.multiplier)
        if self.decoder:
            value = self.decode_data(value)
        return value, data[self.size :]


def bms_status_decoder(data: int) -> Enum:
    """
    Decodes a given data value into a corresponding BMS status name.

    :param data: The `data` parameter is the value that needs to be decoded to determine the BMS status
    :return: the name of the BMSStatus enum value corresponding to the given data
    """

    class BMSStatus(Enum):
        IDLE = 1
        CHARGING = 2
        FULLCHARGE = 3
        DISCHARGING = 4
        RESERVE = 5
        FULLDISCHARGE = 6
        SLEEP = 7
        IMMOBILIZED = 8
        REGENERATING = 9
        BATTERYFAULT_IDLE = 33
        BATTERYFAULT_CHARGING = 34
        BATTERYFAULT_FULLCHARGE = 35
        BATTERYFAULT_DISCHARGING = 36
        BATTERYFAULT_RESERVE = 37
        BATTERYFAULT_FULLDISCHARGE = 38
        BATTERYFAULT_SLEEP = 39
        BATTERYFAULT_IMMOBILIZED = 40
        BATTERYFAULT_REGENERATING = 41
        HARDWAREFAULT_IDLE = 65
        HARDWAREFAULT_CHARGING = 66
        HARDWAREFAULT_FULLCHARGE = 67
        HARDWAREFAULT_DISCHARGING = 68
        HARDWAREFAULT_RESERVE = 69
        HARDWAREFAULT_FULLDISCHARGE = 70
        HARDWAREFAULT_SLEEP = 71
        HARDWAREFAULT_IMMOBILIZED = 72
        HARDWAREFAULT_REGENERATING = 73
        BATTERYFAULT_HARDWAREFAULT_IDLE = 97
        BATTERYFAULT_HARDWAREFAULT_CHARGING = 98
        BATTERYFAULT_HARDWAREFAULT_FULLCHARGE = 99
        BATTERYFAULT_HARDWAREFAULT_DISCHARGING = 100
        BATTERYFAULT_HARDWAREFAULT_RESERVE = 101
        BATTERYFAULT_HARDWAREFAULT_FULLDISCHARGE = 102
        BATTERYFAULT_HARDWAREFAULT_SLEEP = 103
        BATTERYFAULT_HARDWAREFAULT_IMMOBILIZED = 104
        BATTERYFAULT_HARDWAREFAULT_REGENERATING = 105

    try:
        return BMSStatus(data).name
    except ValueError:
        return "UNKNOWN_STATE"


def led_status_decoder(data: int) -> Enum:
    class LEDStatus(Enum):
        SOC_25_LEVEL = 2
        UNDER_VOLTAGE_PROTECTION = 3
        RESERVE_SOC_PROTECTION = 4
        OVER_CURRENT_PROTECTION = 5
        SOC_50_LEVEL = 6
        SHORT_CIRCUIT_FAULT = 7
        TEMP_PROTECTION = 9
        SOC_75_LEVEL = 14
        MOSFET_FAULT = 15
        THERMAL_RUNAWAY = 21
        SOC_99_LEVEL = 27
        SOC_100_LEVEL = 30
        HW_FAULT_TEMP_SENS = 31
        CELL_OVER_DEVIATION = 32
        AFE_TEMP_PROTECTION = 34
        FET_TEMP_PROTECTION = 35

    try:
        return LEDStatus(data).name
    except ValueError:
        return "UNKNOWN_STATE"


def version_decoder(data: str) -> str:
    """Decodes the version name for a parameter from it string value

    Args:
        data (str): Value of parameter as string

    Returns:
        str: Decoded version name
    """
    return "".join(chr(i) for i in bytes.fromhex(f"{data:02x}"))


def cell_balance_decoder(data: str) -> list[int]:
    """Decodes the cell balancing status of BMS

    Args:
        data (str): Status as a string

    Returns:
        list[int]: Decoded status
    """
    data = f"{data:016b}"
    decoded_data = []
    for i in data[::-1]:
        decoded_data.append(int(i))
    return decoded_data


parameters = {
    0x01: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(1, 5)
    ],
    0x02: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(5, 9)
    ],
    0x03: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(9, 13)
    ],
    0x04: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(13, 17)
    ],
    0x05: [
        Paramater(name="Max Cell Vol", size=16, multiplier=1000, tag="batteryVoltages"),
        Paramater(name="Min Cell Vol", size=16, multiplier=1000, tag="batteryVoltages"),
        Paramater(
            name="Avg Cell Voltage", size=16, multiplier=1000, tag="batteryVoltages"
        ),
        Paramater(name="Max Cell Vol ID", size=8, tag="batteryVoltages"),
        Paramater(name="Min Cell Vol ID", size=8, tag="batteryVoltages"),
    ],
    0x06: [
        Paramater(
            name="Pack Current",
            size=32,
            multiplier=1000,
            type=(float, ctypes.c_int),
            tag="topBarData",
        ),
        Paramater(name="Pack Voltage", size=32, multiplier=1000, tag="topBarData"),
    ],
    0x07: [
        Paramater(name="Cycle Count", size=16, tag="topBarTable"),
        Paramater(
            name="Max Temp", type=(int, ctypes.c_byte), size=8, tag="batteryTemp"
        ),
        Paramater(
            name="Min Temp", type=(int, ctypes.c_byte), size=8, tag="batteryTemp"
        ),
        Paramater(name="Max Temp ID", size=8, tag="batteryTemp"),
        Paramater(name="Min Temp ID", size=8, tag="batteryTemp"),
        Paramater(name="Cell Vol Dev", size=16, tag="batteryVoltages"),
    ],
    0x08: [
        Paramater(name="SOC", size=8, tag="topBarCircular"),
        Paramater(name="SOC [Ah]", size=32, tag="liveData", multiplier=1000),
        Paramater(name="SOH", size=8, tag="topBarCircular"),
        Paramater(name="FET Temp", type=(int, ctypes.c_byte), size=8, tag="othTemps"),
        Paramater(
            name="BMS Status", size=8, tag="topBarData", decoder=bms_status_decoder
        ),
    ],
    0x09: [
        # Byte 0
        Paramater(name="Dchg Over Temp. [°C] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Dchg Under Temp. [°C] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Chg Over Temp. [°C] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Chg Under Temp. [°C] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Pack Over Vol. [V] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Pack Under Vol. [V] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Cell Over Vol. [V] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Cell Under Vol. [V] Fault", size=1, tag="batteryFaults"),
        # Byte 1
        Paramater(name="Reserve SoC [%] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Fet Temp. Protection", size=1, tag="bmsFault"),
        Paramater(name="Cell Over Vol. [V] Warning", size=1, tag="warnings"),
        Paramater(name="Cell Under Vol. [V] Warning", size=1, tag="warnings"),
        Paramater(name="Dchg Over Current 1 [A] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Chg Over Current [A] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Low SOC [%] Warning", size=1, tag="warnings"),
        Paramater(name="Cell Vol. Deviation [mV] Fault", size=1, tag="batteryFaults"),
        # Byte 2
        Paramater(name="Dchg Over Temp. [°C] Warning", size=1, tag="warnings"),
        Paramater(name="Dchg Under Temp. [°C] Warning", size=1, tag="warnings"),
        Paramater(name="Chg Over Temp. [°C] Warning", size=1, tag="warnings"),
        Paramater(name="Chg Under Temp. [°C] Warning", size=1, tag="warnings"),
        Paramater(name="Pack Over Vol. [V] Warning", size=1, tag="warnings"),
        Paramater(name="Pack Under Vol. [V] Warning", size=1, tag="warnings"),
        Paramater(name="Temp Sens. Fault", size=1, tag="bmsFault"),
        Paramater(name="FET Fail", size=1, tag="bmsFault"),
        # Byte 3
        Paramater(
            name="Led Status of the BMS",
            size=8,
            decoder=led_status_decoder,
        ),
        # Byte 4
        Paramater(name="AFE2 Over Temp Fault", size=1, tag="bmsFault"),
        Paramater(name="Regenerative OVP Fault", size=1, tag="bmsFault"),
        Paramater(name="Sleep Mode type", size=2, tag="liveData"),
        Paramater(name="Dchg Over Current Latch", size=1, tag="bmsFault"),
        Paramater(name="Cell Deep Dchg [V] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Dchg Over Current 3 [A] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Dchg Over Current 2 [A] Fault", size=1, tag="batteryFaults"),
        # Byte 5,6
        Paramater(
            name="Cell Bal. Status",
            size=16,
            tag="balancingStatus",
            decoder=cell_balance_decoder,
        ),
        # Byte 7
        Paramater(name="Short Circuit Protection", size=1, tag="bmsFault"),
        Paramater(name="Dchg Over Curr. Peak [A] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Chg Over Curr. Peak [A] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Charger Authentication", size=1),
        Paramater(name="Reserve Mode", size=1),
        Paramater(name="DISCHARGE", size=1, tag="fetStatus"),
        Paramater(name="CHARGE", size=1, tag="fetStatus"),
        Paramater(name="PRECHARGE", size=1, tag="fetStatus"),
    ],
    0x0A: [
        Paramater(
            name=f"Temp {idx}", type=(int, ctypes.c_byte), size=8, tag="cellTemps"
        )
        for idx in range(1, 9)
    ],
    0x0B: [
        Paramater(name="Unused", size=38),
        Paramater(name="Dchg SOP [A] Fault", size=1, tag="batteryFaults"),
        Paramater(name="Chg SOP [A] Fault", size=1, tag="batteryFaults"),
    ],
    0x0C: [
        # Byte 0
        Paramater(name="AFE-2 Fault", size=1, tag="bmsFault"),
        Paramater(name="Unauth Chg Fault", size=1, tag="bmsFault"),
        Paramater(name="Cell Bal. Type", size=1, tag="liveData"),
        Paramater(name="Chg Auth.", size=1, tag="events"),
        Paramater(name="Chg HB", size=1, tag="events"),
        Paramater(name="AFE Fault", size=1, tag="bmsFault"),
        Paramater(name="Thermal Runaway Fault", size=1, tag="batteryFaults"),
        Paramater(name="Short circuit Latch", size=1, tag="bmsFault"),
        # Byte 1
        Paramater(name="Latch Type", size=8),
        # Byte 2
        Paramater(name="Charger Type", size=8, tag="topBarTable"),
        # Byte 3
        Paramater(name="PCB Temp", type=(int, ctypes.c_byte), size=8, tag="othTemps"),
        # Byte 4
        Paramater(name="AFE Temp", type=(int, ctypes.c_byte), size=8, tag="othTemps"),
        # Byte 5
        Paramater(name="Cell Chemistry", size=8, tag="topBarTable"),
        # Byte 6
        Paramater(name="Unused 2", size=1),
        Paramater(name="PCB Over Temp. Fault", size=1, tag="bmsFault"),
        Paramater(name="AFE Over Temp Fault", size=1, tag="bmsFault"),
        Paramater(name="AFE Valid Data", size=1, tag="events"),
        Paramater(name="Buzzer", size=1, tag="events"),
        Paramater(name="Dchg Immobilization", size=1, tag="events"),
        Paramater(name="Chg Immobilization", size=1, tag="events"),
        Paramater(name="Cell Bal. Feature", size=1, tag="events"),
        # Byte 7
        Paramater(name="CAN Communication", size=1, tag="events"),
        Paramater(name="Load Detection", size=1, tag="events"),
        Paramater(name="CHG Detection", size=1, tag="events"),
        Paramater(name="Digital Input 1", size=1, tag="events"),
        Paramater(name="Digital Input 2", size=1, tag="events"),
        Paramater(name="Digital Output 1", size=1, tag="events"),
        Paramater(name="Digital Output 2", size=1, tag="events"),
        Paramater(name="Ignition", size=1, tag="events"),
    ],
    0x0D: [
        Paramater(
            name="HW Version", size=24, tag="topBarTable", decoder=version_decoder
        ),
        Paramater(
            name="FW Version", size=24, tag="topBarTable", decoder=version_decoder
        ),
        Paramater(
            name="firmware_version_internal",
            size=16,
            tag="fw_id",
            decoder=version_decoder,
        ),
    ],
    0x0E: [
        Paramater(name="Cum. Chg. Cap [Ah]", size=32, multiplier=1000, tag="liveData"),
        Paramater(name="Cum. Dchg. Cap [Ah]", size=32, multiplier=1000, tag="liveData"),
    ],
    0x0F: [
        Paramater(name="Ref. Vol. [V]", size=16, multiplier=1000, tag="liveDataVol"),
        Paramater(
            name="Supp. Vol. 3.3V [V]", size=16, multiplier=1000, tag="liveDataVol"
        ),
        Paramater(
            name="Supp. Vol. 5V [V]", size=16, multiplier=1000, tag="liveDataVol"
        ),
        Paramater(
            name="Supp. Vol. 12V [V]", size=16, multiplier=1000, tag="liveDataVol"
        ),
    ],
    0x10: [
        Paramater(
            name="Chg Curr. Reco. [A]",
            size=32,
            tag="liveDataCurr",
            multiplier=1000,
        ),
        Paramater(
            name="Dchg Curr. Reco. [A]",
            size=32,
            tag="liveDataCurr",
            multiplier=1000,
        ),
    ],
    0x12: [
        Paramater(name="True SoC", size=32, multiplier=10000, tag="liveData"),
        Paramater(name="Usable Cap. [Ah]", size=32, multiplier=1000, tag="liveData"),
    ],
    0x15: [
        Paramater(
            name="Config Version", size=24, tag="topBarTable", decoder=version_decoder
        ),
        Paramater(
            name="Internal FW Version", size=24, decoder=version_decoder, tag="fw_id"
        ),
        Paramater(
            name="Internal Sub FW Version",
            size=16,
            decoder=version_decoder,
            tag="fw_id",
        ),
    ],
    0x16: [
        Paramater(name="RTC_Hour", size=8, tag="timeStamp"),
        Paramater(name="RTC_Min", size=8, tag="timeStamp"),
        Paramater(name="RTC_Sec", size=8, tag="timeStamp"),
        Paramater(name="RTC_Day", size=8, tag="timeStamp"),
        Paramater(name="RTC_Month", size=8, tag="timeStamp"),
        Paramater(name="RTC_Year", size=8, tag="timeStamp"),
        Paramater(name="RTC_WDay", size=8, tag="timeStamp"),
        Paramater(name="External RTC Fault Type", size=3),
        Paramater(name="RTC State", size=2),
        Paramater(name="RTC_Type", size=1, tag="liveData"),
        Paramater(name="INT RTC CLK SW Flt", size=1),
        Paramater(name="RTC Fault", size=1, tag="bmsFault"),
    ],
    0x17: [
        Paramater(name="FET-2 Temp", type=(int, ctypes.c_byte), size=8, tag="othTemps"),
        Paramater(name="CAN Matrix Type", type=(int, ctypes.c_ubyte), size=8),
        Paramater(
            name="SOP Table Ver", decoder=version_decoder, size=32, tag="topBarTable"
        ),
    ],
    0xA1: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(17, 21)
    ],
    0xA2: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(21, 25)
    ],
    0xA3: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(25, 29)
    ],
    0xA4: [
        Paramater(name=f"Cell Vol {idx}", size=16, multiplier=10000, tag="cellVoltages")
        for idx in range(29, 33)
    ],
    0xAA: [
        Paramater(
            name=f"Temp {idx}", type=(int, ctypes.c_byte), size=8, tag="cellTemps"
        )
        for idx in range(9, 17)
    ],
    0xAC: [
        Paramater(
            name="Cell Bal. Status 2",
            size=16,
            tag="balancingStatus",
            decoder=cell_balance_decoder,
        ),
    ],
    0x41: [
        Paramater(name="Unused", size=5),
        Paramater(name="Dchg Over Curr. [A] Warning", size=1, tag="warnings"),
        Paramater(name="Chg Over Curr. [A] Warning", size=1, tag="warnings"),
        Paramater(name="Cell Vol. Dev. [V] Warning", size=1, tag="warnings"),
    ],
    0x103: [
        Paramater(
            name="Conn. 1 Temp",
            type=(int, ctypes.c_byte),
            size=8,
            tag="othTemps",
        ),
        Paramater(
            name="Conn. 2 Temp",
            type=(int, ctypes.c_byte),
            size=8,
            tag="othTemps",
        ),
    ],
    0xCF0FF03: [
        Paramater(
            name="Bal 1 Temp",
            type=(int, ctypes.c_byte),
            size=8,
            tag="othTemps",
        ),
        Paramater(
            name="Bal 2 Temp",
            type=(int, ctypes.c_byte),
            size=8,
            tag="othTemps",
        ),
        Paramater(
            name="Bal 3 Temp",
            type=(int, ctypes.c_byte),
            size=8,
            tag="othTemps",
        ),
        Paramater(
            name="Bal 4 Temp",
            type=(int, ctypes.c_byte),
            size=8,
            tag="othTemps",
        ),
    ],
}


class MessageDecoder:
    def __init__(self) -> None:
        self.parameters = parameters

    def decode(
        self, id: int, message: Message, tag_data: bool = True
    ) -> dict[str, dict[str, str | int | list[int] | Enum]]:
        """Decodes a message from live data into all parameters corresponding to it.

        Args:
            id (int): ID of message ot be decoded
            message (Message): CAN message
            tag_data (bool, optional): Whether to tag or not. Defaults to True.

        Returns:
            dict[str, dict[str, str | int | list[int]]]: Returns decoded dictionary containing all decoded
            parameters.
        """
        params = self.parameters.get(id)
        if not params:
            return {}
        data = "".join([format(byte, "08b") for byte in message.data])
        json_data = dict()

        def tag_parameters(
            param: Paramater,
            json_data: dict,
            value: str | int | list[int] | Enum,
            tag: bool = True,
        ) -> dict[str, dict[str, str | int | list[int] | Enum]]:
            """Tags parameters by collecting them into seperate groups to be used by frontend

            Args:
                param (Paramater): Parameter being decoded
                json_data (dict): Empty Dictionary
                value (str | int | list[int] | Enum): Decoded value of the parameter
                tag (bool, optional): Whether to tag or not. Defaults to True.

            Returns:
                dict[str, dict[str, str | int | list[int] | Enum]]: Filled dictionary with tagged parameters
            """
            if tag:
                if param.tag:
                    if param.tag in json_data:
                        json_data[param.tag][param.name] = value
                    else:
                        json_data[param.tag] = {param.name: value}
                else:
                    json_data[param.name] = value
                return json_data
            else:
                json_data[param.name] = value
                return json_data

        for param in params:
            value, data = param.parse_data(data)
            json_data = tag_parameters(param, json_data, value, tag=tag_data)

        return json_data
