from ctypes import (
    c_uint16,
    c_uint32,
    c_ubyte,
    Union,
    sizeof,
    BigEndianStructure,
)
from typing import Type


class WarrantyParameter:
    def __init__(
        self,
        value: float | int,
        type: Type = None,
        type_of_type: None | int | float = None,
        name: str = "",
        id: str = "",
        tag: str = "",
    ):
        self.name = name
        self.id = id
        self.type = type
        self.value = value
        self.tag = tag
        self.type_of_type = type_of_type  # type of bytearray values


def swap32(x: int) -> int:
    """
    The function swaps the byte order of a 32-bit integer.

    :param x: The input integer that needs to be swapped
    :return: The function `swap32` takes a 32-bit integer as input and returns the same integer with its
    byte order reversed. Specifically, it swaps the first and fourth bytes, and the second and third
    bytes.
    """
    return (
        ((x << 24) & 0xFF000000)
        | ((x << 8) & 0x00FF0000)
        | ((x >> 8) & 0x0000FF00)
        | ((x >> 24) & 0x000000FF)
    )


def swap16(x: int) -> int:
    """
    The function swaps the positions of the first 8 bits and the second 8 bits in a 16-bit integer.

    :param x: The parameter x is an integer that represents a 16-bit binary number that needs to be
    swapped
    :return: The function `swap16` takes an input `x` and returns the value obtained by swapping the two
    bytes of `x`. Specifically, it shifts the first byte of `x` to the left by 8 bits and masks it with
    `0xFF00` to keep only the first byte, then shifts the second byte of `x` to the right by 8 bits and
    masks it with
    """
    return ((x << 8) & 0xFF00) | ((x >> 8) & 0x00FF)


params = [
    WarrantyParameter(
        name="BMS_Ser_No.",
        id="cfgBmsSerNo",
        type=c_ubyte * 15,
        value=0,
        tag="Manufacturer_Details",
    ),
    WarrantyParameter(
        name="Charge_Cycles",
        id="ChargeCycles",
        type=c_uint32,
        value=0,
        tag="Manufacturer_Data",
    ),
    WarrantyParameter(
        name="SOH", id="Soh", type=c_ubyte, value=0, tag="Manufacturer_Data"
    ),
    WarrantyParameter(
        name="Under_Voltage_Count",
        id="UnderVoltageCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Over_Voltage_Count",
        id="OverVoltageCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Discharge_Under_Temp_Count",
        id="DischargeUnderTempCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Discharge_Over_Temp_Count",
        id="DischargeOverTempCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Charge_Under_Temp_Count",
        id="ChargeUnderTempCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Charge_Over_Temp_Count",
        id="ChargeOverTempCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Discharge_Over_Current_Count",
        id="DischargeOverCurrentCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Charge_Over_Current_Count",
        id="ChargeOverCurrentCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Low_Soc_Count",
        id="LowSocCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Cell_Over_Deviation_Count",
        id="CellOverDeviationCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Cumulative_Charge_Energy",
        id="CumulativeChargeEnergy",
        type=c_uint32,
        value=0,
        tag="Cumulative_Energy_[kWh]",
    ),
    WarrantyParameter(
        name="Cumulative_Discharge_Energy",
        id="CumulativeDisChargeEnergy",
        type=c_uint32,
        value=0,
        tag="Cumulative_Energy_[kWh]",
    ),
    WarrantyParameter(
        name="Battery_Charge_Cumulative_Capacity",
        id="BatteryChargeCumulativeCapacity",
        type=c_uint32,
        value=0,
        tag="Cumulative_Capacity_[Ah]",
    ),
    WarrantyParameter(
        name="Battery_Discharge_Cumulative_Capacity",
        id="BatteryDischargeCumulativeCapacity",
        type=c_uint32,
        value=0,
        tag="Cumulative_Capacity_[Ah]",
    ),
    WarrantyParameter(
        name="Fet_Temp_Protection_Count",
        id="FetTempProtectionCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Short_Circuit_Protection_Count",
        id="ShortCircuitProtectionCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
    WarrantyParameter(
        name="Battery_Charge_Run_hrs",
        id="BatteryChargeRunhrs",
        type=c_uint32,
        value=0,
        tag="Run_Hours",
    ),
    WarrantyParameter(
        name="Battery_Discharge_Run_hrs",
        id="BatteryDisChargeRunhrs",
        type=c_uint32,
        value=0,
        tag="Run_Hours",
    ),
    WarrantyParameter(
        name="Temperature_Hr_Bucketing_[Hrs]",
        id="TemperatureHrBucketing",
        type=c_uint16 * 14,
        value=0,
        type_of_type=int,
        tag="Temperature_Bins",
    ),
    WarrantyParameter(
        name="SOC_Lower_Limit_Bucketing",
        id="SOCLowerLimitBucketing",
        type=c_uint16 * 10,
        value=0,
        type_of_type=int,
        # tag="SOC_Bucketing",
    ),
    WarrantyParameter(
        name="SOC_Higher_Limit_Bucketing",
        id="SOCHigherLimitBucketing",
        type=c_uint16 * 10,
        value=0,
        type_of_type=int,
        # tag="SOC_Bucketing",
    ),
    WarrantyParameter(
        name="Battery_serial_number",
        id="cfgBattSerNo",
        type=c_ubyte * 15,
        value=0,
        tag="Manufacturer_Details",
    ),
    WarrantyParameter(
        name="Thermal_Runaway_Count",
        id="ThermalRunawayCount",
        type=c_uint32,
        value=0,
        tag="Error_Counts",
    ),
]

# Initiating headers for warranty

warranty_headers = {
    "TemperatureHrBucketing": [
        "< 10 °C",
        "10-19 °C",
        "20-29 °C",
        "30-34 °C",
        "35-39 °C",
        "40-44 °C",
        "45-49 °C",
        "50-54 °C",
        "55-59 °C",
        "60-64 °C",
        "65-69 °C",
        "70-74 °C",
        "75-79 °C",
        "> 80 °C",
    ]
}


# Creating a Union Structure for Warranty
class WarrantyConfig(BigEndianStructure):
    _pack_ = 1
    _fields_ = [(param.id, param.type) for param in params]


class WarrantyConfigUnion(Union):
    _fields_ = [("aData", c_ubyte * sizeof(WarrantyConfig)), ("sData", WarrantyConfig)]


def convert_union_to_json_warranty(
    battConfig: WarrantyConfigUnion, config: dict, length: int
) -> None:
    """
    Converts battery configuration parameters to JSON format and calculates the difference
    between limit and recovery parameters.

    :param battConfig: It is a variable that contains some battery configuration data
    :param config: It is a dictionary that will be populated with configuration parameters extracted
    from the battConfig object
    :param length: Length of warranty config.
    """
    size = 0
    for param in params:
        param_name = param.name
        if param.tag not in config:
            config[param.tag] = {}
        if hasattr(param.type, "_length_"):
            if param.type_of_type is int:
                if param.id in warranty_headers:
                    config[param.tag][param_name] = {
                        warranty_headers[param.id][i]: getattr(
                            battConfig.sData, param.id
                        )[i]
                        for i in range(len(warranty_headers[param.id]))
                    }

                else:
                    config[param.tag][param_name] = "".join(
                        [str(value) for value in getattr(battConfig.sData, param.id)]
                    )
            else:
                config[param.tag][param_name] = "".join(
                    [chr(char) for char in getattr(battConfig.sData, param.id)]
                )
            size += sizeof(param.type)
            if size == length:
                return
            continue

        config[param.tag][param_name] = getattr(battConfig.sData, param.id)
        size += sizeof(param.type)
        if size == length:
            return
