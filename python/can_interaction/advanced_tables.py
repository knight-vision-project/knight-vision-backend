import can
import json
import time
import fnmatch
import sqlite3
import pyzipper
from math import ceil

# from shutil import copyfile
from datetime import datetime
from ctypes import c_ubyte, c_byte

# from openpyxl import load_workbook, workbook
from utils.can_singleton import CanBusSingleton
from utils.common_utils import config_queue, is_bundled, sio

# Load Chemistry table limit information from local database
conn = sqlite3.connect("./resources/app/data.db" if is_bundled() else "./data.db")
curr = conn.cursor()
curr.execute("SELECT data FROM req_data WHERE id = ?", ("table_config_2",))
row = curr.fetchone()
if row:
    table_config = row[0]
    dat_dic = json.loads(table_config)
else:
    print("No data found in the config table.")
conn.close()


class AdvancedChemistryTable:
    def __init__(self) -> None:
        self.user_dict = dict()
        self.data_dict = dat_dic
        self.bus = CanBusSingleton.get_instance()
        self.sio = sio
        self.bin_data = bytearray()

    def message_sender(self, arbitration_id: int, data: list[int]) -> bool:
        """Sends message  on bus with given parameters"""
        message = can.Message(
            is_extended_id=False,
            arbitration_id=arbitration_id,
            dlc=8,
            data=(c_ubyte * 8)(*data),
        )
        try:
            self.bus.send(message)
            return True
        except Exception as e:
            print(f"Error sending config message: {e}")
            return False

    def message_broadcaster(self, handler: str, data: str) -> None:
        """Sends a message to frontend with specific handler

        Args:
            handler (str): Name of handler
            data (str): Message
        """
        self.sio.emit(
            "message_rerouter",
            {"target": "jsroom", "handler": handler, "data": data},
        )
        if handler == "adv_table_status_info":
            time.sleep(3)
            self.sio.emit(
                "message_rerouter",
                {"target": "jsroom", "handler": handler, "data": ""},
            )

    def get_dataType(self, type: str) -> int:
        """Returns a value based on the type

        Args:
            type (str): Type

        Returns:
            int: Corresponding value
        """
        val = 0
        if type == "uint8" or type == "int8":
            val = 1
        elif type == "uint16" or type == "int16":
            val = 2
        elif type == "uint32" or type == "int32" or type == "float":
            val = 4
        return val

    def send_data(self, sid: int, array: list[int]) -> bool:
        """Sends the data from array in sequence 7 bytes at a time starting with
        sending sid

        Args:
            sid (int): SID for specific table
            array (list[int]): List of bytes to send

        Returns:
            bool: Success or Failure
        """
        try:
            dataLen = len(array)
            self.message_sender(
                0x701,
                [
                    (sid),
                    (dataLen >> 8) & 0xFF,
                    (dataLen >> 0) & 0xFF,
                    00,
                    00,
                    00,
                    00,
                    00,
                ],
            )
            seq = 0
            for i in range(0, dataLen, 7):
                K = 7
                byteArr = bytearray([seq])
                if i + 7 > dataLen:
                    K = dataLen - i
                    dumArr = []
                    for itr in range(0, 7 - K):
                        dumArr.append(0)
                    for j in range(0, K):
                        byteArr.append(array[seq * 7 + j])
                    for x in range(0, len(dumArr)):
                        byteArr.append(dumArr[x])
                else:
                    for j in range(0, K):
                        byteArr.append(array[seq * 7 + j])
                self.message_sender(0x702, byteArr)
                seq = seq + 1
            return self.confirm_response(sid)
        except Exception:
            self.message_broadcaster(
                "adv_table_status_info", "Setting table parameters failed"
            )
            return False

    def confirm_response(self, msg_data: int) -> bool:
        """Waits for confirmation from CAN."""
        start_time = time.time()
        while (time.time() - start_time) < 5:
            message = config_queue.get(timeout=5)
            if not message:
                continue
            if message.arbitration_id == 0x710 and message.data[0] == msg_data:
                return True
        return False

    def ReadTableFile(self, file_path: str) -> None:
        """Reads Chemistry table from either zip or excel file.

        Args:
            file_path (str): Path to the file.

        Raises:
            ValueError: Raised when there is an issue with the file.
        """
        if file_path[-3:] == "zip":
            with pyzipper.AESZipFile(file_path, "r") as file:
                try:
                    file.setpassword(b"<9]_dlE#,},p")
                    innerfilename = fnmatch.filter(file.namelist(), "*.json")[0]
                    with file.open(innerfilename, "r") as json_file:
                        self.user_dict = json.load(json_file)
                    self.table_rearranger_f_a(self.user_dict, imp=True)
                except Exception:
                    raise ValueError()
        else:
            try:
                # workbook = load_workbook(filename=file_path)
                # self.convert_xl_to_json(workbook)
                # workbook.close()
                pass
            except Exception:
                raise ValueError()

    def SetChemistryTable(self, data: dict) -> None:
        """Sends chemistry table data to BMS.

        Args:
            data (dict): Contains all the tables to be set.
        """
        # print(data)
        self.table_rearranger_a_f(data)
        for x in self.user_dict:
            Array = []
            Sid = int(self.data_dict.get(x).get("WRITE_SID"), 16)
            for i in self.user_dict.get(x):
                if i == "RowVal":
                    RowCnt = len(self.user_dict.get(x).get(i))
                    if RowCnt > self.data_dict.get(x).get("MAX_ROW_COUNT"):
                        self.message_broadcaster(
                            "adv_table_status_info",
                            "Row count exceeded. Setting tables failed",
                        )
                        return
                    else:
                        Array.append(RowCnt)

                if i == "ColVal":
                    ColCnt = len(self.user_dict.get(x).get(i))
                    if ColCnt > self.data_dict.get(x).get("MAX_COLUMN_COUNT"):
                        self.message_broadcaster(
                            "adv_table_status_info",
                            "Column count exceeded. Setting tables failed",
                        )
                        return
                    else:
                        Array.append(ColCnt)
            try:
                for i in self.user_dict.get(x):
                    RowElSize = self.get_dataType(self.data_dict.get(x).get("ROW_TYPE"))
                    ColElSize = self.get_dataType(self.data_dict.get(x).get("COL_TYPE"))
                    DataElSize = self.get_dataType(
                        self.data_dict.get(x).get("Data_Type")
                    )
                    if i == "RowVal":
                        for j in self.user_dict.get(x).get(i):
                            Data = j
                            for k in range(RowElSize - 1, -1, -1):
                                var = (Data >> 8 * k) & 0xFF
                                Array.append(var)
                    if i == "ColVal":
                        for j in self.user_dict.get(x).get(i):
                            Data = j
                            for k in range(ColElSize - 1, -1, -1):
                                var = (Data >> 8 * k) & 0xFF
                                Array.append(var)
                    if i == "DataVal":
                        for j in self.user_dict.get(x).get("DataVal"):
                            if Sid == 0x70:
                                Data = j
                                for k in range(DataElSize - 1, -1, -1):
                                    var = (Data >> 8 * k) & 0xFF
                                    Array.append(var)
                            else:
                                for ijo in j:
                                    Data = ijo
                                    for k in range(DataElSize - 1, -1, -1):
                                        var = (Data >> 8 * k) & 0xFF
                                        Array.append(var)
            except Exception:
                self.message_broadcaster(
                    "adv_table_status_info", "Setting table parameters failed"
                )

            res = self.send_data(Sid, Array)
            if not res:
                self.message_broadcaster(
                    "adv_table_status_info", "Setting table parameters failed"
                )
                return
        if res:
            ver = [ord(i) for i in self.user_dict["TIME_TO_CHARGE"]["Version"]]
            self.message_sender(0x701, [0xE0, 0x00, 0x04])
            self.message_sender(0x702, [0x00] + ver)
            res = self.confirm_response(0xE0)
            if res:
                self.message_broadcaster(
                    "adv_table_status_info", "Setting table parameters successfull"
                )
            else:
                self.message_broadcaster(
                    "adv_table_status_info", "Setting table parameters failed"
                )

    def GetSocTable(self) -> None:
        """Gets Chemistry tables from the BMS and stores them"""
        sid_Array = [
            (int(self.data_dict[i]["READ_SID"], 16), i) for i in self.data_dict
        ]
        temp_d = {}
        for Sid in sid_Array:
            Sid, name = Sid
            segments_received = 0
            bytes_received = True
            data = []
            self.message_sender(0x701, [Sid, 00, 00, 00, 00, 00, 00, 00])
            dummy_dict = {
                "RowCount": 0,
                "ColCount": 0,
                "RowVal": [],
                "ColVal": [],
                "DataVal": [],
            }
            curr_time = time.time()
            while segments_received < 1 and time.time() - curr_time < 2:
                try:
                    message = config_queue.get_nowait()
                except Exception:
                    message = False
                    pass
                if not message:
                    continue
                if message.arbitration_id == 0x710:
                    TableType = str(hex(Sid))
                    for i in self.data_dict:
                        if self.data_dict.get(i).get("READ_SID") == TableType:
                            TableName = i
                            row_size = self.get_dataType(
                                self.data_dict.get(i).get("ROW_TYPE")
                            )
                            col_size = self.get_dataType(
                                self.data_dict.get(i).get("COL_TYPE")
                            )
                            data_size = self.get_dataType(
                                self.data_dict.get(i).get("Data_Type")
                            )
                            break
                    bytes_received = 0
                    total_bytes = int.from_bytes(message.data[1:3])

                if bytes_received == 0:
                    temp_buffer = [0] * total_bytes
                    while bytes_received < total_bytes:
                        try:
                            msg = config_queue.get_nowait()
                        except:  # noqa: E722
                            continue
                        if msg.arbitration_id == 0x711:
                            if msg.data[0] == 0x00:
                                if msg.data[3] > self.data_dict.get(TableName).get(
                                    "MAX_COLUMN_COUNT"
                                ) or msg.data[2] > self.data_dict.get(TableName).get(
                                    "MAX_ROW_COUNT"
                                ):
                                    self.message_broadcaster(
                                        "adv_table_status_info",
                                        "Row or column count exceeded. Setting tables failed",
                                    )
                                    return
                            if msg.data[0] == ceil(total_bytes / 7) - 1:
                                for i in range(0, total_bytes - msg.data[0] * 7):
                                    temp_buffer[7 * msg.data[0] + i] = msg.data[i + 1]
                                    bytes_received += 1
                            else:
                                for i in range(7):
                                    temp_buffer[7 * msg.data[0] + i] = msg.data[i + 1]
                                    bytes_received += 1
                    try:
                        data = temp_buffer
                    except Exception:
                        self.message_broadcaster(
                            "adv_table_status_info", "Getting table failed"
                        )
                        return
                    if len(data) > 2:
                        temp_d[name] = self.parse_dataTo_json(
                            data, dummy_dict, row_size, col_size, data_size
                        )
                        if temp_d[name] != "err":
                            segments_received = segments_received + 1
                        else:
                            return
        if not temp_d:
            self.message_broadcaster("adv_table_status_info", "Getting table failed")
            return
        # print(temp_d)
        self.table_rearranger_f_a(temp_d)
        # print(self.user_dict)
        self.message_broadcaster("adv_table_data", self.user_dict)
        time.sleep(0.5)
        self.message_broadcaster(
            "adv_table_status_info", "Getting table parameters successfull"
        )

    def table_rearranger_f_a(self, dic: dict[dict], imp: bool = False) -> None:
        """Converts the format of chemistry tables from the one present in the file
        to the one which is used by app.

        Args:
            dic (dict[dict]): Data in file format
            imp (bool, optional): Indicate whether the data is imported from a file. Defaults to False.
        """
        ttc_dict = {
            dic["TIME_TO_CHARGE"]["ColVal"][i]: dic["TIME_TO_CHARGE"]["DataVal"][0][i]
            for i in range(dic["TIME_TO_CHARGE"]["ColCount"])
        }
        dte_dict = {
            dic["DISTANCE_TO_EMPTY"]["ColVal"][i]: dic["DISTANCE_TO_EMPTY"]["DataVal"][
                0
            ][i]
            for i in range(dic["DISTANCE_TO_EMPTY"]["ColCount"])
        }
        try:
            ver = dic["TIME_TO_CHARGE"]["Version"]
        except Exception:
            ver = ""
        if imp:
            data = {
                "Version": ver,
                "TIME_TO_CHARGE": ttc_dict,
                "DISTANCE_TO_EMPTY": dte_dict,
            }
        else:
            data = {
                "TIME_TO_CHARGE": ttc_dict,
                "DISTANCE_TO_EMPTY": dte_dict,
            }
        self.user_dict = data
        self.version = ver

    def table_rearranger_a_f(
        self, dic: dict[str, dict[str, dict[str, str | int | list]]]
    ) -> None:
        """Converts the format of chemistry tables from the one used by app
        to the one which is present in the file.

        Args:
            dic (dict[str, dict[str,dict]]): Data in app format
        """
        ttc = {
            "Version": dic["Version"],
            "RowCount": 1,
            "ColCount": len(dic["TIME_TO_CHARGE"]),
            "RowVal": [0],
            "ColVal": list(map(int, dic["TIME_TO_CHARGE"].keys())),
            "DataVal": [list(dic["TIME_TO_CHARGE"].values())],
        }
        dte = {
            "Version": dic["Version"],
            "RowCount": 1,
            "ColCount": len(dic["DISTANCE_TO_EMPTY"]),
            "RowVal": [0],
            "ColVal": list(map(int, dic["DISTANCE_TO_EMPTY"].keys())),
            "DataVal": [list(dic["DISTANCE_TO_EMPTY"].values())],
        }
        self.user_dict = {
            "TIME_TO_CHARGE": ttc,
            "DISTANCE_TO_EMPTY": dte,
        }
        self.version = dic["Version"]

    # def conv_data_to_bin(self) -> None:
    #     """Converts chemistry tables JSON to bytearray"""
    #     bin_array = [0xC0]
    #     soc_length = (
    #         self.user_dict["SOC_OCV"]["RowCount"]
    #         + self.user_dict["SOC_OCV"]["ColCount"]
    #         + 2
    #         * (
    #             self.user_dict["SOC_OCV"]["RowCount"]
    #             * self.user_dict["SOC_OCV"]["ColCount"]
    #         )
    #         + 2
    #     )
    #     bin_array.append(soc_length >> 8 & 0xFF)
    #     bin_array.append(soc_length & 0xFF)
    #     bin_array.append(self.user_dict["SOC_OCV"]["RowCount"])
    #     bin_array.append(self.user_dict["SOC_OCV"]["ColCount"])
    #     for rowVal in self.user_dict["SOC_OCV"]["RowVal"]:
    #         bin_array.append(rowVal & 0xFF)
    #     for colVal in self.user_dict["SOC_OCV"]["ColVal"]:
    #         bin_array.append(colVal & 0xFF)
    #     for line in self.user_dict["SOC_OCV"]["DataVal"]:
    #         for dataVal in line:
    #             bin_array.append(dataVal >> 8 & 0xFF)
    #             bin_array.append(dataVal & 0xFF)

    #     bin_array.append(0xC1)

    #     chg_length = (
    #         self.user_dict["CHG_DER"]["RowCount"]
    #         + self.user_dict["CHG_DER"]["ColCount"]
    #         + 2
    #         * (
    #             self.user_dict["CHG_DER"]["RowCount"]
    #             * self.user_dict["CHG_DER"]["ColCount"]
    #         )
    #         + 2
    #     )
    #     bin_array.append(chg_length >> 8 & 0xFF)
    #     bin_array.append(chg_length & 0xFF)
    #     bin_array.append(self.user_dict["CHG_DER"]["RowCount"])
    #     bin_array.append(self.user_dict["CHG_DER"]["ColCount"])
    #     for rowVal in self.user_dict["CHG_DER"]["RowVal"]:
    #         bin_array.append(rowVal & 0xFF)
    #     for colVal in self.user_dict["CHG_DER"]["ColVal"]:
    #         bin_array.append(colVal & 0xFF)
    #     for line in self.user_dict["CHG_DER"]["DataVal"]:
    #         for dataVal in line:
    #             bin_array.append(dataVal >> 8 & 0xFF)
    #             bin_array.append(dataVal & 0xFF)

    #     bin_array.append(0xC2)

    #     dchg_length = (
    #         self.user_dict["DSCHG_DER"]["RowCount"]
    #         + self.user_dict["DSCHG_DER"]["ColCount"]
    #         + 2
    #         * (
    #             self.user_dict["DSCHG_DER"]["RowCount"]
    #             * self.user_dict["DSCHG_DER"]["ColCount"]
    #         )
    #         + 2
    #     )
    #     bin_array.append(dchg_length >> 8 & 0xFF)
    #     bin_array.append(dchg_length & 0xFF)
    #     bin_array.append(self.user_dict["DSCHG_DER"]["RowCount"])
    #     bin_array.append(self.user_dict["DSCHG_DER"]["ColCount"])
    #     for rowVal in self.user_dict["DSCHG_DER"]["RowVal"]:
    #         bin_array.append(rowVal & 0xFF)
    #     for colVal in self.user_dict["DSCHG_DER"]["ColVal"]:
    #         bin_array.append(colVal & 0xFF)
    #     for line in self.user_dict["DSCHG_DER"]["DataVal"]:
    #         for dataVal in line:
    #             bin_array.append(dataVal >> 8 & 0xFF)
    #             bin_array.append(dataVal & 0xFF)

    #     bin_array.extend(
    #         [0xE0, 0, 4] + [ord(i) for i in self.user_dict["SOC_OCV"]["Version"]]
    #     )
    #     self.bin_data = bytearray(bin_array)

    # def convert_xl_to_json(self, workbook: workbook.workbook.Workbook) -> None:
    #     """Loads an Excel Workbook and converts it to JSON

    #     Args:
    #         workbook (workbook.workbook.Workbook): Excel workbook object containing chemistry tables data
    #     """
    #     # Initialize a dictionary to hold all data
    #     all_data = {}

    #     # Iterate through each sheet in the workbook
    #     for sheet in workbook.sheetnames:
    #         worksheet = workbook[sheet]  # Select the sheet
    #         sheet_data = []

    #         # Iterate through the rows in the worksheet
    #         for row in worksheet.iter_rows(values_only=True):
    #             if any(cell is not None for cell in row):
    #                 sheet_data.append(row)
    #             elif sheet == "OCV-SOC":
    #                 continue
    #             else:
    #                 break
    #         # Store the sheet data in the dictionary
    #         all_data[sheet] = sheet_data
    #     # print(all_data)
    #     conv_dict = {}
    #     ver = all_data["OCV-SOC"][0][1]
    #     conv_dict["Version"] = ver
    #     conv_dict["SOC_OCV"] = {int(i[0]): int(i[1]) for i in all_data["OCV-SOC"][2:]}
    #     conv_dict["CHG_DER"] = {
    #         int(i[0]): {
    #             int(all_data["Charge SOP Table"][0][j]): int(i[j])
    #             for j in range(1, len(all_data["Charge SOP Table"][0]))
    #         }
    #         for i in all_data["Charge SOP Table"][1:]
    #     }
    #     conv_dict["DSCHG_DER"] = {
    #         int(i[0]): {
    #             int(all_data["Discharge SOP Table"][0][j]): int(i[j])
    #             for j in range(1, len(all_data["Discharge SOP Table"][0]))
    #         }
    #         for i in all_data["Discharge SOP Table"][1:]
    #     }
    #     # print(conv_dict)
    #     self.user_dict = conv_dict
    #     self.version = ver

    def parse_dataTo_json(
        self,
        dataArr: list[int],
        dictionarydata: dict[str, dict[str, str | int | list]],
        Row_size: int,
        Column_size: int,
        Data_size: int,
    ) -> dict[str, int | list[list[int]]]:
        """Converts Chemistry table data into JSON format

        Args:
            dataArr (list[int]): Array containing data of the table
            dictionarydata (dict[str, dict[str, str  |  int  |  list]]): Dictionary containing keys of JSON
            Row_size (int): Size of the row
            Column_size (int): Size of columns
            Data_size (int): Total size of the data

        Returns:
            dict[str, int | list[list[int]]]: Table in dictionary format
        """
        RowCnt = 0
        ColCnt = 0
        RowVal = []
        ColVal = []
        DataVal = []
        RowCnt = dataArr[1]
        ColCnt = dataArr[2]
        DataCnt = RowCnt * ColCnt

        for i in range(0, RowCnt):
            combined_integer = 0
            for j in range(0, Row_size):
                combined_integer = c_byte(
                    combined_integer | (dataArr[3 + j + i * Row_size] << 8 * j)
                ).value
            RowVal.append(combined_integer)

        for i in range(0, ColCnt):
            combined_integer = 0
            for j in range(0, Column_size):
                combined_integer = (combined_integer) | (
                    dataArr[3 + RowCnt * Row_size + j + i * Column_size] << 8 * j
                )
            ColVal.append(combined_integer)
        dumArr = []
        for i in range(0, DataCnt):
            combined_integer = 0
            for j in range(0, Data_size):
                try:
                    combined_integer = (combined_integer) | (
                        (
                            dataArr[
                                3
                                + ColCnt * Column_size
                                + RowCnt * Row_size
                                + j
                                + i * Data_size
                            ]
                        )
                        << 8 * j
                    )
                except IndexError:
                    self.message_broadcaster(
                        "adv_table_status_info", "Getting table failed"
                    )
                    return {}
            dumArr.append(combined_integer)
        try:
            DataVal = [dumArr[i : i + ColCnt] for i in range(0, len(dumArr), ColCnt)]
        except Exception:
            self.message_broadcaster(
                "adv_table_status_info",
                "Getting table failed",
            )
            return "err"
        dictionarydata["ColCount"] = ColCnt
        dictionarydata["ColVal"] = ColVal
        dictionarydata["RowCount"] = RowCnt
        dictionarydata["RowVal"] = RowVal
        dictionarydata["DataVal"] = DataVal

        return dictionarydata

    def table_importer(self, path: str) -> None:
        """Imports Chemistry table file using path

        Args:
            path (str): Path to the file.
        """
        try:
            self.ReadTableFile(path)
        except Exception:
            self.message_broadcaster(
                "adv_table_status_info",
                "Incorrect file. Importing table failed",
            )
            return

        self.message_broadcaster(
            "adv_table_data",
            self.user_dict,
        )
        time.sleep(0.5)
        self.message_broadcaster(
            "adv_table_status_info",
            "Import table successful",
        )

    def table_exporter(
        self, path: str, data: dict[str, dict[str, dict[str, str | int | list]]]
    ) -> None:
        """Export Chemistry tables in a zip file as a JSON and bin file

        Args:
            path (str): Location where it should be exported
            data (dict[str, dict[str, dict[str, str  |  int  |  list]]]): Dictionary containing the data
            of all tables
        """
        try:
            self.table_rearranger_a_f(data)
            # self.conv_data_to_bin()
        except Exception as e:
            print(e)
            self.message_broadcaster(
                "adv_table_status_info",
                "Exporting table failed",
            )
            return
        fileName = (
            path
            + "\\"
            + datetime.now().strftime("%Y-%m-%d-%I-%M-%S-%p-")
            + self.version
            + "-advanced-table-config.zip"
        )
        with pyzipper.AESZipFile(
            fileName,
            "w",
            compression=pyzipper.ZIP_LZMA,
            encryption=pyzipper.WZ_AES,
        ) as file:
            file.setpassword(b"<9]_dlE#,},p")

            # Create a JSON string directly and write it to the zip archive
            file.writestr(
                f"range-estimation-tables-{self.version}.json",
                json.dumps(self.user_dict, indent=2),
            )
            # file.writestr(
            #     "advanced-feature-tables.json",
            #     json.dumps(self.user_dict, indent=2),
            # )
            # file.writestr(f"chemistry-tables-{self.version}.bin", self.bin_data)

        # with open(
        #     path
        #     + "\\"
        #     + datetime.now().strftime("%Y-%m-%d-%I-%M-%S-%p-")
        #     + "table-config.json",
        #     "w",
        # ) as json_file:
        #     json.dump(self.user_dict, json_file, indent=2)
        self.message_broadcaster(
            "adv_table_status_info",
            "Export Table successful",
        )

    # def template_export(self, data: str) -> None:
    #     """Copies a template to a user selected location for their reference

    #     Args:
    #         data (str): Path of the location to copy to.
    #     """
    #     try:
    #         copyfile(
    #             "./resources/app/SampleTables.xlsx"
    #             if is_bundled()
    #             else "./SampleTables.xlsx",
    #             data + "\\SampleTable.xlsx",
    #         )
    #         self.message_broadcaster("adv_table_status_info", "Template export successful")
    #     except Exception:
    #         self.message_broadcaster("adv_table_status_info", "Template export failed")
    #     time.sleep(3)
    #     self.message_broadcaster("adv_table_status_info", "")
