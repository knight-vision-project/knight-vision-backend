from datetime import datetime
from can_interaction.message_decoder import MessageDecoder

decoder = MessageDecoder()


def msg_decoder(message):
    timestamp = datetime.now().strftime("%M:%S.%f")
    timestamp = (
        int(timestamp[:2]) * 10000 + int(timestamp[3:5]) * 100 + int(timestamp[6:8])
    )
    if message.arbitration_id in [1, 2, 3, 4, 6, 8]:
        ms = decoder.decode(message.arbitration_id, message, tag_data=False)
        return (timestamp, ms)
    else:
        return


data_dict_id = {
    "Voltage of Cell 1": "cell_voltage_1",
    "Voltage of Cell 2": "cell_voltage_2",
    "Voltage of Cell 3": "cell_voltage_3",
    "Voltage of Cell 4": "cell_voltage_4",
    "Voltage of Cell 5": "cell_voltage_5",
    "Voltage of Cell 6": "cell_voltage_6",
    "Voltage of Cell 7": "cell_voltage_7",
    "Voltage of Cell 8": "cell_voltage_8",
    "Voltage of Cell 9": "cell_voltage_9",
    "Voltage of Cell 10": "cell_voltage_10",
    "Voltage of Cell 11": "cell_voltage_11",
    "Voltage of Cell 12": "cell_voltage_12",
    "Voltage of Cell 13": "cell_voltage_13",
    "Voltage of Cell 14": "cell_voltage_14",
    "Voltage of Cell 15": "cell_voltage_15",
    "Voltage of Cell 16": "cell_voltage_16",
    "Pack Current": "Pack_current",
    "Pack Voltage": "Pack_voltage",
    "SOC": "SOC",
    "SOH": "SOH",
    "SOC (Ah)": "SOCAh",
    "FET Temperature": "FET Temperature",
    "BMS Status": "BMS Status",
}

data_dict = {
    "Pack_voltage": {"x": [], "y": []},
    "Pack_current": {"x": [], "y": []},
    "cell_voltage_1": {"x": [], "y": []},
    "cell_voltage_2": {"x": [], "y": []},
    "cell_voltage_3": {"x": [], "y": []},
    "cell_voltage_4": {"x": [], "y": []},
    "cell_voltage_5": {"x": [], "y": []},
    "cell_voltage_6": {"x": [], "y": []},
    "cell_voltage_7": {"x": [], "y": []},
    "cell_voltage_8": {"x": [], "y": []},
    "cell_voltage_9": {"x": [], "y": []},
    "cell_voltage_10": {"x": [], "y": []},
    "cell_voltage_11": {"x": [], "y": []},
    "cell_voltage_12": {"x": [], "y": []},
    "cell_voltage_13": {"x": [], "y": []},
    "cell_voltage_14": {"x": [], "y": []},
    "cell_voltage_15": {"x": [], "y": []},
    "cell_voltage_16": {"x": [], "y": []},
    "SOC": {"x": [], "y": []},
    "SOH": {"x": [], "y": []},
    "SOCAh": {"x": [], "y": []},
    "FET Temperature": {"x": [], "y": []},
    "BMS Status": {"x": [], "y": []},
}
