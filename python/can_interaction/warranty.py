import can
import math
from ctypes import c_ubyte
from utils.common_utils import config_queue
from can_interaction.warranty_config import (
    WarrantyConfigUnion,
    convert_union_to_json_warranty,
)
import time
from utils.can_singleton import CanBusSingleton


class Warranty:
    def __init__(self, sid: hex) -> None:
        self.warranty_config = WarrantyConfigUnion()
        self.timeout = 2
        self.bus = CanBusSingleton.get_instance()
        self.sid = sid
        self.warranty = {}

    def read_message(self) -> can.Message | None:
        """Return message from config queue. None in case of error."""
        try:
            msg = config_queue.get(timeout=self.timeout)
            return msg
        except Exception:
            return None

    def send_signal(
        self,
    ) -> bool:
        """Send signal to CAN."""
        try:
            self.bus.reset()
        except Exception:
            pass
        sid = self.sid
        return self.message_sender(
            0x701, [sid, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        )

    def message_sender(self, arbitration_id: int, data: list[int]) -> bool:
        """Sends message  on bus with given parameters"""
        message = can.Message(
            is_extended_id=False,
            arbitration_id=arbitration_id,
            dlc=8,
            data=(c_ubyte * 8)(*data),
        )
        try:
            self.bus.send(message)
            return True
        except Exception as e:
            print(f"Error sending config message: {e}")
            return False

    def confirm_response(
        self,
    ) -> int:
        """Waits for confirmation from CAN."""
        curr = time.time()
        while time.time() - curr < self.timeout:
            message = self.read_message()
            if not message:
                continue
            if message.arbitration_id == 0x710 and message.data[0] == self.sid:
                data_length = message.data[1] << 8 | message.data[2]
                print(
                    time.time(),
                    "Received response, reading data of length ",
                    data_length,
                )
                return data_length
        print("No response from CAN while confirming response")
        return -1

    def collect_warranty(self, config_length: int) -> dict:
        """Return JSON of a warranty config."""
        start_time = time.time()
        message_number = 0
        while (time.time() - start_time) < self.timeout:
            message = self.read_message()
            if not message:
                continue
            if message.arbitration_id == 0x711:
                if message.data[0] == math.ceil(config_length / 7) - 1:
                    self.warranty_config.aData[
                        7 * message.data[0] : 7 * message.data[0] + config_length % 7
                    ] = message.data[1 : config_length % 7 + 1]
                else:
                    self.warranty_config.aData[
                        7 * message.data[0] : 7 * message.data[0] + 7
                    ] = message.data[1:]
                message_number += 1
            if config_length < 7 * message_number:
                print("GET Warranty Acknowledged at backend level")
                config = {}
                convert_union_to_json_warranty(
                    self.warranty_config, config, config_length
                )
                self.warranty = config
                return self.warranty
        print("Communication timeout........ while collecting warranty config")
