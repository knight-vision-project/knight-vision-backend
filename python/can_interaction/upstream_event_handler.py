import os
import csv
import json
import time
import hashlib
import sqlite3
from threading import Event
from datetime import datetime
from utils.common_utils import (
    ConfigSID,
    logging_data_queue,
    config_queue,
    clear_queue,
    new_fw,
    is_bundled,
    sio,
)
from can_interaction.config.config_operations import ConfigOperations
from can_interaction.warranty import Warranty
from can_interaction.firmware_update import FirmwareUpdate
from can_interaction.chemistry_table import ChemistryTable
from can_interaction.config.config_actions import rearrange_config
from can_interaction.config.config_trade import config_importer, config_exporter
from can_interaction.FlashDataExtraction import FlashExtraction
from can_interaction.utility_functions import Utilities
from can_interaction.advanced_tables import AdvancedChemistryTable


class UpstreamEventHandler:
    """Main function to control all modules and decide which to execute based
    on command from Frontend.
    """

    def __init__(self) -> None:
        self.sio = sio
        self.update_config()
        self.utilities = Utilities()
        self.flash = FlashExtraction()
        self.chem_table = ChemistryTable()
        self.firmware_update = FirmwareUpdate()
        self.advanced_chem_table = AdvancedChemistryTable()
        self.warranty = Warranty(ConfigSID.GET_WARR_CONFIG.value)

    # @classmethod
    def update_config(self) -> None:
        """Creates a new ConfigOperations object based on whether new or old fw is detected."""
        self.config = ConfigOperations(
            # ConfigSID.SET_BASE_CONFIG.value
            # if new_fw.is_set()
            # else ConfigSID.SET_BASE_CONFIG_OLD.value,
            ConfigSID.SET_BASE_CONFIG_IOT.value,
            ConfigSID.GET_BASE_CONFIG.value,
            False if new_fw.is_set() else True,
        )

    def on_new_fw_change(self) -> None:
        """This method should be called whenever the new_fw event changes"""
        self.update_config()

    def broadcast_to_frontend(self, handle: str, data) -> None:
        """Sends a message to frontend with specific handler

        Args:
            handler (str): Name of handler
            data (any): Message
        """
        self.sio.emit(
            "message_rerouter", {"target": "jsroom", "handler": handle, "data": data}
        )

    def send_schema(
        self,
    ) -> None:
        """Loads from local database and sends timestamp, authorization status and
        schema to front end.
        """
        conn = sqlite3.connect(
            "./resources/app/data.db" if is_bundled() else "./data.db"
        )
        curr = conn.cursor()

        curr.execute("SELECT data FROM req_data WHERE id = ?", ("rearranged_config",))
        row = curr.fetchone()
        if row:
            rearranged_config = row[0]
            schema = json.loads(rearranged_config)
        else:
            print("No data found in the config table.")

        curr.execute("SELECT data FROM req_data WHERE id = ?", ("last_updated",))
        row = curr.fetchone()
        if row:
            updated = row[0]
        else:
            print("No data found in the last updated table")
            updated = "0000/00/00 00:00:00"

        curr.execute("SELECT data FROM req_data WHERE id = ?", ("auth_status",))
        row = curr.fetchone()
        if row:
            status = row[0]
        else:
            print("No data found in the last updated table")
            status = ""

        curr.execute("SELECT data FROM req_data WHERE id = ?", ("old_config",))
        row = curr.fetchone()
        if row:
            old_config = row[0]
            old_schema = json.loads(old_config)
        else:
            print("No data found in the config table.")
        conn.close()

        if new_fw.is_set():
            self.broadcast_to_frontend("schema", schema)
            print("Sent Schema")
        else:
            self.broadcast_to_frontend("schema", rearrange_config(old_schema))
            print("Sent Schema")
        time.sleep(0.5)
        self.broadcast_to_frontend("updatetime", updated)
        print("Sent time")
        time.sleep(0.5)
        if status == hashlib.sha256(b"KnightAdmin@Lime").hexdigest():
            self.broadcast_to_frontend("authstatus", status)
            print("Sent auth")

    def handle_get_config(self, ini: bool = False, num: int = 0) -> None:
        """
        Gets the base config of the battery.
        """
        clear_queue(config_queue)
        if num == 3:
            print("Retry Limit")
            return
        else:
            res = self.config.send_signal(get_data=True)
            if res:
                data_length = self.config.confirm_response(get_data=True)
                if data_length:
                    configuration = self.config.collect_base_config(data_length)
                    if configuration:
                        configuration = rearrange_config(configuration)
                        print(list(configuration.keys()))
                        self.broadcast_to_frontend(
                            "firmware_type", "new" if new_fw.is_set() else "old"
                        )
                        time.sleep(0.1)
                        self.broadcast_to_frontend("config", configuration)
                        time.sleep(0.3)
                        self.broadcast_to_frontend("status", "Get Config successful")
                        time.sleep(3)
                        self.broadcast_to_frontend("config", {})
                        time.sleep(0.3)
                        self.broadcast_to_frontend("status", "")
                    elif ini:
                        print("no config")
                        self.handle_get_config(True, num + 1)
                    else:
                        print("no config")
                        self.broadcast_to_frontend("status", "Get Config failed")
                        time.sleep(3)
                        self.broadcast_to_frontend("status", "")
                        return
                elif ini:
                    print("no length")
                    self.handle_get_config(True, num + 1)
                else:
                    print("no length")
                    self.broadcast_to_frontend("status", "Get Config failed")
                    time.sleep(3)
                    self.broadcast_to_frontend("status", "")
                    return
            elif ini:
                print("no response")
                self.handle_get_config(True, num + 1)
            else:
                print("no response")
                self.broadcast_to_frontend("status", "Get Config failed")
                time.sleep(3)
                self.broadcast_to_frontend("status", "")
                return

    def handle_set_config(self, json_data: dict) -> None:
        """Sets the base config of the battery"""
        self.config.send_signal(get_data=False)
        self.config.send_data(json_data)
        res = self.config.confirm_response(get_data=False)
        if res >= 0:
            self.broadcast_to_frontend("status", "Set Config successful")
            time.sleep(3)
            self.broadcast_to_frontend("status", "")
        else:
            self.broadcast_to_frontend("status", "Set Config failed")
            time.sleep(3)
            self.broadcast_to_frontend("status", "")

    def handle_import_config(
        self,
        data: dict,
    ) -> None:
        """Imports config from file

        Args:
            data (dict): Dictionary containing path to config file
        """
        config = config_importer(data)
        if config:
            configuration = rearrange_config(config, True)
            if configuration:
                self.broadcast_to_frontend(
                    "firmware_type", "new" if new_fw.is_set() else "old"
                )
                time.sleep(0.1)
                self.broadcast_to_frontend("config", configuration)
                time.sleep(0.1)
                self.broadcast_to_frontend("status", "Import Config successful")
            else:
                self.broadcast_to_frontend("status", "Import Config failed")
            time.sleep(3)
            self.broadcast_to_frontend("config", {})
            self.broadcast_to_frontend("status", "")
        else:
            self.broadcast_to_frontend("status", "Import Config failed")
            time.sleep(3)
            self.broadcast_to_frontend("status", "")

    def handle_export_config(self, data: list) -> None:
        """Exports config to file(s)

        Args:
            data (list): List containing the location and config data to be exported
        """
        bin_data = self.config.return_bytearray(data[1])
        if data[2] == "new":
            new_fw.set()
        else:
            new_fw.clear()
        res = config_exporter(data, bin_data)
        if res:
            self.broadcast_to_frontend("status", "Export Config successful")
        else:
            self.broadcast_to_frontend("status", "Export Config failed")
        time.sleep(3)
        self.broadcast_to_frontend("status", "")

    def handle_soft_reset(
        self,
    ) -> None:
        """Sends the command to soft reset BMS"""
        self.utilities.send_soft_reset_request()

    def handle_rtc_sync(
        self,
    ) -> None:
        """Sends the current timestamp to the BMS to sync up."""
        self.utilities.rtc_sync()

    def convert_trace_to_csv(self, csv_path: str, trace_path: str) -> None:
        """Decodes the data from a given trace file and exports it to a csv file.

        Args:
            csv_path (str): Location to export csv to.
            trace_path (str): Path to the trace file.
        """
        res = self.utilities.convert_trace_to_csv(csv_path, trace_path)
        if res:
            self.broadcast_to_frontend("status", "Trace conversion successful")
        else:
            self.broadcast_to_frontend("status", "Trace conversion failed")
        time.sleep(3)
        self.broadcast_to_frontend("status", "")

    def handle_read_firmware_file(
        self,
        data: dict,
    ) -> None:
        """Update firmware on the BMS

        Args:
            data (dict): Dictionary containing path to the firmware file.
        """
        self.broadcast_to_frontend("firmware_status_info", "> Firmware Update Started")
        time.sleep(0.1)
        self.broadcast_to_frontend(
            "firmware_status_info", "> Entering App Programming Mode"
        )
        self.firmware_update.mainSeq = 1
        self.firmware_update.per = 0
        self.firmware_update.read_file(
            data["size"],
            data["path"],
            data["type"],
        )
        response = self.firmware_update.request_sender(0x03)
        time.sleep(1)

        if response:
            self.broadcast_to_frontend(
                "firmware_status_info", "> Erasing Flash Started"
            )
            response = self.firmware_update.request_sender(0xA0)
        if response:
            self.broadcast_to_frontend("firmware_status_info", "> Sending App Info")
            response = self.firmware_update.request_sender(0xA1)
        if response:
            self.broadcast_to_frontend("firmware_status_info", "> App Update Started")
            response = self.firmware_update.data_sender()
        if response:
            self.broadcast_to_frontend("firmware_status_info", "> App Update Finished")
            self.broadcast_to_frontend("firmware_status_info", "> Verifying App Image")
            self.firmware_update.request_sender(0xA5)
            self.broadcast_to_frontend(
                "firmware_status_info", "> Firmware Update successful"
            )
            return
        self.broadcast_to_frontend("firmware_status_info", "> Firmware Update Failed")

    def handle_set_chemistry_tables(self, data: dict) -> None:
        """Sets Chemistry Tables

        Args:
            data (dict): Dictionary containing chemistry table data
        """
        clear_queue(config_queue)
        self.chem_table.SetChemistryTable(data)

    def handle_get_chemistry_tables(self) -> None:
        """Gets Chemistry Tables from the BMS."""
        clear_queue(config_queue)
        self.chem_table.GetSocTable()

    def handle_import_chemistry_tables(self, data: str) -> None:
        """Imports Chemistry Tables from either zip or excel file.

        Args:
            data (str): Path to the file
        """
        self.chem_table.table_importer(data)

    def handle_export_chemistry_tables(self, data: list) -> None:
        """Export Chemistry Tables to a zip file

        Args:
            data (list): List containing the chemistry table data and folder location to export to.
        """
        self.chem_table.table_exporter(data[0], data[1])

    def handle_set_advanced_tables(self, data: dict) -> None:
        """Sets Advanced Chemistry Tables

        Args:
            data (dict): Dictionary containing chemistry table data
        """
        clear_queue(config_queue)
        self.advanced_chem_table.SetChemistryTable(data)

    def handle_get_advanced_tables(self) -> None:
        """Gets Advanced Chemistry Tables from the BMS."""
        clear_queue(config_queue)
        self.advanced_chem_table.GetSocTable()

    def handle_import_advanced_tables(self, data: str) -> None:
        """Imports Advanced Chemistry Tables from either zip or excel file.

        Args:
            data (str): Path to the file
        """
        self.advanced_chem_table.table_importer(data)

    def handle_export_advanced_tables(self, data: list) -> None:
        """Export Advanced Chemistry Tables to a zip file

        Args:
            data (list): List containing the chemistry table data and folder location to export to.
        """
        self.advanced_chem_table.table_exporter(data[0], data[1])

    def handle_logging(
        self, LogFilePath: str, stop_event: Event, timeout: float
    ) -> None:
        """Starts/Stops the process of logging live data coming from the BMS.

        Args:
            LogFilePath (str): Path where the log file should be created.
            stop_event (Event): Event that dictates starting/stopping of the process
            timeout (float): Time interval at which data should be logged.
        """
        # clear_queue(logging_data_queue)
        LogFilePath = os.path.join(
            LogFilePath,
            datetime.now().strftime("BMSLOG-%Y-%m-%d-%I-%M-%S-%p") + ".csv",
        )
        print(LogFilePath)
        try:
            msg = logging_data_queue.get(timeout=1)
            csvfile = open(LogFilePath, "w+", newline="")
            writer = csv.DictWriter(csvfile, fieldnames=list(msg[1]))
            writer.writeheader()
            writer.writerow(msg[0])
            csvfile.close()
            while not stop_event.is_set():
                try:
                    msg = logging_data_queue.get(timeout=1)
                    # print(msg)
                    csvfile = open(LogFilePath, "a", newline="")
                    writer = csv.DictWriter(csvfile, fieldnames=list(msg[1]))
                    writer.writerow(msg[0])
                    csvfile.close()
                    time.sleep(float(timeout))
                except Exception as e:
                    print(e)
                    continue
        except Exception as e:
            print(e)
            pass

    def handle_get_warranty(
        self,
    ):
        """Gets the warranty config of the battery."""
        self.warranty.send_signal()
        data_length = self.warranty.confirm_response()
        if data_length:
            warranty = self.warranty.collect_warranty(data_length)
            if warranty:
                self.broadcast_to_frontend("warranty_data", warranty)
                time.sleep(0.3)
                self.broadcast_to_frontend("warranty_status", "Get warranty successful")
                time.sleep(3)
                self.broadcast_to_frontend("warranty_status", "")

    def handle_export_warranty(self, filepath: str) -> None:
        """Exports the waranty data gotten from the BMS

        Args:
            filepath (str): Location where the warranty file should be exported to.
        """
        if "Battery_serial_number" in self.warranty.warranty["Manufacturer_Details"]:
            serial_number = self.warranty.warranty["Manufacturer_Details"][
                "Battery_serial_number"
            ]
        else:
            try:
                serial_number = "".join(
                    [
                        chr(i)
                        for i in getattr(
                            self.config.battery_config.sData, "Battery_Serial_Number"
                        )
                    ]
                )
            except Exception:
                self.broadcast_to_frontend("warranty_status", "Export warranty failed")
                time.sleep(3)
                self.broadcast_to_frontend("warranty_status", "")
                return
        filepath = os.path.join(
            filepath,
            datetime.now().strftime(
                f"Warranty-Data-{serial_number}-%Y-%m-%d-%I-%M-%S-%p"
            )
            + ".json",
        )
        warranty = {
            i: self.warranty.warranty[i] for i in self.warranty.warranty if i != ""
        }
        with open(filepath, mode="w", encoding="utf-8") as f:
            json.dump(warranty, f, indent=2, ensure_ascii=False)
        self.broadcast_to_frontend("warranty_status", "Export warranty successful")
        time.sleep(3)
        self.broadcast_to_frontend("warranty_status", "")

    def erase_access(
        self,
    ) -> None:
        """Erases the authorization given to a user."""
        conn = sqlite3.connect(
            "./resources/app/data.db" if is_bundled() else "./data.db"
        )
        curr = conn.cursor()
        curr.execute("""
            CREATE TABLE IF NOT EXISTS req_data (
                id TEXT PRIMARY KEY,
                data TEXT
            )
        """)
        curr.execute(
            """
                INSERT OR REPLACE INTO req_data (id, data)
                VALUES (?, ?)
            """,
            ("auth_status", "No Access"),
        )
        conn.commit()
        curr.close()
        conn.close()
        self.broadcast_to_frontend("status", "Access Revoked")
        time.sleep(3)
        self.broadcast_to_frontend("status", "")

    def handle_auth(self, data: str) -> None:
        """Collects the password entered by user and grants them authorization based
        on if the password is correct.

        Args:
            data (str): Password entered by the User.
        """
        if data == "KnightAdmin@Lime":
            conn = sqlite3.connect(
                "./resources/app/data.db" if is_bundled() else "./data.db"
            )
            curr = conn.cursor()
            curr.execute("""
                CREATE TABLE IF NOT EXISTS req_data (
                    id TEXT PRIMARY KEY,
                    data TEXT
                )
            """)
            curr.execute(
                """
                    INSERT OR REPLACE INTO req_data (id, data)
                    VALUES (?, ?)
                """,
                ("auth_status", hashlib.sha256(b"KnightAdmin@Lime").hexdigest()),
            )
            conn.commit()
            curr.close()
            conn.close()
            self.broadcast_to_frontend("status", "Password is correct")
            self.broadcast_to_frontend(
                "authstatus", hashlib.sha256(b"KnightAdmin@Lime").hexdigest()
            )
            print("Sent auth")
            time.sleep(3)
            self.broadcast_to_frontend("status", "")
        else:
            self.broadcast_to_frontend("status", "Password is incorrect")
            time.sleep(3)
            self.broadcast_to_frontend("status", "")

    def handle_ser_no(self, data: list) -> None:
        """Function used to set BMS/Battery serial number on the BMS.
        If Firmware is L18 or very old L19 series we set it by replacing the corresponding bytes in config.
        If firmware is newer we set it by sending it together with HW config and through config.

        Args:
            data (list): List containing information about which serial number to change as well
            as the serial number.
        """
        number, typ = data[0], data[1]
        clear_queue(config_queue)
        res = self.config.send_signal(get_data=True)
        if res:
            data_length = self.config.confirm_response(get_data=True)
            if data_length:
                configuration = self.config.collect_base_config(data_length)
        if typ == "BMS":
            configuration["FW-ID-84"]["value"] = number
        else:
            configuration["FW-ID-83"]["value"] = number
        configuration = rearrange_config(configuration)
        res = self.utilities.set_ser_no(number, typ)
        # print("Send serial number done")
        if res:
            self.config.send_signal(get_data=False, ser=True)
            self.config.send_data(configuration)
            res = self.config.confirm_response(get_data=False)
            if res != -1:
                self.broadcast_to_frontend(
                    "status", f"Set {typ} serial number successful"
                )
            else:
                self.broadcast_to_frontend("status", f"Set {typ} serial number failed")
        else:
            self.broadcast_to_frontend("status", f"Set {typ} serial number failed")
        time.sleep(3)
        self.broadcast_to_frontend("status", "")

    def template_export(self, data: str) -> None:
        """Exports a template Excel file containing a sample Chemistry table

        Args:
            data (str): Location the the template should be copied to.
        """
        self.chem_table.template_export(data)

    def get_bootloader_info(
        self,
    ) -> None:
        """Gets the Bootloader information from BMS and sends it to Frontend."""
        data = self.utilities.bootloader_info()
        if data:
            self.broadcast_to_frontend("bootloader_info", data)
            time.sleep(0.1)
            self.broadcast_to_frontend(
                "status", "Get Bootloader information successful"
            )
        else:
            self.broadcast_to_frontend("status", "Get Bootloader information failed")
        time.sleep(3)
        self.broadcast_to_frontend("status", "")

    def handle_flash_extraction(
        self,
        data: str,
    ) -> None:
        """Extracts the flash data stored in the BMS decodes it and saves it in a csv.

        Args:
            data (str): Location where the csv should be stored.
        """
        self.flash.timestamp_str = datetime.now().strftime("%Y%m%d%H%M%S")
        self.flash.path = data
        par = False
        t = datetime.now()
        self.broadcast_to_frontend(
            handle="flash_status_info", data="Script started at : " + str(t)
        )
        BattId, BMSId = self.flash.get_bms_dats()
        if BattId:
            self.broadcast_to_frontend(
                handle="flash_status_info", data="Sending query message"
            )
            self.flash.message_sender(0x701, [0xF0, 00, 00, 00, 00, 00, 00, 00])
            segments = self.flash.confirm_response()
            if segments:
                fill = self.flash.fill_queue(segments)
            else:
                return
            if fill:
                self.broadcast_to_frontend(handle="flash_progress", data=100)
                time.sleep(0.1)
                self.broadcast_to_frontend(
                    handle="flash_status_info", data="Data collection complete"
                )
                t2 = datetime.now()
                time.sleep(0.1)
                self.broadcast_to_frontend(
                    handle="flash_status_info", data="\nCollection time: " + str(t2 - t)
                )
                time.sleep(0.1)
                self.broadcast_to_frontend(
                    handle="flash_status_info",
                    data=f"\n{(segments * 100) // 6108}% of flash full",
                )
                par = self.flash.Parse_Data(BattId, BMSId)
            else:
                return
            if par:
                self.broadcast_to_frontend(handle="flash_progress", data=100)
                time.sleep(0.1)
                self.broadcast_to_frontend(
                    handle="flash_status_info",
                    data="\nProcessing time: " + str(datetime.now() - t2),
                )
                time.sleep(0.1)
                self.broadcast_to_frontend(
                    handle="flash_status_info",
                    data="Total time: " + str(datetime.now() - t),
                )
            else:
                self.broadcast_to_frontend(
                    handle="flash_status_info",
                    data="Error Parsing Data",
                )

    def toggle_advanced_features(self, val):
        res = self.utilities.adv_toggle(val)
        cha = "En" if val == 1 else "Dis"
        if res:
            self.broadcast_to_frontend(
                "status", f"Advanced Tables {cha}able successful"
            )
        else:
            self.broadcast_to_frontend("status", f"Advanced Tables {cha}able failed")
        time.sleep(3)
        self.broadcast_to_frontend("status", "")
