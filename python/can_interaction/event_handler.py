from can import Message, BusState
from queue import Queue
from datetime import datetime
import time
from can_interaction import live_database
from can_interaction.message_decoder import MessageDecoder
from utils.common_utils import (
    logging_data_queue,
    new_fw,
    ini,
    sio,
    message_queue,
)

message_queue: Queue[Message]


""" The EventHandler class handles the broadcasting of messages and errors, storing message information
in a dictionary and sending it for asynchronous broadcasting.
"""


class EventHandler:
    def __init__(self) -> None:
        self.messages = dict()
        self.message_decoder = MessageDecoder()
        self.log_data = {"Date": datetime.now().date(), "Time": datetime.now().time()}
        self.currTime = time.time()
        self.currTimeLive = time.time()
        self.dict = dict()
        self.broadcast_type = "sync"
        self.state_time = time.time()
        self.ini = ini
        self.size = 0
        self.repeat = 0
        self.header = []
        self.timedel = time.time()

    def broadcast_messages(
        self,
    ):
        """
        Collects a last message, stores its information in a dictionary, and then
        sends the data to be broadcasted asynchronously.
        """
        if self.ini.is_set():
            try:
                message = message_queue.get_nowait()
            except Exception:
                return

            decoded_message = self.message_decoder.decode(
                message.arbitration_id, message, tag_data=False
            )
            if decoded_message:
                self.messages[message.arbitration_id] = {
                    "id": "{:04x}".format(message.arbitration_id),
                    "time": message.timestamp,
                    "data": "".join("{:02x}".format(b) for b in message.data),
                    "json_data": self.message_decoder.decode(
                        message.arbitration_id, message
                    ),
                }

            for i in decoded_message:
                if i not in self.log_data:
                    self.log_data[i] = decoded_message[i]
                elif self.repeat > 1000:
                    self.size = len(self.log_data.keys())
                    self.log_data = {
                        "Date": datetime.now().date(),
                        "Time": datetime.now().time(),
                    }
                    self.log_data.update(
                        {i: decoded_message[i] for i in decoded_message}
                    )
                    try:
                        if (
                            self.messages[13]["json_data"]["topBarTable"]["FW Version"][
                                1:
                            ]
                            == "18"
                        ):
                            new_fw.clear()
                            # print("event handler", new_fw.is_set())
                        else:
                            new_fw.set()
                    except Exception:
                        pass

                    self.header = ["Date", "Time"]
                    for i in sorted(self.messages.keys()):
                        for j in self.messages[i]["json_data"]:
                            if isinstance(self.messages[i]["json_data"][j], dict):
                                for k in self.messages[i]["json_data"][j]:
                                    self.header.append(k)
                            else:
                                self.header.append(j)

                    self.messages = dict()
                    self.ini.clear()
                    self.timedel = time.time()
                    break
                else:
                    self.repeat += 1
        else:
            try:
                message = message_queue.get_nowait()
            except Exception:
                return
            if time.time() - self.timedel < 0.06:
                return
            decoded_message = self.message_decoder.decode(
                message.arbitration_id, message, tag_data=False
            )
            if decoded_message:
                self.messages[message.arbitration_id] = {
                    "id": "{:04x}".format(message.arbitration_id),
                    "time": message.timestamp,
                    "data": "".join("{:02x}".format(b) for b in message.data),
                    "json_data": self.message_decoder.decode(
                        message.arbitration_id, message
                    ),
                }
            for i in decoded_message:
                self.log_data[i] = decoded_message[i]
            if len(self.log_data) == self.size:
                self.log_data["Date"] = datetime.now().date()
                self.log_data["Time"] = datetime.now().time()
                try:
                    logging_data_queue.get_nowait()
                except Exception:
                    pass
                logging_data_queue.put([self.log_data, self.header])
                self.log_data = {
                    "Date": datetime.now().date(),
                    "Time": datetime.now().time(),
                }
            # print(self.messages.keys())
            if (time.time() - self.currTime) > 1:
                # print(self.messages)
                try:
                    if (
                        self.messages[13]["json_data"]["topBarTable"]["FW Version"][1:]
                        == "18"
                    ):
                        new_fw.clear()
                    else:
                        new_fw.set()
                except Exception:
                    # print(self.messages.keys())
                    pass

                if self.broadcast_type == "sync":
                    sio.emit(
                        "message_rerouter",
                        {
                            "target": "jsroom",
                            "handler": "data",
                            "data": self.messages,
                        },
                    )
                else:
                    pass
                    # start_event_loop(broadcast_data, (self.messages,))  # Async server
                self.currTime = time.time()

    def broadcast_errors(self, error: str) -> None:
        """
        Starts an event loop to broadcast an error.

        :param error: The "error" parameter is a variable that represents an error object or message.
        It is used as an argument to the "broadcast_errors" function
        """
        if (time.time() - self.state_time) > 1:
            if self.broadcast_type == "sync":
                sio.emit(
                    "message_rerouter",
                    {"target": "jsroom", "handler": "error", "data": error},
                )
            else:
                pass
                # start_event_loop(broadcast_errors, (error,))
            self.state_time = time.time()

    def broadcast_state(self, state: BusState) -> None:
        """Send connection state status to the clients

        Args:
            state (BusState): BusState object
        """
        bus_state = state if isinstance(state, str) else state.name
        if (time.time() - self.state_time) > 1:
            if self.broadcast_type == "sync":
                try:
                    sio.emit(
                        "message_rerouter",
                        {"target": "jsroom", "handler": "state", "data": bus_state},
                    )
                except Exception:
                    pass
            else:
                pass
            self.state_time = time.time()

    def broadcast_live_data(self, analytical_data_queue: Queue) -> None:
        """
        Collects a last message, stores its information in a dictionary, and then
        sends the data to be broadcasted asynchronously.
        """
        try:
            message = analytical_data_queue.get_nowait()
        except Exception:
            return
        buffer_data = live_database.msg_decoder(message)
        if buffer_data:
            for i in buffer_data[1]:
                self.dict[i] = buffer_data[1][i]
            if (time.time() - self.currTimeLive) > 1:
                for i in self.dict:
                    live_database.data_dict[live_database.data_dict_id[i]]["y"].insert(
                        0, self.dict[i]
                    )
                    if (
                        len(live_database.data_dict[live_database.data_dict_id[i]]["y"])
                        >= 61
                    ):
                        live_database.data_dict[live_database.data_dict_id[i]][
                            "y"
                        ].pop()
                if self.broadcast_type == "sync":
                    sio.emit(
                        "message_rerouter",
                        {
                            "target": "jsroom",
                            "handler": "analytical_data",
                            "data": live_database.data_dict,
                        },
                    )
                else:
                    pass
                    # start_event_loop(
                    #     broadcast_analytical_data, (live_database.data_dict,)
                    # )
                self.currTimeLive = time.time()
