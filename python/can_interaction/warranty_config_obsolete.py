from ctypes import (
    c_uint16,
    c_uint32,
    c_uint8,
    Union,
    sizeof,
    BigEndianStructure,
)
from typing import Type


class ConfigParamater:
    def __init__(
        self,
        name: str,
        id: str,
        value: float | int,
        multiplier: int = 1,
        type: Type = None,
        type_of_type: None | int | float = None,
        tag: str = "",
    ):
        self.name = name
        self.id = id
        self.multiplier = multiplier
        self.type = type
        self.value = value
        self.tag = tag
        self.type_of_type = type_of_type  # type of bytearray values


def swap32(x):
    """
    The function swaps the byte order of a 32-bit integer.

    :param x: The input integer that needs to be swapped
    :return: The function `swap32` takes a 32-bit integer as input and returns the same integer with its
    byte order reversed. Specifically, it swaps the first and fourth bytes, and the second and third
    bytes.
    """
    return (
        ((x << 24) & 0xFF000000)
        | ((x << 8) & 0x00FF0000)
        | ((x >> 8) & 0x0000FF00)
        | ((x >> 24) & 0x000000FF)
    )


def swap16(x):
    """
    The function swaps the positions of the first 8 bits and the second 8 bits in a 16-bit integer.

    :param x: The parameter x is an integer that represents a 16-bit binary number that needs to be
    swapped
    :return: The function `swap16` takes an input `x` and returns the value obtained by swapping the two
    bytes of `x`. Specifically, it shifts the first byte of `x` to the left by 8 bits and masks it with
    `0xFF00` to keep only the first byte, then shifts the second byte of `x` to the right by 8 bits and
    masks it with
    """
    return ((x << 8) & 0xFF00) | ((x >> 8) & 0x00FF)


params = [
    ConfigParamater(
        name="Start_Byte",
        id="StartByte",
        type=c_uint32,
        multiplier=1,
        value=287454020.0,
    ),
    ConfigParamater(  # IN
        name="ChargeCycles",
        id="ChgCyc",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="ManfData",
    ),
    ConfigParamater(
        name="SOC_Percentage", id="SOCPercent", type=c_uint32, multiplier=1, value=1.0
    ),
    ConfigParamater(name="SoCAh", id="SoCAh", type=c_uint32, multiplier=1, value=0.0),
    ConfigParamater(
        name="Shutdown_Reason",
        id="ShutdownReason",
        type=c_uint8,
        multiplier=1,
        value=1.0,
    ),
    ConfigParamater(
        name="BatteryCapacity_ChargeAccumulation_mAh",
        id="BattCapChgAcc",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="CummCap",
    ),
    ConfigParamater(
        name="BatteryCapacity_DischargeAccumulation_mAh",
        id="BattCapDisChgAcc",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="CummCap",
    ),
    ConfigParamater(name="SOH", id="SOH", type=c_uint32, multiplier=1, value=100),
    ConfigParamater(
        name="Pack_Under_Voltage_Occurance_Count",
        id="PackUndVoltOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Pack_Over_Voltage_Occurance_Count",
        id="PackOverVoltOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    # ConfigParamater(
    #     name="Cell_Under_Voltage_Occurance_Count",
    #     id="CellUndVoltOccCnt",
    #     type=c_uint32,
    #     multiplier=1,
    #     value=0.0,
    #     tag="Errcnt",
    # ),
    # ConfigParamater(
    #     name="Cell_Over_Voltage_Occurance_Count",
    #     id="CellOverVoltOccCnt",
    #     type=c_uint32,
    #     multiplier=1,
    #     value=0.0,
    #     tag="Errcnt",
    # ),
    ConfigParamater(
        name="Discharge_Under_Temperature_Occurrence_Count",
        id="DisChgUndTempOccCnt",
        type=c_uint32,
        multiplier=1,
        value=1.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Discharge_Over_Temperature_Occurrence_Count",
        id="DisChgOverTempOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Charge_Under_Temperature_Occurrence_Count",
        id="ChgUndTempOccCnt",
        type=c_uint32,
        multiplier=1,
        value=1.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Charge_Over_Temperature_Occurrence_Count",
        id="ChgOverTempOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Discharge_Over_Nominal1_Current_Occurrence_Count",
        id="DischgOverNom1CurrOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    # ConfigParamater(
    #     name="Discharge_Over_Nominal2_Current_Occurrence_Count",
    #     id="DischgOverNom2CurrOccCnt",
    #     type=c_uint32,
    #     multiplier=1,
    #     value=0.0,
    #     tag="Errcnt",
    # ),
    ConfigParamater(
        name="Charge_Over_Current_Occurrence_Count",
        id="ChgOverCurrOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Cell_Over_Deviation_Occurrence_Count",
        id="CellOverDevOccCnt",
        type=c_uint32,
        multiplier=1,
        value=4.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Low_Soc_Occurrence_Count",
        id="LowSocOccCnt",
        type=c_uint32,
        multiplier=1,
        value=4.0,
        tag="Errcnt",
    ),
    # ConfigParamater(
    #     name="Discharge_Peak_Current_Occurrence_Count",
    #     id="DisChgPeakCurrOccCnt",
    #     type=c_uint32,
    #     multiplier=1,
    #     value=0.0,
    #     tag="Errcnt",
    # ),
    # ConfigParamater(
    #     name="Charge_Peak_Current_Occurrence_Count",
    #     id="ChgPeakCurrOccCnt",
    #     type=c_uint32,
    #     multiplier=1,
    #     value=0.0,
    #     tag="Errcnt",
    # ),
    ConfigParamater(
        name="Fet_Temp_Protection_Count",
        id="FetTempProtCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Short_Circuit_Protection_Count",
        id="ShortCktProtCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    # ConfigParamater(
    #     name="Short_Circuit_Latch_Protection_Count",
    #     id="ShortCktLatchProtCnt",
    #     type=c_uint32,
    #     multiplier=1,
    #     value=0.0,
    #     tag="Errcnt",
    # ),
    ConfigParamater(
        name="Cumulative_Charging_Energy",
        id="CummChgEnergy",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Cumulative_Discharging_Energy",
        id="CummDisChgEnergy",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Battery_Charging_Run_Seconds",
        id="BattChgRunSec",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Battery_Discharging_Run_Seconds",
        id="BattDisChgRunSec",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Battery_Charging_Run_Hours",
        id="BattChgRunHrs",
        type=c_uint32,
        multiplier=1,
        value=1.0,
    ),
    ConfigParamater(
        name="Battery_Discharging_Run_Hours",
        id="BattDisChgRunHrs",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Temperature_Seconds_Bucketing_Idle",
        id="TempSecBucketIdle",
        type=c_uint16 * 14,
        multiplier=1,
        value="0043900000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="Temperature_Hours_Bucketing_Idle",
        id="TempHrsBucketIdle",
        type=c_uint16 * 14,
        multiplier=1,
        value="00000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="SOC_Lower_Limit_Bucketing",
        id="TempHrsBucketIdle",
        type=c_uint32 * 10,
        multiplier=1,
        value="00000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="SOC_Higher_Limit_Bucketing",
        id="TempHrsBucketIdle",
        type=c_uint32 * 10,
        multiplier=1,
        value="00000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="Sleep_Status", id="SleepStat", type=c_uint32, multiplier=1, value=0.0
    ),
    ConfigParamater(
        name="Immobilize_Status", id="ImmoStat", type=c_uint32, multiplier=1, value=78.0
    ),
    ConfigParamater(
        name="Immobilize_Charge", id="ImmoChg", type=c_uint16, multiplier=1, value=78.0
    ),
    ConfigParamater(
        name="Immobilize_Discharge",
        id="ImmoDchg",
        type=c_uint16,
        multiplier=1,
        value=78.0,
    ),
    ConfigParamater(
        name="Current_Bucketing_Seconds",
        id="CurrBucketSec",
        type=c_uint16 * 42,
        multiplier=1,
        value="0000000000000000000023220700000000000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="AFE_Over_Temp_Protection_Count",
        id="AFEOverTempProtCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Cell_Temp_Fault",
        id="CellTempFault",
        type=c_uint32,
        multiplier=1,
        value=0.0,
        tag="Errcnt",
    ),
    ConfigParamater(
        name="Pcb_Over_Temp_Protection_Count",
        id="PcbOverTempProtCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Current_Sensor_Fault_Count",
        id="CurrSensFaultCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="AFE_Fail_Protection_Occurrence_Count",
        id="AFEFailProtOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Memory_Fail_Protection_Occurrence_Count",
        id="MemFailProtOccCnt",
        type=c_uint32,
        multiplier=1,
        value=1.0,
    ),
    ConfigParamater(
        name="CFET_Fail_Occurance_Count",
        id="CFETFailOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="DFET_Fail_Occurance_Count",
        id="DFETFailOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Reference_Voltage_Fail_Protection_Occurrence_Count",
        id="RefVoltFailProtOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Fail_Protection_Occurrence_Count_3V3",
        id="FailProtOccCnt3V3",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Fail_Protection_Occurrence_Count_5V",
        id="FailProtOccCnt5V",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Fail_Protection_Occurrence_Count_12V",
        id="FailProtOccCnt12V",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="AFE_Reg18_Volt_Protection_Occurrence_Count",
        id="AFEReg18VoltProtOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Thermal_Runaway_Protection_Occurrence_Count",
        id="ThermRunProtOccCnt",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Temperature_Deviation_Protection_Occurrence_Count",
        id="TempDevProtOccCnt",
        type=c_uint32,
        multiplier=1,
        value=3.0,
    ),
    ConfigParamater(
        name="Cell_Under_Voltage_Safety_Occurence_Count",
        id="CellUndVoltSafeOccCnt",
        type=c_uint32,
        multiplier=1,
        value=4.0,
    ),
    ConfigParamater(
        name="Cell_Over_Voltage_Safety_Occurence_Count",
        id="CellOverVoltSafeOccCnt",
        type=c_uint32,
        multiplier=1,
        value=1.0,
    ),
    ConfigParamater(
        name="Current_Bucketing_Hours",
        id="CurrBucketHrs",
        type=c_uint16 * 42,
        multiplier=1,
        value="000000000000000000000000000000000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="Temperature_Seconds_Bucketing_Charging",
        id="TempSecBucketChg",
        type=c_uint16 * 14,
        multiplier=1,
        value="00000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="Temperature_Hours_Bucketing_Charging",
        id="TempHrsBucketChg",
        type=c_uint16 * 14,
        multiplier=1,
        value="00000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="Temperature_Seconds_Bucketing_Discharging",
        id="TempSecBucketDisChg",
        type=c_uint16 * 14,
        multiplier=1,
        value="00000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="Temperature_Hours_Bucketing_Discharging",
        id="TempHrsBucketDisChg",
        type=c_uint16 * 14,
        multiplier=1,
        value="00000000000000",
        type_of_type=int,
    ),
    ConfigParamater(
        name="Battery_Idle_Run_Seconds",
        id="BattIdleRunSec",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="Battery_Idle_Run_Hours",
        id="BattIdleRunHrs",
        type=c_uint32,
        multiplier=1,
        value=0.0,
    ),
    ConfigParamater(
        name="BMS_Serial_No",
        id="BMSSerNo",
        type=c_uint8 * 15,
        multiplier=1,
        value="000000000000000",
    ),
    ConfigParamater(
        name="Battery_Serial_No",
        id="BattSerNo",
        type=c_uint8 * 15,
        multiplier=1,
        value="BATTERY40AH0002",
    ),
    ConfigParamater(
        name="Immobilize_Discharging",
        id="ImmoDisChg",
        type=c_uint8,
        multiplier=1,
        value=78.0,
    ),
    ConfigParamater(name="Date_DD", id="DD", type=c_uint8, multiplier=1, value=0.0),
    ConfigParamater(name="Date_MM", id="MM", type=c_uint8, multiplier=1, value=0.0),
    ConfigParamater(name="Date_YY", id="YY", type=c_uint8, multiplier=1, value=0.0),
    ConfigParamater(name="Time_HH", id="HH", type=c_uint8, multiplier=1, value=0.0),
    ConfigParamater(name="Time_MM", id="MM", type=c_uint8, multiplier=1, value=0.0),
]


class WarrantyConfig(BigEndianStructure):
    _pack_ = 1
    _fields_ = [(param.id, param.type) for param in params]


class WarrantyConfigUnion(Union):
    _fields_ = [("aData", c_uint8 * sizeof(WarrantyConfig)), ("sData", WarrantyConfig)]


def populate_union_from_json_warranty(
    warranty_config: WarrantyConfigUnion, config: dict = {}
):
    """
    Populates a warranty configuration object with values from a dictionary using specific
    parameters.

    :param warranty_config: holds warranty configuration data.
    :param config: dictionary that contains configuration data
    """
    for param in params:
        value = config.get(param.id, param.value)
        # Check for list of values
        if hasattr(param.type, "_length_"):
            for idx in range(0, param.type._length_):
                getattr(warranty_config.sData, param.id)[idx] = ord(value[idx])
            continue

        # Set single position values
        value = int(float(value) * param.multiplier)
        setattr(warranty_config.sData, param.id, value)
    return warranty_config


def convert_union_to_json_warranty(battConfig, config, key_is_id=False):
    """
    Converts battery configuration parameters to JSON format and calculates the difference
    between limit and recovery parameters.

    :param battConfig: It is a variable that contains some battery configuration data
    :param config: It is a dictionary that will be populated with configuration parameters extracted
    from the battConfig object
    """
    # for i in battConfig.aData:
    #     print(i)

    for idx, param in enumerate(params):
        param_name = param.id if key_is_id else param.name if param.name else param.id
        if param.tag not in config:
            config[param.tag] = {}
        if hasattr(param.type, "_length_"):
            if param.type_of_type is int:
                config[param.tag][param_name] = "".join(
                    [str(value) for value in getattr(battConfig.sData, param.id)]
                )
            else:
                config[param.tag][param_name] = "".join(
                    [chr(char) for char in getattr(battConfig.sData, param.id)]
                )
            continue
        config[param.tag][param_name] = getattr(battConfig.sData, param.id) / float(
            param.multiplier
        )


def AlignBytes(battConfig):
    """
    The function AlignBytes swaps the byte order of certain parameters in a battConfig object to ensure
    proper alignment.

    :param battConfig: It is a variable that holds some battery configuration data
    """
    for param in params:
        if hasattr(param.type, "_length_"):
            continue
        if sizeof(param.type) == 2:
            value = swap16(getattr(battConfig.sData, param.id))
            setattr(battConfig.sData, param[0], value)
        elif sizeof(param.type) == 4:
            value = swap32(getattr(battConfig.sData, param.id))
            setattr(battConfig.sData, param.id, value)
