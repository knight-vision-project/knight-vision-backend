import can
import time
import queue
import threading
from datetime import datetime

from can_interaction.event_handler import EventHandler
from utils.can_singleton import CanBusSingleton


class MessageListener(threading.Thread):
    """
    Thread class for listening to CAN messages.
    """

    def __init__(
        self,
        bus: can.BusABC,
        event_handler: EventHandler,
        message_queue: queue.Queue,
        config_queue: queue.Queue,
        emulator_config_queue: queue.Queue,
        analytical_data_queue: queue.Queue,
        running: threading.Event,
        pause_event: threading.Event,
        msg_list: list,
    ) -> None:
        """
        This is the initialization function for a threading class
        that takes in a CAN bus and message queue and sets up
        events for running and pausing.

        :param bus: The "bus" parameter is referring to a CAN (Controller
        Area Network) bus

        :param message_queue: The message_queue parameter is a queue object
        that is used to store messages that need to be sent over the bus. The
        thread will continuously check this queue for new messages and send them
        over the bus when they are available

        :param config_queue: The config parameters are added to this queue
        """
        super().__init__()
        self.bus = bus
        self.event_handler = event_handler
        self.config_id_limit = 0x700
        self.message_queue = message_queue
        self.config_queue = config_queue
        self.emulator_config_queue = emulator_config_queue
        self.analytical_data_queue = analytical_data_queue
        self.running = running
        self.pause_event = pause_event
        self.messages = {}  # Store IDs for live data
        self.msg_list = msg_list
        self.currtime = time.time()

    def run(self) -> None:
        """
        This function runs a loop that receives messages from a CAN bus
        and puts them in a respective queue, while handling any CAN
        errors that occur.
        """
        self.running.set()
        self.pause_event.clear()
        if not self.bus:
            self.reconnect()
        while self.running.is_set():
            try:
                self.pause_event.wait()
                message = self.bus.recv(timeout=3)
                self.pause_event.wait()
                if message:
                    # print(message)
                    if message.arbitration_id in [
                        0x710,
                        0x711,
                        0x712,
                        0x713,
                        0x714,
                        0x715,
                    ]:
                        if time.time() - self.currtime > 0.5:
                            self.event_handler.broadcast_state(can.BusState.ACTIVE)
                            self.currtime = time.time()
                        # print(message)
                        self.config_queue.put(message)
                    elif message.arbitration_id in self.msg_list:
                        if message.is_extended_id:
                            if time.time() - self.currtime > 0.5:
                                self.event_handler.broadcast_state(can.BusState.ACTIVE)
                                self.currtime = time.time()
                            self.message_queue.put(message)
                            self.event_handler.broadcast_messages()
                        elif message.arbitration_id in [0x103]:
                            if time.time() - self.currtime > 0.5:
                                self.event_handler.broadcast_state(can.BusState.ACTIVE)
                                self.currtime = time.time()
                            self.message_queue.put(message)
                            self.event_handler.broadcast_messages()
                    elif message.arbitration_id == 0x501 and list(message.data) == [
                        0x43,
                        0x41,
                        0x4E,
                        0x53,
                        0x54,
                        0x41,
                        0x52,
                        0x54,
                    ]:
                        self.event_handler.broadcast_state(can.BusState.PASSIVE)
                    else:
                        continue
                else:
                    if time.time() - self.currtime > 0.5:
                        self.event_handler.broadcast_state(can.BusState.PASSIVE)
                        self.currtime = time.time()
                    self.event_handler.ini.set()
                    self.event_handler.repeat = 0
                    self.event_handler.log_data = {
                        "Date": datetime.now().date(),
                        "Time": datetime.now().time(),
                    }
                    self.event_handler.messages = dict()
                    pass
                self.pause_event.wait()
            except Exception as e:
                if not e.__str__():
                    continue
                self.event_handler.broadcast_state(can.BusState.ERROR)
                self.event_handler.broadcast_errors(e.__str__())
                print("CAN error occurred: ", e)
                print(">> Reconnecting...")
                self.reconnect()

    def get_message(self) -> None | can.Message:
        """
        This function returns a message received by a bus.
        :return: The `None` on timeout or `~can.Message` object
        Thread process will be paused. Remember to restart the process with
        self.advance() method
        """
        # self.pause_event.clear()
        try:
            message = self.bus.recv(1)
            return message
        except Exception as e:
            print("Error receiving the message: ", e)
            print(">> Reconnecting...")
            self.reconnect()
            return None

    def stop(self) -> None:
        """
        Reset "running" flag to false. Used to stop the thread
        """
        self.running.clear()

    def advance(self) -> None:
        """
        Set both events to true. Restarts paused process
        """
        self.pause_event.set()
        self.running.set()

    def pause(self) -> None:
        """
        Reset flag "pause_event" to false. Pause the process
        """
        self.pause_event.clear()

    def reconnect(self) -> None:
        """
        This function attempts to reconnect to a CAN bus interface using the PCAN
        interface if it is available.
        """
        # Clean up the existing connection

        self.event_handler.broadcast_state("Disconnected")
        CanBusSingleton.shutdown()
        while True:
            self.pause_event.wait()
            try:
                self.bus = CanBusSingleton.get_instance(connect=True)
                if not self.bus:
                    raise ConnectionError("No PCan Connection")
                self.running.set()
                self.event_handler.ini.set()
                self.event_handler.repeat = 0
                self.event_handler.log_data = {
                    "Date": datetime.now().date(),
                    "Time": datetime.now().time(),
                }
                self.event_handler.messages = dict()
                break
            except Exception as e:
                self.event_handler.broadcast_state(can.BusState.ERROR)
                self.event_handler.broadcast_errors(e.__str__())
                time.sleep(2)
                continue
        print("Connected to CAN bus")

    # def connect(self):
    #     """This function opens a connection with the CAN bus."""


# if __name__ == "__main__":
#     message_queue = queue.Queue()
#     config_queue = queue.Queue()
#     bus = can.Bus(channel="PCAN_USBBUS1", interface="pcan")
#     message_listener = MessageListener(bus, message_queue, config_queue)
#     message_listener.start()

#     while True:
#         time.sleep(5)
#         message_listener.pause()
#         print("Paused for 5 seconds")
#         time.sleep(5)
#         msg = can.Message(
#             arbitration_id=0x701,
#             dlc=8,
#             data=bytearray([0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]),
#         )
#         bus.send(msg)
#         message_listener.advance()

#     message_listener.stop()
#     message_listener.join()
#     time.sleep(1)
#     bus.shutdown()

# Converting hex to UTF-8 format
# hex = ''.join('{:02x}'.format(b) for b in msg.data)
# data = [hex[i:i+2] for i in range(0, len(hex), 2)]
# print(data, msg.channel, ' ID: ', '{:08x}'.format(msg.arbitration_id) )
