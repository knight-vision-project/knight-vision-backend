import json
import fnmatch
import pyzipper
from datetime import datetime
from utils.common_utils import new_fw
from can_interaction.config.battery_config import config_format, config_format_old


def config_importer(data: dict) -> dict[dict]:
    """Imports config JSON file from inside zip file if new fw is set else imports it from JSON file.

    Args:
        data (dict): Dictionary containing path to file.

    Returns:
        dict[dict]: Imported config JSON
    """
    try:
        if new_fw.is_set():
            if data["path"][-4:] == "json":
                new_fw.clear()
                return config_importer(data)
            with pyzipper.AESZipFile(data["path"], "r") as file:
                file.setpassword(b"<9]_dlE#,},p")
                innerfilename = fnmatch.filter(file.namelist(), "*.json")[0]
                try:
                    with file.open(innerfilename, "r") as json_file:
                        config = json.load(json_file)
                except Exception:
                    return
            for i in config:
                if "FW-ID-" + i in config_format:
                    if "value" in config[i]:
                        config_format["FW-ID-" + i]["value"] = config[i]["value"]
                    else:
                        print("Invalid config")
                        return
                else:
                    try:
                        int(i)
                        return
                    except Exception:
                        new_fw.clear()
                        return config_importer(data)
            for i in config_format:
                if (
                    "value" not in config_format[i]
                    and config_format[i]["mandatory?"] == "YES"
                ):
                    config_format[i]["value"] = (
                        float(config_format[i]["default_value"])
                        if config_format[i]["data_type_ct"] == "float"
                        else int(config_format[i]["default_value"])
                    )
            return config_format
        else:
            if data["path"][-3:] == "zip":
                new_fw.set()
                return config_importer(data)

            with open(data["path"], "r") as file:
                try:
                    config = json.load(file)
                except Exception:
                    return

            dic = {
                "cellOvpRecov": ["FW-ID-1", "-"],
                "cellOvwRecov": ["FW-ID-4", "-"],
                "cellUvpRecov": ["FW-ID-7", "+"],
                "cellUvwRecov": ["FW-ID-10", "+"],
                "cellUnbalanceRecov": ["FW-ID-13", "-"],
                "cellBalanceRecov": ["FW-ID-16", "-"],
                "cellBalStartRecov": ["FW-ID-19", "-"],
                "chargeOtpRecov": ["FW-ID-22", "-"],
                "chargeOtwRecov": ["FW-ID-25", "-"],
                "dischOtpRecov": ["FW-ID-28", "-"],
                "dischOtwRecov": ["FW-ID-31", "-"],
                "chargeUtpRecov": ["FW-ID-34", "+"],
                "chargeUtwRecov": ["FW-ID-37", "+"],
                "dischUtpRecov": ["FW-ID-40", "+"],
                "dischUtwRecov": ["FW-ID-43", "+"],
                "FetTempProtRecov": ["FW-ID-46", "-"],
                "lowSocWarnRecov": ["FW-ID-49", "+"],
                "packOvpRecov": ["FW-ID-51", "-"],
                "packOvwRecov": ["FW-ID-54", "-"],
                "packUvpRecov": ["FW-ID-57", "+"],
                "packUvwRecov": ["FW-ID-60", "+"],
            }
            for i in config_format_old:
                if "export_name" in config_format_old[i]:
                    if config_format_old[i]["export_name"] in config:
                        if config_format_old[i]["export_name"][-5:] == "Recov":
                            if dic[config_format_old[i]["export_name"]][1] == "+":
                                config_format_old[i]["value"] = "{:.3f}".format(
                                    config_format_old[
                                        dic[config_format_old[i]["export_name"]][0]
                                    ]["value"]
                                    + config[config_format_old[i]["export_name"]]
                                )
                            else:
                                config_format_old[i]["value"] = "{:.3f}".format(
                                    config_format_old[
                                        dic[config_format_old[i]["export_name"]][0]
                                    ]["value"]
                                    - config[config_format_old[i]["export_name"]]
                                )
                        else:
                            config_format_old[i]["value"] = config[
                                config_format_old[i]["export_name"]
                            ]
                    else:
                        new_fw.set()
                        result = config_importer(data)
                        if result is not None:
                            return result
                        else:
                            new_fw.clear()
                            # print(config_format_old[i]["export_name"])
                            # print("Wrong config")
                            return
            chg_lis = [j for i in config["ChargingProfileTable"] for j in i]
            for i in range(20):
                config_format_old["FW-ID-" + str(i + 89)]["value"] = chg_lis[i]
            return config_format_old
    except Exception:
        return


def config_exporter(
    data: dict[str, dict[str, dict[str, dict | int | str]]], bin_data: bytearray
) -> bool:
    """Exports Config in JSON and bin format

    Args:
        data (dict[dict[dict[dict]]]): Config JSON
        bin_data (bytearray): Config in bytearray format

    Returns:
        bool: Failure or Success
    """
    try:
        if new_fw.is_set():
            path, data = data[0], data[1]
            cver = ""
            json_data = {}
            for i in data:
                for j in data[i]:
                    for k in data[i][j]:
                        if k not in ["Delay", "Recovery"]:
                            if k[-2:] == "86":
                                if (
                                    "value" in data[i][j][k]
                                    and data[i][j][k]["value"] != ""
                                ):
                                    cver = data[i][j][k]["value"]
                                else:
                                    cver = data[i][j][k]["default_value"]

                            if (
                                "value" in data[i][j][k]
                                and data[i][j][k]["value"] != ""
                            ):
                                if data[i][j][k]["data_type_ct"] != "string":
                                    json_data[k[6:]] = {
                                        "name": data[i][j][k]["parameter_name"],
                                        "value": int(
                                            data[i][j][k]["value"]
                                            * int(data[i][j][k]["ct_to_fw_gain"])
                                        ),
                                    }
                                else:
                                    json_data[k[6:]] = {
                                        "name": data[i][j][k]["parameter_name"],
                                        "value": data[i][j][k]["value"]
                                        * int(data[i][j][k]["ct_to_fw_gain"]),
                                    }
                            else:
                                if data[i][j][k]["data_type_ct"] != "string":
                                    json_data[k[6:]] = {
                                        "name": data[i][j][k]["parameter_name"],
                                        "value": int(
                                            float(data[i][j][k]["default_value"])
                                            * int(data[i][j][k]["ct_to_fw_gain"])
                                        ),
                                    }
                                else:
                                    json_data[k[6:]] = {
                                        "name": data[i][j][k]["parameter_name"],
                                        "value": data[i][j][k]["default_value"]
                                        * int(data[i][j][k]["ct_to_fw_gain"]),
                                    }
                            if "delay" in data[i][j][k]:
                                if (
                                    "value" in data[i][j][k]["delay"]
                                    and data[i][j][k]["delay"]["value"] != ""
                                ):
                                    json_data[data[i][j][k]["delay"]["FW-ID"][6:]] = {
                                        "name": data[i][j][k]["delay"][
                                            "parameter_name"
                                        ],
                                        "value": int(
                                            data[i][j][k]["delay"]["value"]
                                            * int(
                                                data[i][j][k]["delay"]["ct_to_fw_gain"]
                                            )
                                        ),
                                    }
                                else:
                                    json_data[data[i][j][k]["delay"]["FW-ID"][6:]] = {
                                        "name": data[i][j][k]["delay"][
                                            "parameter_name"
                                        ],
                                        "value": int(
                                            float(
                                                data[i][j][k]["delay"]["default_value"]
                                            )
                                            * int(
                                                data[i][j][k]["delay"]["ct_to_fw_gain"]
                                            )
                                        ),
                                    }
                            if "recovery" in data[i][j][k]:
                                if (
                                    "value" in data[i][j][k]["recovery"]
                                    and data[i][j][k]["recovery"]["value"] != ""
                                ):
                                    json_data[
                                        data[i][j][k]["recovery"]["FW-ID"][6:]
                                    ] = {
                                        "name": data[i][j][k]["recovery"][
                                            "parameter_name"
                                        ],
                                        "value": int(
                                            data[i][j][k]["recovery"]["value"]
                                            * int(
                                                data[i][j][k]["recovery"][
                                                    "ct_to_fw_gain"
                                                ]
                                            )
                                        ),
                                    }
                                else:
                                    json_data[
                                        data[i][j][k]["recovery"]["FW-ID"][6:]
                                    ] = {
                                        "name": data[i][j][k]["recovery"][
                                            "parameter_name"
                                        ],
                                        "value": int(
                                            float(
                                                data[i][j][k]["recovery"][
                                                    "default_value"
                                                ]
                                            )
                                            * int(
                                                data[i][j][k]["recovery"][
                                                    "ct_to_fw_gain"
                                                ]
                                            )
                                        ),
                                    }
            fileName = (
                "config-"
                + cver
                + "-"
                + datetime.now().strftime("%d-%m-%Y-%H-%M-%S")
                + ".zip"
            )
            json_data = {
                i: json_data[i] for i in sorted(json_data.keys(), key=lambda x: int(x))
            }
            hexstr = ""
            for i in bin_data:
                hexstr += f"{i:02x}"
            with pyzipper.AESZipFile(
                path + "\\" + fileName,
                "w",
                compression=pyzipper.ZIP_LZMA,
                encryption=pyzipper.WZ_AES,
            ) as file:
                file.setpassword(b"<9]_dlE#,},p")

                # Create a JSON string directly and write it to the zip archive
                file.writestr(f"config-{cver}.json", json.dumps(json_data, indent=2))
                # file.writestr("config.json", json.dumps(json_data, indent=2))
                file.writestr(f"config-{cver}.hex", hexstr)
                file.writestr(f"config-{cver}.bin", bin_data)
            return True
        else:
            path, data = data[0], data[1]
            json_data = {}
            for i in data:
                for j in data[i]:
                    for k in data[i][j]:
                        if k not in ["Delay", "Recovery"]:
                            if k[-2:] == "86":
                                cver = data[i][j][k]["value"]
                            if "export_name" in data[i][j][k]:
                                json_data[data[i][j][k]["export_name"]] = data[i][j][k][
                                    "value"
                                ]
                            if "delay" in data[i][j][k]:
                                if "export_name" in data[i][j][k]["delay"]:
                                    json_data[data[i][j][k]["delay"]["export_name"]] = (
                                        data[i][j][k]["delay"]["value"]
                                    )
                            if "recovery" in data[i][j][k]:
                                if "export_name" in data[i][j][k]["recovery"]:
                                    json_data[
                                        data[i][j][k]["recovery"]["export_name"]
                                    ] = float(
                                        "{:0.3f}".format(
                                            abs(
                                                data[i][j][k]["value"]
                                                - data[i][j][k]["recovery"]["value"]
                                            )
                                        )
                                    )
            charge_arr = [[], [], [], [], []]
            j = 0
            for i in data[""]["7._Charging_Profile_Table"]:
                charge_arr[j].append(data[""]["7._Charging_Profile_Table"][i]["value"])
                charge_arr[j].append(
                    data[""]["7._Charging_Profile_Table"][i]["beta"]["value"]
                )
                charge_arr[j].append(
                    data[""]["7._Charging_Profile_Table"][i]["gamma"]["value"]
                )
                charge_arr[j].append(
                    data[""]["7._Charging_Profile_Table"][i]["delta"]["value"]
                )
                j += 1

            json_data["ChargingProfileTable"] = charge_arr
            fileName = (
                "config-" + cver + "-" + datetime.now().strftime("%d-%m-%Y-%H-%M-%S")
            )
            hexstr = ""
            for i in bin_data:
                hexstr += f"{i:02x}"
            with open(
                path + "\\" + fileName + ".json",
                "w",
            ) as file:
                # Create a JSON string directly and write it to the JSON file
                json.dump(json_data, file, indent=2)
            with open(
                path + "\\" + fileName + ".hex",
                "w",
            ) as file:
                file.write(hexstr)
            with open(path + "\\" + fileName + ".bin", "wb") as bin_file:
                bin_file.write(bin_data)
            return True
    except Exception as e:
        print(e)
        return False
