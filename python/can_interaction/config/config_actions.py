import json
import sqlite3
from datetime import datetime
from utils.common_utils import new_fw, is_bundled, sio
from can_interaction.airtable_integration import download_from_airtable


def fetch_schema() -> None:
    """Downloads schema from Airtable, Changes its formatting and stores it in local database

    Raises:
        FileNotFoundError: Raised when there is no active internet connection
        ValueError: Raised when there is an issue with airtable schema
    """
    try:
        try:
            downloaded_json_data = download_from_airtable()
            # print(downloaded_json_data["FW-ID-112"])
        except Exception:
            raise FileNotFoundError(
                "2No Internet : Config Wizard may be outdated. Please restart with an active internet connection."
            )
        try:
            config_validator(downloaded_json_data)
        except Exception:
            raise ValueError(
                "3Config Wizard Outdated/invalid, please contact Lime Support."
            )
        conn = sqlite3.connect(
            "./resources/app/data.db" if is_bundled() else "./data.db"
        )
        curr = conn.cursor()
        curr.execute("""
            CREATE TABLE IF NOT EXISTS req_data (
                id TEXT PRIMARY KEY,
                data TEXT
            )
        """)
        curr.execute(
            """
                INSERT OR REPLACE INTO req_data (id, data)
                VALUES (?, ?)
            """,
            ("config", json.dumps(downloaded_json_data)),
        )
        curr.execute(
            """
                INSERT OR REPLACE INTO req_data (id, data)
                VALUES (?, ?)
            """,
            ("last_updated", datetime.now().strftime("%Y/%m/%d %H:%M:%S")),
        )
        conn.commit()
        curr.close()
        conn.close()
        sio.emit(
            "message_rerouter",
            {
                "target": "jsroom",
                "handler": "schema_status",
                "data": {"code": 1, "message": "Config Wizard up to date"},
            },
        )
    except Exception as e:
        sio.emit(
            "message_rerouter",
            {
                "target": "jsroom",
                "handler": "schema_status",
                "data": {"code": int(f"{e}"[0]), "message": f"{e}"[1:]},
            },
        )
        print("error 43", e)
        conn = sqlite3.connect(
            "./resources/app/data.db" if is_bundled() else "./data.db"
        )
        curr = conn.cursor()

        # Execute a SELECT query to fetch the JSON data
        curr.execute("SELECT data FROM req_data WHERE id = ?", ("config",))
        row = curr.fetchone()

        # Check if data exists and convert the JSON string back to a Python dictionary
        if row:
            json_data_str = row[0]
            downloaded_json_data = json.loads(json_data_str)
        else:
            print("No data found in the config table.")
        conn.close()

    rearranged_config = rearrange_config(downloaded_json_data)
    conn = sqlite3.connect("./resources/app/data.db" if is_bundled() else "./data.db")
    curr = conn.cursor()
    curr.execute("""
        CREATE TABLE IF NOT EXISTS req_data (
            id TEXT PRIMARY KEY,
            data TEXT
        )
    """)
    curr.execute(
        """
            INSERT OR REPLACE INTO req_data (id, data)
            VALUES (?, ?)
        """,
        ("rearranged_config", json.dumps(rearranged_config)),
    )
    conn.commit()
    curr.close()
    conn.close()


def config_validator(config: dict[dict]) -> None:
    """Validates the downloaded schema and raises error if it is wrong

    Args:
        config (dict): Schema downloaded from airtable
    """
    attributes = [
        "parameter_name",
        "category",
        "subcategory",
        "CT-ID",
        "data_type_ct",
        "unit",
        "resolution",
        "min_value",
        "max_value",
        "default_value",
        "mandatory?",
        "data_type_fw",
        "length_fw_array",
        "ct_to_fw_gain",
        "documentation_link",
    ]
    for id in range(1, len(config.keys()) + 1):
        for attr in attributes:
            # print(attr)
            config["FW-ID-" + str(id)][attr]


def rearrange_config(config: dict[dict], impot: bool = False) -> dict[dict[dict[dict]]]:
    """Rearranges config into a form different from the schema downloaded from Airtable or imported config
    into something usable by the app.

    Args:
        config (dict): Config in schema form or import form.
        impot (bool, optional): Check whether it's imported config. Defaults to False.

    Returns:
        dict: Rearranged dictionary
    """
    headers = {}
    if new_fw.is_set():
        for i in config:
            if config[i]["category"] in headers:
                if config[i]["subcategory"] in headers[config[i]["category"]]:
                    headers[config[i]["category"]][config[i]["subcategory"]][i] = (
                        config[i]
                    )
                else:
                    headers[config[i]["category"]][config[i]["subcategory"]] = {
                        i: config[i]
                    }
            else:
                tpdict = {}
                tpdict[config[i]["subcategory"]] = {i: config[i]}
                headers[config[i]["category"]] = tpdict

        headers: dict[str, dict[str, dict[str, dict[str, str]]]] = {
            i: headers[i] for i in sorted(list(headers.keys()))
        }
        for cat in headers:
            headers[cat] = {
                i: headers[cat][i] for i in sorted(list(headers[cat].keys()))
            }
            for sub in headers[cat]:
                headers[cat][sub] = {
                    i: headers[cat][sub][i]
                    for i in sorted(
                        list(headers[cat][sub].keys()),
                        key=lambda x: int(headers[cat][sub][x]["CT-ID"].split(".")[-1]),
                    )
                }
        delkeys = []
        for i in headers:
            for j in headers[i]:
                b = False
                for k in headers[i][j]:
                    if headers[i][j][k]["data_type_ct"] != "string":
                        if (
                            "value" in headers[i][j][k]
                            and headers[i][j][k]["value"] != ""
                        ):
                            headers[i][j][k]["value"] = float(
                                headers[i][j][k]["value"]
                            ) / int(headers[i][j][k]["ct_to_fw_gain"])
                    if headers[i][j][k]["parameter_name"].split("_")[-1] == "Delay":
                        b = True
                        name = (
                            "_".join(headers[i][j][k]["parameter_name"].split("_")[:-1])
                            + "_Limit"
                        )
                        for var in headers[i][j]:  # noqa: E741
                            if headers[i][j][var]["parameter_name"] == name:
                                headers[i][j][var]["delay"] = headers[i][j][k]
                                headers[i][j][var]["delay"]["FW-ID"] = k
                                b = True
                                delkeys.append(k)
                                break

                while len(delkeys) > 0:
                    del headers[i][j][delkeys.pop()]
                if b is True:
                    headers[i][j]["Delay"] = True

        for i in headers:
            for j in headers[i]:
                b = False
                for k in headers[i][j]:
                    if (
                        k != "Delay"
                        and headers[i][j][k]["parameter_name"].split("_")[-1]
                        == "Recovery"
                    ):
                        b = True
                        name = (
                            "_".join(headers[i][j][k]["parameter_name"].split("_")[:-1])
                            + "_Limit"
                        )
                        for var in headers[i][j]:  # noqa: E741
                            if var != "Delay":
                                if headers[i][j][var]["parameter_name"] == name:
                                    headers[i][j][var]["recovery"] = headers[i][j][k]
                                    headers[i][j][var]["recovery"]["FW-ID"] = k
                                    b = True
                                    delkeys.append(k)
                                    break
                        if k not in delkeys:
                            name = (
                                "_".join(
                                    headers[i][j][k]["parameter_name"].split("_")[:-1]
                                )
                                + "_Delay"
                            )
                            for var in headers[i][j]:  # noqa: E741
                                if var != "Delay":
                                    if headers[i][j][var]["parameter_name"] == name:
                                        headers[i][j][var]["recovery"] = headers[i][j][
                                            k
                                        ]
                                        headers[i][j][var]["recovery"]["FW-ID"] = k
                                        b = True
                                        delkeys.append(k)
                                        break
                while len(delkeys) > 0:
                    del headers[i][j][delkeys.pop()]
                if b is True:
                    headers[i][j]["Recovery"] = True
    else:
        for i in config:
            if config[i]["category"] in headers:
                if config[i]["subcategory"] in headers[config[i]["category"]]:
                    headers[config[i]["category"]][config[i]["subcategory"]][i] = (
                        config[i]
                    )
                else:
                    headers[config[i]["category"]][config[i]["subcategory"]] = {
                        i: config[i]
                    }
            else:
                tpdict = {}
                tpdict[config[i]["subcategory"]] = {i: config[i]}
                headers[config[i]["category"]] = tpdict

        headers = {i: headers[i] for i in sorted(list(headers.keys()))}
        for cat in headers:
            headers[cat] = {
                i: headers[cat][i] for i in sorted(list(headers[cat].keys()))
            }
            for sub in headers[cat]:
                headers[cat][sub] = {
                    i: headers[cat][sub][i]
                    for i in sorted(
                        list(headers[cat][sub].keys()),
                        key=lambda x: int(headers[cat][sub][x]["CT-ID"].split(".")[-1]),
                    )
                }
        delkeys = []
        for i in headers:
            for j in headers[i]:
                b = False
                for k in headers[i][j]:
                    if headers[i][j][k]["data_type_ct"] != "string":
                        if (
                            "value" in headers[i][j][k]
                            and headers[i][j][k]["value"] != ""
                        ):
                            if not impot:
                                headers[i][j][k]["value"] = float(
                                    headers[i][j][k]["value"]
                                ) / int(headers[i][j][k]["ct_to_fw_gain"])
                            else:
                                headers[i][j][k]["value"] = float(
                                    headers[i][j][k]["value"]
                                )
                    if headers[i][j][k]["parameter_name"].split("_")[-1] == "Delay":
                        b = True
                        name = (
                            "_".join(headers[i][j][k]["parameter_name"].split("_")[:-1])
                            + "_Limit"
                        )
                        for var in headers[i][j]:  # noqa: E741
                            if headers[i][j][var]["parameter_name"] == name:
                                headers[i][j][var]["delay"] = headers[i][j][k]
                                headers[i][j][var]["delay"]["FW-ID"] = k
                                b = True
                                delkeys.append(k)
                                break

                while len(delkeys) > 0:
                    del headers[i][j][delkeys.pop()]
                if b is True:
                    headers[i][j]["Delay"] = True

        for i in headers:
            for j in headers[i]:
                b = False
                for k in headers[i][j]:
                    if (
                        k != "Delay"
                        and headers[i][j][k]["parameter_name"].split("_")[-1]
                        == "Recovery"
                    ):
                        b = True
                        name = (
                            "_".join(headers[i][j][k]["parameter_name"].split("_")[:-1])
                            + "_Limit"
                        )
                        for var in headers[i][j]:  # noqa: E741
                            if var != "Delay":
                                if headers[i][j][var]["parameter_name"] == name:
                                    headers[i][j][var]["recovery"] = headers[i][j][k]
                                    headers[i][j][var]["recovery"]["FW-ID"] = k
                                    b = True
                                    delkeys.append(k)
                                    break
                        if k not in delkeys:
                            name = (
                                "_".join(
                                    headers[i][j]["parameter_name"].split("_")[:-1]
                                )
                                + "_Delay"
                            )
                            for var in headers[i][j]:  # noqa: E741
                                if var != "Delay":
                                    if headers[i][j][var]["parameter_name"] == name:
                                        headers[i][j][var]["recovery"] = headers[i][j][
                                            k
                                        ]
                                        headers[i][j][var]["recovery"]["FW-ID"] = k
                                        b = True
                                        delkeys.append(k)
                                        break
                while len(delkeys) > 0:
                    del headers[i][j][delkeys.pop()]
                if b is True:
                    headers[i][j]["Recovery"] = True
        for k in headers[""]["7._Charging_Profile_Table"]:
            if (
                k != "Delay"
                and k != "Recovery"
                and headers[""]["7._Charging_Profile_Table"][k]["parameter_name"].split(
                    "_"
                )[-1]
                == "table"
                and headers[""]["7._Charging_Profile_Table"][k]["parameter_name"].split(
                    "_"
                )[1]
                != "alpha"
            ):
                name = (
                    headers[""]["7._Charging_Profile_Table"][k]["parameter_name"].split(
                        "_"
                    )[0]
                    + "_alpha"
                    + "_table"
                )
                for var in headers[""]["7._Charging_Profile_Table"]:
                    if (
                        headers[""]["7._Charging_Profile_Table"][var]["parameter_name"]
                        == name
                    ):
                        headers[""]["7._Charging_Profile_Table"][var][
                            headers[""]["7._Charging_Profile_Table"][k][
                                "parameter_name"
                            ].split("_")[1]
                        ] = headers[""]["7._Charging_Profile_Table"][k]
                        # headers[i][j][var][
                        #     headers[i][j][k]["parameter_name"].split("_")[1]
                        # ]["FW-ID"] = k
                        delkeys.append(k)
                        break
        while len(delkeys) > 0:
            del headers[""]["7._Charging_Profile_Table"][delkeys.pop()]
    return headers
