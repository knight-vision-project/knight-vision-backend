import can
import math
import time
from ctypes import sizeof, c_ubyte
from datetime import datetime
from utils.common_utils import config_queue, new_fw
from utils.can_singleton import CanBusSingleton
from can_interaction.config.battery_config import (
    BatteryConfigUnion,
    ConfigToJson,
    BatteryConfig,
    populate_union_from_json,
    BatteryConfigUnionOld,
)


class ConfigOperations:
    """Handles operations of all configurations"""

    def __init__(
        self,
        set_id: hex,
        get_id: hex,
        old=False,
    ) -> None:
        self.set_id = set_id
        self.get_id = get_id
        self.data_length = sizeof(BatteryConfig()) if not old else 400
        self.battery_config = BatteryConfigUnion()
        self.battery_config_old = BatteryConfigUnionOld()
        self.signal_id = 0x701
        self.send_id = 0x702
        self.confirmation_id = 0x710
        self.timeout = 3
        self.bus = CanBusSingleton.get_instance()
        self.old = old

    def read_message(self) -> can.Message | None:
        """Return message from config queue. None in case of error."""
        try:
            msg = config_queue.get(timeout=self.timeout)
            return msg
        except Exception:
            return None

    def message_sender(self, arbitration_id: int, data: list[int]) -> bool:
        """Sends message  on bus with given parameters"""
        message = can.Message(
            is_extended_id=False,
            arbitration_id=arbitration_id,
            dlc=8,
            data=(c_ubyte * 8)(*data),
        )
        try:
            self.bus.send(message)
            return True
        except Exception as e:
            print(f"Error sending config message: {e}")
            return False

    def send_signal(self, get_data: bool, ser: bool = False) -> bool:
        """Send signal to CAN."""
        try:
            self.bus.reset()
        except Exception:
            pass
        if not ser:
            sid = self.get_id if get_data else self.set_id
        else:
            sid = 0xE2 if new_fw.is_set() else 0xE1
        msb = 0x00 if get_data else int(f"{self.data_length:04x}"[:2], 16)
        lsb = 0x00 if get_data else int(f"{self.data_length:04x}"[2:], 16)
        return self.message_sender(
            self.signal_id, [sid, msb, lsb, 0x00, 0x00, 0x00, 0x00, 0x00]
        )

    def confirm_response(self, get_data: bool) -> int:
        """Waits for confirmation from CAN."""
        curr = time.time()
        while time.time() - curr < self.timeout:
            message = self.read_message()
            if not message:
                continue
            if message.arbitration_id == self.confirmation_id and message.data[0] == (
                self.get_id if get_data else self.set_id
            ):
                data_length = message.data[1] << 8 | message.data[2]
                print(
                    time.time(),
                    "Received response, reading data of length ",
                    data_length,
                )
                return data_length
        print(
            f'No response from CAN while confirming response {"get_data" if get_data else "set_data"}'
        )
        return -1

    def collect_base_config(self, config_length: int) -> dict[dict]:
        """Return JSON of a base config"""
        try:
            start_time = datetime.now()
            message_number = 0
            while (datetime.now() - start_time).total_seconds() < self.timeout:
                message = self.read_message()
                if not message:
                    continue  # noqa: E701
                if message.arbitration_id == 0x711:
                    if message.data[0] == math.ceil(config_length / 7) - 1:
                        if new_fw.is_set():
                            self.battery_config.aData[
                                7 * message.data[0] : 7 * message.data[0]
                                + config_length % 7
                            ] = message.data[1 : config_length % 7 + 1]
                        else:
                            self.battery_config_old.aData[
                                7 * message.data[0] : 7 * message.data[0]
                                + config_length % 7
                            ] = message.data[1 : config_length % 7 + 1]
                    else:
                        if new_fw.is_set():
                            self.battery_config.aData[
                                7 * message.data[0] : 7 * message.data[0] + 7
                            ] = message.data[1:]
                        else:
                            self.battery_config_old.aData[
                                7 * message.data[0] : 7 * message.data[0] + 7
                            ] = message.data[1:]
                    message_number += 1
                if 0 < config_length < 7 * message_number:
                    # AlignBytes(self.battery_config)
                    if new_fw.is_set():
                        config = ConfigToJson(self.battery_config, config_length)
                    else:
                        config = ConfigToJson(self.battery_config_old, config_length)
                    print("GET Config Acknowledged at backend level")
                    return config
            print("Communication timeout........ while collecting base config")
        except Exception:
            return

    def send_data(self, json_data: dict[dict[dict[dict]]]) -> None:
        """Send config data for setting to the CAN."""
        self.battery_config = populate_union_from_json(
            self.battery_config if new_fw.isSet() else self.battery_config_old,
            json_data,
        )
        config = list(self.battery_config.aData)
        for message_number in range(0, math.ceil(self.data_length / 7)):
            data = bytearray(
                [
                    message_number,
                    *config[message_number * 7 : message_number * 7 + 7],
                ]
            )
            self.message_sender(0x702, data)

    def return_bytearray(self, json_data: dict[dict[dict[dict]]]) -> bytearray:
        """Converts JSON config into bytearray

        Args:
            json_data (dict[dict[dict[dict]]]): Config in JSON form

        Returns:
            bytearray: Bytearray created from config
        """
        self.battery_config = populate_union_from_json(
            self.battery_config if new_fw.isSet() else self.battery_config_old,
            json_data,
        )
        return self.battery_config.aData
