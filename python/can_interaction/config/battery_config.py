import json
import ctypes as ctype
from ctypes import (
    BigEndianStructure,
    Union,
    c_uint8,
    sizeof,
)
import sqlite3
from typing import Type
from can_interaction.config.config_actions import fetch_schema
from utils.common_utils import new_fw, is_bundled


class ConfigParamater:
    def __init__(
        self,
        name: str,
        id: str,
        value: float | int,
        multiplier: int = 1,
        type: Type = None,
    ):
        self.name = name
        self.id = id
        self.multiplier = multiplier
        self.type = type
        self.value = value


def swap32(x: int) -> int:
    """
    The function swaps the byte order of a 32-bit integer.

    :param x: The input integer that needs to be swapped
    :return: The function `swap32` takes a 32-bit integer as input and returns the same integer with its
    byte order reversed. Specifically, it swaps the first and fourth bytes, and the second and third
    bytes.
    """
    return (
        ((x << 24) & 0xFF000000)
        | ((x << 8) & 0x00FF0000)
        | ((x >> 8) & 0x0000FF00)
        | ((x >> 24) & 0x000000FF)
    )


def swap16(x: int) -> int:
    """
    The function swaps the positions of the first 8 bits and the second 8 bits in a 16-bit integer.

    :param x: The parameter x is an integer that represents a 16-bit binary number that needs to be
    swapped
    :return: The function `swap16` takes an input `x` and returns the value obtained by swapping the two
    bytes of `x`. Specifically, it shifts the first byte of `x` to the left by 8 bits and masks it with
    `0xFF00` to keep only the first byte, then shifts the second byte of `x` to the right by 8 bits and
    masks it with
    """
    return ((x << 8) & 0xFF00) | ((x >> 8) & 0x00FF)


# Fetching schema from Airtable and storing it.
fetch_schema()
conn = sqlite3.connect("./resources/app/data.db" if is_bundled() else "./data.db")
curr = conn.cursor()

# Execute a SELECT query to fetch the JSON data
curr.execute("SELECT data FROM req_data WHERE id = ?", ("config",))
row = curr.fetchone()
# Check if data exists and convert the JSON string back to a Python dictionary
if row:
    json_data_str = row[0]
    config_format: dict = json.loads(json_data_str)
else:
    print("No data found in the config table.")

curr.execute("SELECT data FROM req_data WHERE id = ?", ("old_config",))
row = curr.fetchone()
if row:
    json_data_str = row[0]
    config_format_old: dict = json.loads(json_data_str)
else:
    print("No data found in the old config table.")
# Close the connection
conn.close()

# Initiating ByteArray for config
config_fields = list()
tp_config_fields = list()
data_size = 0
for idx, item in enumerate(
    sorted(list(config_format.keys()), key=lambda x: int(x[6:]))
):
    name = config_format[item]["parameter_name"]  # config_format[item]['name']
    data_type = (
        getattr(ctype, f"c_{config_format[item]['data_type_fw']}")
        if config_format[item]["data_type_ct"] != "string"
        else getattr(ctype, "c_uint8")
    )
    if config_format[item].get("length_fw_array") != "1":
        data_type = data_type * int(config_format[item]["length_fw_array"])
    data_size += sizeof(data_type)
    tp_config_fields.append((name, data_type, int(item[6:])))

# Sorting Fields
tp_config_fields.sort(key=lambda x: x[2])

for idx, i in enumerate(tp_config_fields):
    config_fields.append(
        (
            i[0],
            i[1],
        )
    )


# Creating class to store fields
class BatteryConfig(BigEndianStructure):
    # _pack_ = 1
    _fields_ = config_fields


# Creating ByteArray from fields
class BatteryConfigUnion(Union):
    _fields_ = [
        ("aData", c_uint8 * ctype.sizeof(BatteryConfig)),
        ("sData", BatteryConfig),
    ]


# Initiating ByteArray for Old Config

config_fields_old = list()
tp_config_fields_old = list()
data_size = 0
# print(config_format_old)
for idx, item in enumerate(
    sorted(list(config_format_old.keys()), key=lambda x: int(x[6:]))
):
    name = config_format_old[item]["parameter_name"]  # config_format[item]['name']
    data_type = (
        getattr(ctype, f"c_{config_format_old[item]['data_type_fw']}")
        if config_format_old[item]["data_type_ct"] != "string"
        else getattr(ctype, "c_uint8")
    )
    if config_format_old[item].get("length_fw_array") != "1":
        data_type = data_type * int(config_format_old[item]["length_fw_array"])
    data_size += sizeof(data_type)
    # print(idx, data_size, name, data_type)
    tp_config_fields_old.append((name, data_type, int(item[6:])))

# Sorting fields
tp_config_fields_old.sort(key=lambda x: x[2])

for idx, i in enumerate(tp_config_fields_old):
    config_fields_old.append(
        (
            i[0],
            i[1],
        )
    )


# Creating class to store fields
class BatteryConfigOld(BigEndianStructure):
    # _pack_ = 1
    _fields_ = config_fields_old


# Creating ByteArray from fields
class BatteryConfigUnionOld(Union):
    _fields_ = [
        ("aData", c_uint8 * 400),
        ("sData", BatteryConfigOld),
    ]


def populate_union_from_json(
    battery_config: BatteryConfigUnion | BatteryConfigUnionOld,
    data: dict[dict[dict[dict]]] = {},
) -> BatteryConfigUnion | BatteryConfigUnionOld:
    """
    Populates a battery configuration object with values from a dictionary using specific
    parameters.

    :param battery_config: holds battery configuration data.
    :param config: dictionary that contains configuration data
    """
    for i in data:
        for j in data[i]:
            for k in data[i][j]:
                if k not in ["Delay", "Recovery"]:
                    if data[i][j][k]["data_type_ct"] != "string":
                        if "value" in data[i][j][k] and data[i][j][k]["value"] != "":
                            setattr(
                                battery_config.sData,
                                data[i][j][k]["parameter_name"],
                                int(
                                    float(data[i][j][k]["value"])
                                    * int(data[i][j][k]["ct_to_fw_gain"])
                                ),
                            )
                        else:
                            setattr(
                                battery_config.sData,
                                data[i][j][k]["parameter_name"],
                                int(
                                    float(data[i][j][k]["default_value"])
                                    * int(data[i][j][k]["ct_to_fw_gain"])
                                ),
                            )
                        if "delay" in data[i][j][k]:
                            if (
                                "value" in data[i][j][k]["delay"]
                                and data[i][j][k]["delay"]["value"] != ""
                            ):
                                setattr(
                                    battery_config.sData,
                                    data[i][j][k]["delay"]["parameter_name"],
                                    int(
                                        float(data[i][j][k]["delay"]["value"])
                                        * int(data[i][j][k]["delay"]["ct_to_fw_gain"])
                                    ),
                                )
                            else:
                                setattr(
                                    battery_config.sData,
                                    data[i][j][k]["delay"]["parameter_name"],
                                    int(
                                        float(data[i][j][k]["delay"]["default_value"])
                                        * int(data[i][j][k]["delay"]["ct_to_fw_gain"])
                                    ),
                                )
                        if "recovery" in data[i][j][k]:
                            if (
                                "value" in data[i][j][k]["recovery"]
                                and data[i][j][k]["recovery"]["value"] != ""
                            ):
                                setattr(
                                    battery_config.sData,
                                    data[i][j][k]["recovery"]["parameter_name"],
                                    int(
                                        float(data[i][j][k]["recovery"]["value"])
                                        * int(
                                            data[i][j][k]["recovery"]["ct_to_fw_gain"]
                                        )
                                    ),
                                )
                            else:
                                setattr(
                                    battery_config.sData,
                                    data[i][j][k]["recovery"]["parameter_name"],
                                    int(
                                        float(
                                            data[i][j][k]["recovery"]["default_value"]
                                        )
                                        * int(
                                            data[i][j][k]["recovery"]["ct_to_fw_gain"]
                                        )
                                    ),
                                )
                        if "beta" in data[i][j][k]:
                            setattr(
                                battery_config.sData,
                                data[i][j][k]["beta"]["parameter_name"],
                                int(
                                    float(data[i][j][k]["beta"]["value"])
                                    * int(data[i][j][k]["beta"]["ct_to_fw_gain"])
                                ),
                            )
                            setattr(
                                battery_config.sData,
                                data[i][j][k]["gamma"]["parameter_name"],
                                int(
                                    float(data[i][j][k]["gamma"]["value"])
                                    * int(data[i][j][k]["gamma"]["ct_to_fw_gain"])
                                ),
                            )
                            setattr(
                                battery_config.sData,
                                data[i][j][k]["delta"]["parameter_name"],
                                int(
                                    float(data[i][j][k]["delta"]["value"])
                                    * int(data[i][j][k]["delta"]["ct_to_fw_gain"])
                                ),
                            )

                    else:
                        if "value" in data[i][j][k] and data[i][j][k]["value"] != "":
                            for idx in range(0, int(data[i][j][k]["length_fw_array"])):
                                getattr(
                                    battery_config.sData,
                                    data[i][j][k]["parameter_name"],
                                )[idx] = ord(data[i][j][k]["value"][idx])
                        else:
                            for idx in range(0, int(data[i][j][k]["length_fw_array"])):
                                getattr(
                                    battery_config.sData,
                                    data[i][j][k]["parameter_name"],
                                )[idx] = ord(data[i][j][k]["default_value"][idx])
    return battery_config


def ConfigToJson(
    battConfig: BatteryConfigUnion | BatteryConfigOld, config_length: int
) -> dict[dict]:
    """Converts Union Battery Config structure to JSON

    Args:
        battConfig (BatteryConfigUnion | BatteryConfigOld): Battery Config in Union structure

    Returns:
        dict: Converted dictionary
    """
    if new_fw.is_set():
        length = 0
        fields = dict(BatteryConfig._fields_)
        add = 0
        for key in config_fields:
            key = key[0]
            for i in config_format:
                if config_format[i]["parameter_name"] == key:
                    key2 = i
                    break
            value = getattr(battConfig.sData, key)
            if length < config_length:
                if config_format[key2].get("length_fw_array") != "1":
                    if config_format[key2]["data_type_ct"] == "string":
                        config_format[key2]["value"] = "".join([chr(i) for i in value])
                        length += sizeof(fields[key])
                        add = (add + sizeof(fields[key])) % 4
                else:
                    config_format[key2]["value"] = value
                    if add + sizeof(fields[key]) > 4:
                        length += sizeof(fields[key]) + 4 - add
                        add = sizeof(fields[key])
                    else:
                        add += sizeof(fields[key])
                        length += sizeof(fields[key])
            elif config_format[key2]["mandatory?"] == "YES":
                config_format[key2]["value"] = (
                    float(config_format[i]["default_value"])
                    if config_format[i]["data_type_ct"] == "float"
                    else int(config_format[i]["default_value"])
                )
        return config_format
    else:
        for key in config_fields_old:
            key = key[0]
            for i in config_format_old:
                if config_format_old[i]["parameter_name"] == key:
                    key2 = i
                    break
            value = getattr(battConfig.sData, key)
            if config_format_old[key2].get("length_fw_array") != "1":
                if config_format_old[key2]["data_type_ct"] == "string":
                    config_format_old[key2]["value"] = "".join([chr(i) for i in value])
                else:
                    config_format_old[key2]["value"] = [i for i in value]
            else:
                config_format_old[key2]["value"] = value
        return config_format_old


def AlignBytes(battConfig: BatteryConfigUnion | BatteryConfigUnionOld) -> None:
    """
    The function AlignBytes swaps the byte order of certain parameters in a battConfig object to ensure
    proper alignment.

    :param battConfig: It is a variable that holds some battery configuration data
    """
    if new_fw.is_set():
        for param in config_fields:
            para = param[0]
            if sizeof(param[1]) == 2:
                value = swap16(getattr(battConfig.sData, para))
                setattr(battConfig.sData, para, value)
            elif sizeof(param[1]) == 4:
                value = swap32(getattr(battConfig.sData, para))
                setattr(battConfig.sData, para, value)
    else:
        for param in config_fields_old:
            para = param[0]
            if sizeof(param[1]) == 2:
                value = swap16(getattr(battConfig.sData, para))
                setattr(battConfig.sData, para, value)
            elif sizeof(param[1]) == 4:
                value = swap32(getattr(battConfig.sData, para))
                setattr(battConfig.sData, para, value)
