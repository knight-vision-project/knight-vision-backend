import can
import csv
import time
import math
from queue import Queue
from ctypes import c_ubyte
from datetime import datetime
from utils.common_utils import config_queue
from utils.can_singleton import CanBusSingleton
from can_interaction.message_decoder import MessageDecoder

config_queue: Queue[can.Message]


class Utilities:
    def __init__(self) -> None:
        self.bus = CanBusSingleton.get_instance()
        self.message_decoder = MessageDecoder()
        self.timeout = 2

    def message_sender(self, arbitration_id: int, data: list[int]) -> bool:
        """Sends message  on bus with given parameters"""
        message = can.Message(
            is_extended_id=False,
            arbitration_id=arbitration_id,
            dlc=8,
            data=(c_ubyte * 8)(*data),
        )
        try:
            self.bus.send(message)
            return True
        except Exception as e:
            print(f"Error sending config message: {e}")
            return False

    def confirm_response(
        self, confirmation_id: int, confirmation_response: int
    ) -> bool | can.Message:
        """Waits for confirmation from CAN."""
        curr = time.time()
        while time.time() - curr < self.timeout:
            try:
                message = config_queue.get(timeout=1)
            except Exception:
                continue
            if not message:
                continue
            if (
                message.arbitration_id == confirmation_id
                and message.data[0] == confirmation_response
            ):
                return message
        return False

    def read_message(self) -> None | can.Message:
        """Return message from config queue. None in case of error."""
        try:
            msg = config_queue.get(timeout=self.timeout)
            return msg
        except Exception:
            return None

    def rtc_sync(self) -> None:
        """Sends the current timestamp to the BMS to sync its clock."""
        now = datetime.now()
        # dd/mm/YY H:M:S
        dt_string = now.strftime("%H,%M,%S,%d,%m,%Y")
        list = dt_string.split(",")
        list2 = [0]
        for i in list:
            val = int(i)
            if val > 2000:
                val = val % 2000
            list2.append(val)
        list2.append(0)
        self.message_sender(0x701, [0xDB, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00])
        self.message_sender(0x702, list2)
        self.confirm_response(0x710, 0xDB)
        print("Timestamp Message Sent", end="\n")

    def send_soft_reset_request(self) -> None:
        """Sends a command to the BMS to soft reset it."""
        try:
            self.message_sender(0x701, [0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
            print("Soft reset sent")
        except Exception as e:
            print(f"Error sending soft reset : {e}")

    def set_ser_no(self, number: str, type: str) -> bool:
        """Sends BMS serial number to the BMS based on user Input and firmware version.
        If firmware is fo L18 series or a very old L19 series we do nothing here. Otherwise we send it
        together with HW config gotten from the BMS.

        Args:
            number (str): BMS serial number
            type (str): BMS/Battery

        Returns:
            bool: Success or failure in setting serial number.
        """
        if type == "Battery":
            return True
        currtime = time.time()
        packet1 = [0x00]
        packet2 = [0x01] + [ord(i) for i in number[4:11]]
        packet3 = [0x02] + [ord(i) for i in number[11:]]
        acq = False
        self.message_sender(0x701, [0xE3])
        while time.time() - currtime < 1:
            msg = self.read_message()
            if msg is not None and msg.arbitration_id == 0x711 and msg.data[0] == 0x00:
                packet1.extend(msg.data[1:4])
                packet1.extend([ord(i) for i in number[:4]])
                acq = True
                break
        if not acq:
            self.message_sender(0x701, [0xE3])
            currtime = time.time()
            while time.time() - currtime < 1:
                msg = self.read_message()
                if (
                    msg is not None
                    and msg.arbitration_id == 0x711
                    and msg.data[0] == 0x00
                ):
                    packet1.extend(msg.data[1:4])
                    packet1.extend([ord(i) for i in number[:4]])
                    acq = True
                    break
        if not acq:
            self.message_sender(0x701, [0xE3])
            currtime = time.time()
            while time.time() - currtime < 1:
                msg = self.read_message()
                if (
                    msg is not None
                    and msg.arbitration_id == 0x711
                    and msg.data[0] == 0x00
                ):
                    packet1.extend(msg.data[1:4])
                    packet1.extend([ord(i) for i in number[:4]])
                    acq = True
                    break
        if len(packet1) == 8:
            self.message_sender(0x701, [0xE4, 0x00, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00])
            self.message_sender(0x702, packet1)
            self.message_sender(0x702, packet2)
            self.message_sender(0x702, packet3)
            res = self.confirm_response(0x710, 0xE4)
            # self.message_sender(0x701, [0x03])
            if res:
                return True
            else:
                return False
        else:
            return True

    def convert_trace_to_csv(self, csv_path: str, trace_path: str) -> bool:
        """Convert a given trace file in to decoded csv file and export it.

        Args:
            csv_path (str): Path to export csv to.
            trace_path (str): Path to the input trace file.

        Returns:
            bool: Success or failure status.
        """
        try:
            trace_data = open(trace_path, "r").readlines()
            # print(csv_path)
            tr_name = trace_path.replace("\\", "\\\\")
            csvname = tr_name.split("\\")[-1][:-4]
            LogFilePath = csv_path + "\\" + csvname + ".csv"
            # print(LogFilePath)

            packet = {}
            packet_length_known = False
            packet_length = 999999999
            first_packet = False
            stored_rows = []

            for line in trace_data:
                try:
                    if (
                        len(line.split()) > 3
                        and line.split()[3] == "00000001"
                        and not packet_length_known
                    ):
                        line = line.split()
                        decoded_message = self.message_decoder.decode(
                            int(line[3], 16),
                            can.Message(data=[int(i, 16) for i in line[5:13]]),
                            tag_data=False,
                        )
                        if not first_packet:
                            first_packet = True
                            packet.update(decoded_message)
                        else:
                            first_packet = False
                            packet_length_known = True
                            packet_length = len(packet)
                            packet = decoded_message
                    elif first_packet:
                        line = line.split()
                        decoded_message = self.message_decoder.decode(
                            int(line[3], 16),
                            can.Message(data=[int(i, 16) for i in line[5:13]]),
                            tag_data=False,
                        )
                        if len(decoded_message) > 0:
                            packet.update(decoded_message)
                    elif packet_length_known:
                        line = line.split()
                        decoded_message = self.message_decoder.decode(
                            int(line[3], 16),
                            can.Message(data=[int(i, 16) for i in line[5:13]]),
                            tag_data=False,
                        )
                        if len(decoded_message) > 0:
                            packet.update(decoded_message)
                            if len(packet) == packet_length:
                                stored_rows.append(packet)
                                packet = {}
                except Exception:
                    pass
            csvfile = open(LogFilePath, "w+", newline="")
            writer = csv.DictWriter(csvfile, fieldnames=list(stored_rows[0].keys()))
            writer.writeheader()
            writer.writerows(stored_rows)
            csvfile.close()
            return True
        except Exception as e:
            print(e)
            return False

    def bootloader_info(
        self,
    ) -> dict | None:
        """This function requests bootloader information from the BMS, decodes it and returns the decoded data.

        Returns:
            dict|None: The dictionary containing decoded bootloader info if the function executes succesfully.
        """
        self.message_sender(0x701, [0xE9])
        res = self.confirm_response(0x710, 0xE9)
        if res:
            config_length = res.data[1] << 8 | res.data[2]
        else:
            return
        array = [0] * (config_length)
        try:
            start_time = datetime.now()
            message_number = 0
            while (datetime.now() - start_time).total_seconds() < self.timeout:
                message = self.read_message()
                if not message:
                    continue  # noqa: E701
                if message.arbitration_id == 0x711:
                    if message.data[0] == math.ceil(config_length / 7) - 1:
                        array[
                            7 * message.data[0] : 7 * message.data[0]
                            + config_length % 7
                        ] = message.data[1 : config_length % 7 + 1]
                    else:
                        array[7 * message.data[0] : 7 * message.data[0] + 7] = (
                            message.data[1:]
                        )
                    message_number += 1
                if 0 < config_length < 7 * message_number:
                    decoded_data = {
                        "Bootloader Version": "".join(chr(i) for i in array[4:7]),
                        "App Validity Status": "".join(chr(i) for i in array[8:10]),
                        "Backup Validity Status": "".join(chr(i) for i in array[12:14]),
                        "App lenth": array[19] << 24
                        | array[18] << 16
                        | array[17] << 8
                        | array[16],
                        "Backup lenth": array[23] << 24
                        | array[22] << 16
                        | array[21] << 8
                        | array[20],
                        "App or Backup": array[24],
                        "FW Erase Count": array[29] << 8 | array[28],
                        "FW Update Count": array[31] << 8 | array[30],
                        "Backup Update Count": array[33] << 8 | array[32],
                        "Backup to App Count": array[35] << 8 | array[34],
                    }
                    return decoded_data
        except Exception:
            return

    def adv_toggle(self, value):
        self.message_sender(0x701, [0xE5, 0, 3])
        self.message_sender(0x702, [0, 0, 3, value])
        return self.confirm_response(0x710, 0xE5)
