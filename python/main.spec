# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(
    ['main.py'],
    pathex=['pyenv\\Lib\\site-packages'],
    binaries=[],
    datas=[],
    hiddenimports=['intelhex', 'socketio', 'engineio', 'nest_asyncio', 'asyncio', 'can.interfaces.pcan', 'eventlet.hubs.epolls', 'eventlet.hubs.kqueue', 'eventlet.hubs.selects', 'dns.rdtypes.dnskeybase', 'dns.rdtypes.dsbase', 'dns.rdtypes.euibase', 'dns.rdtypes.mxbase', 'dns.rdtypes.nsbase', 'dns.rdtypes.svcbbase', 'dns.rdtypes.tlsabase', 'dns.rdtypes.txtbase', 'dns.rdtypes.IN', 'dns.rdtypes.ANY', 'dns.asyncbackend', 'dns.dnssec', 'dns.e164', 'dns.namedict', 'dns.tsigkeyring', 'dns.versioned', 'dns.asyncquery', 'dns.asyncresolver', 'engineio', 'socketio', 'queue', 'threading', 'engineio.async_gevent'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    [],
    name='main',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
