import socketio
import eventlet
from utittls import sio
eventlet.monkey_patch()
# Create a new Socket.IO server
# sio = socketio.Server()

socketio_server = sio

# Define an event handler for the 'message' event
@sio.event
def my_message(sid, data):
  print(sio.rooms(sid))
  print(f"Message from client {sid}: {data}")

@sio.event
def join_room(sid, room_name):
  sio.enter_room(sid, room_name)
  print(f"Client {sid} joined {room_name}")
  sio.emit('message', f"Client {sid} joined the room {room_name}", room=sid)

# Define an event handler for the 'connect' event
@sio.event
def connect(sid, environ):
  global socketio_server
  socketio_server = sio
  sio.emit('message', 'this is test message from server')
  print(f"Client connected: {sid}")

# Define an event handler for the 'disconnect' event
@sio.event
def disconnect(sid):
    print(f"Client disconnected: {sid}")

# Create a Socket.IO app with the server instance
socketio_app = socketio.WSGIApp(sio)

# Run the server on localhost, port 5000 using eventlet
if __name__ == '__main__':
    # Use eventlet to run the server
    eventlet.wsgi.server(eventlet.listen(('localhost', 56789)), socketio_app)
    sio.wait()
