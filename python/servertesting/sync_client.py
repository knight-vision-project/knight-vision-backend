import socketio
import eventlet
eventlet.monkey_patch()

# Create a new Socket.IO client
sio = socketio.Client()

# Define an event handler for the 'message' event
@sio.event
def message(data):
    print(f"Message from server: {data}")

# Connect to the Socket.IO server
sio.connect('http://127.0.0.1:5000')

# Join a room called 'room1'
# sio.emit('join_room', 'room1')

# Keep the client running
sio.wait()
