import socketio

# Create a new Socket.IO client
sio = socketio.Client()

# Define an event handler for the 'message' event
@sio.event
def message(data):
    print(f"Message from server: {data}")

# Connect to the Socket.IO server
sio.connect('http://127.0.0.1:56789')

# Join the same room as client 1
sio.emit('join_room', 'pyroom')

# sio.emit('message', 'gfda')

# # Keep the client running
# sio.wait()
