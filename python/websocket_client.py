# Can be used for testing connection to the server

import asyncio
import socketio

sio = socketio.AsyncClient()

@sio.event
async def connect():
  print('connection established')

@sio.event
async def my_message(data):
  print('message received with ', data)

@sio.event
async def disconnect():
  print('disconnected from server')

@sio.event
async def analytical_data(data):
  print(data, type(data))

@sio.event
async def chemistry_data(data):
  print(data,type(data))

async def main():
  await sio.connect('http://localhost:56789')
  # await sio.emit('message', {'response': 'my first message from client'})
  await sio.wait()

if __name__ == '__main__':
  # Connect through async client
  asyncio.run(main())