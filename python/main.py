import threading
from sync_websocket_server import start_sync_server

server = threading.Thread(target=start_sync_server)
server.start()

from utils.common_utils import (  # noqa: E402
    config_queue,
    message_queue,
    emulator_config_queue,
    analytical_data_queue,
    running,
    pause_event,
)
from can_interaction.message_listener import MessageListener  # noqa: E402
from can_interaction.event_handler import EventHandler  # noqa: E402
from can_interaction.message_decoder import parameters  # noqa: E402
from utils.can_singleton import CanBusSingleton  # noqa: E402

# async def on_startup(app):
#     print("Application started")
#     loop = asyncio.get_event_loop()
#     loop.call_later(1, after_startup)
#     if not is_bundled():
#         loop.call_later(1, start_emulator)


def after_startup() -> None:
    """Start Listening to CAN messages
    Current support is there only for PCAN hardware.
    Extendable by identifying new hardwares from `available_interfaces`
    """

    event_handler = EventHandler()
    message_listener = MessageListener(
        bus=CanBusSingleton.get_instance(connect=True),
        event_handler=event_handler,
        message_queue=message_queue,
        config_queue=config_queue,
        emulator_config_queue=emulator_config_queue,
        analytical_data_queue=analytical_data_queue,
        running=running,
        pause_event=pause_event,
        msg_list=list(parameters.keys()),
    )
    from utils.sync_client import start_client  # noqa:  E402

    message_listener.start()
    start_client()
    print("MessageListner thread is started")


## Async server
# app.on_startup.append(on_startup)
# start_async_server()

## Sync server
after_startup()
# start_emulator()
# print('all started')
