from utils.PCANBasic import *
import can
from ctypes import c_ubyte

class Bus:
    def __init__(self, channel="PCAN_USBBUS1", interface="pcan"):
        self.pcan = PCANBasic() 
        self.pcan.Initialize(PCAN_USBBUS1, PCAN_BAUD_500K, TPCANType(0), 0, 0)

    def recv(self,):
        error, msg, timeStamp = self.pcan.Read(PCAN_USBBUS1)
        message = can.Message(arbitration_id = msg.ID,
                              dlc = 8,
                              data = (c_ubyte*8)(*msg.DATA))
        return message
    
    def send(self, msg):
        message = TPCANMsg()
        message.ID      = msg.arbitration_id
        message.MSGTYPE = PCAN_MESSAGE_EXTENDED
        message.LEN     = 8
        message.DATA    = (c_ubyte*8)(*msg.data)
        self.pcan.Write(PCAN_USBBUS1, message)
        
    def shutdown(self):
        self.pcan.Uninitialize(PCAN_USBBUS1)