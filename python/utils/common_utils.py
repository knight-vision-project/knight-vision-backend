import sys
import time
import queue
from threading import Event
import socketio
from enum import Enum

# Socketio server objectk
sio = socketio.Client()
while True:
    try:
        sio.connect("http://127.0.0.1:56789", transports=["websocket"])
        sio.emit("join_room", "pyroom")
        break
    except Exception:
        time.sleep(3)
        pass

# Queues for message collections
message_queue = queue.Queue()
config_queue = queue.Queue(maxsize=10000)
emulator_config_queue = queue.Queue()
analytical_data_queue = queue.Queue()
logging_data_queue = queue.Queue()
running = Event()
pause_event = Event()
new_fw = Event()
ini = Event()
in_process = Event()
usb_can_a = Event()
ini.set()
new_fw.set()


def clear_queue(myque: queue.Queue) -> None:
    """Clears a queue by getting all items in it

    Args:
        myque (queue.Queue): Queue to be emptied
    """
    while not myque.empty():
        myque.get_nowait()


def is_bundled() -> bool:
    """Returns whether the app is running in production mode or developement mode

    Returns:
        bool: True of production mode else False
    """
    return getattr(sys, "frozen", False)  # and hasattr(sys, "_MEIPASS")


class ConfigSID(Enum):
    SET_BASE_CONFIG_IOT = 0x01
    SET_BASE_CONFIG_OLD = 0xE1
    SET_BASE_CONFIG = 0xE2
    GET_BASE_CONFIG = 0x02
    SOFT_RESET = 0x03
    GET_WARR_CONFIG = 0x04
    SET_WARR_CONFIG = 0x05
    SET_OCV_TABLE = 0xF0
    GET_OCV_TABLE = 0x70
    SET_CDETARING_TABLE = 0xF1
    GET_CDETARING_TABLE = 0x71
    SET_DDETARING_TABLE = 0xF2
    GET_DDETARING_TABLE = 0x72
    RTC_SYNC = 0x0A


get_chemistry_ids = ["GET_OCV_TABLE", "GET_CDETARING_TABLE", "GET_DDETARING_TABLE"]
set_chemistry_ids = ["SET_OCV_TABLE", "SET_CDETARING_TABLE", "SET_DDETARING_TABLE"]
