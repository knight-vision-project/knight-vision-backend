import can
import usb.core
import usb.util
import usb.backend.libusb1
from ctypes import c_ubyte
from utils.common_utils import usb_can_a, is_bundled
from utils.J2534_Driver import J2534API, CANFrame
from utils.usb_can_a import USB_CAN_A


# Handling single bus object for the CAN
class CanBusSingleton:
    _instance = None

    @classmethod
    def get_instance(cls, connect: bool = False):
        if cls._instance is None and connect:
            cls._instance = cls._initialize_can_bus()
        return cls._instance

    @classmethod
    def _initialize_can_bus(cls):
        backend = usb.backend.libusb1.get_backend(
            find_library=lambda x: "./resources/app/libusb-1.0.dll"
            if is_bundled()
            else "./libusb-1.0.dll"
        )
        devices = usb.core.find(find_all=True, backend=backend)

        if not devices:
            print("No USB devices found.")
            return

        for device in devices:
            if device.idVendor == 0xC72 and device.idProduct == 0xC:
                try:
                    bus = can.interface.Bus(
                        channel="PCAN_USBBUS1", interface="pcan", bitrate=500000
                    )
                    usb_can_a.clear()
                    return bus
                except Exception:
                    return None
            elif device.idVendor == 0x1A86:
                if device.idProduct == 0x7523:
                    try:
                        bus = USB_CAN_A()
                        usb_can_a.set()
                        return bus
                    except Exception:
                        return None
                elif device.idProduct == 0x5523:
                    try:
                        bus = can.interface.Bus(
                            channel=0, interface="canalystii", bitrate=500000
                        )
                        usb_can_a.clear()
                        return bus
                    except Exception:
                        return None
        return None

    @classmethod
    def shutdown(cls):
        if cls._instance:
            cls._instance.shutdown()
        cls._instance = None


class Sloki:
    def __init__(
        self,
    ) -> None:
        self.bus: J2534API
        raise ConnectionError()
        self.connect()

    def send(self, message: can.Message):
        canframe = CANFrame()
        canframe.CAN_ID = message.arbitration_id
        canframe.data = message.data
        canframe.DLC = message.dlc
        print("sent", canframe)
        self.reset()
        self.bus.SBusCanSendMgs(canframe)

    def connect(
        self,
    ):
        self.bus = J2534API()
        try:
            result = self.bus.SBusCanOpen()
            if result != 0:
                raise ConnectionError()
            self.bus.SBusCanConnect(J2534API.Protocol_ID.CAN.value, 500000)
            print(self.bus)
            self.reset()
        except Exception as e:
            print(e)
            raise ConnectionError()

    def shutdown(self):
        self.bus.SBusCanDisconnect()
        self.bus.SBusCanClose()

    def recv(self, timeout: float):
        status, recv = self.bus.SBusCanReadMgs(timeout=int(timeout * 1000))
        if status == 0:
            msg = can.Message(
                arbitration_id=recv.CAN_ID,
                dlc=recv.DLC,
                data=(c_ubyte * 8)(*recv.data),
            )
            print(recv)
            return msg
        else:
            pass
            # print(status)

    def reset(self):
        return self.bus.SBusCanClearRxMsg()
