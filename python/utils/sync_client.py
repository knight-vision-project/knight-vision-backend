import time

# import socketio
from threading import Thread, Event
from can_interaction.upstream_event_handler import UpstreamEventHandler
from utils.common_utils import pause_event, new_fw, sio, in_process
from utils.can_singleton import CanBusSingleton

in_process: Event
# from utils.common_utils import sbio as sio
# Create a new Socket.IO client
# sio = socketio.Client()

handler = UpstreamEventHandler()


def broadcast_to_frontend(self, handle: str, data) -> None:
    """Sends a message to frontend with specific handler

    Args:
        handler (str): Name of handler
        data (any): Message
    """
    sio.emit("message_rerouter", {"target": "jsroom", "handler": handle, "data": data})


# Define an event handler for the 'message' event
@sio.event
def message(data):
    pass
    # print(f"Message from server: {data}")


@sio.event
def get_config(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Get config requested: ", data)
    handler.handle_get_config()
    in_process.clear()


@sio.event
def set_config(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Set config requested: ")
    handler.handle_set_config(data)
    in_process.clear()


@sio.event
def get_warranty(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Get warranty requested: ")
    handler.handle_get_warranty()
    in_process.clear()


@sio.event
def export_warranty(data):
    print("Export warranty requested")
    handler.handle_export_warranty(data)


@sio.event
def update_firmware(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Firmware update requested", data)
    handler.handle_read_firmware_file(data)
    in_process.clear()


@sio.event
def get_chemistry_table(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print(
        "Get chemistry table requested",
    )
    handler.handle_get_chemistry_tables()
    in_process.clear()


@sio.event
def get_adv_chemistry_table(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print(
        "Get advanced chemistry table requested",
    )
    handler.handle_get_advanced_tables()
    in_process.clear()


@sio.event
def set_chemistry_tables(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Set chemistry table requested")
    handler.handle_set_chemistry_tables(data)
    in_process.clear()


@sio.event
def toggle_advanced_tables(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Advanced table toggle requested", data)
    handler.toggle_advanced_features(data)
    in_process.clear()


@sio.event
def set_adv_chemistry_tables(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Set advanced chemistry table requested")
    handler.handle_set_advanced_tables(data)
    in_process.clear()


@sio.event
def export_table(data):
    print("Export chemistry data requested")
    handler.handle_export_chemistry_tables(data)


@sio.event
def export_adv_table(data):
    print("Export advanced chemistry data requested")
    handler.handle_export_advanced_tables(data)


@sio.event
def import_table(data):
    print("Import chemistry data requested")
    handler.handle_import_chemistry_tables(data["path"])


@sio.event
def import_adv_table(data):
    print("Import advanced chemistry data requested")
    handler.handle_import_advanced_tables(data["path"])


@sio.event
def get_schema(data):
    print("get schema requested:", data)
    pause_event.set()
    time.sleep(0.1)
    handler.send_schema()
    # running.set()
    sio.emit(
        "message_rerouter",
        {"target": "jsroom", "handler": "ConnectionStatus", "data": "Connected"},
    )
    time.sleep(1)
    handler.on_new_fw_change()
    handler.handle_get_config(ini=True)
    print("\nMessageListner thread is started schema")
    if new_fw.is_set():
        time.sleep(0.1)
        handler.handle_rtc_sync()
        print("RTC_sync requested")


@sio.event
def login(data):
    print("Login requested ", data)
    handler.handle_auth(data)


@sio.event
def import_config(data):
    print("import config requested", data)
    handler.handle_import_config(data)


@sio.event
def export_config(data):
    print("export config requested")
    handler.handle_export_config(data)


@sio.event
def flash_extraction(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Flash data extraction requested")
    handler.handle_flash_extraction(data)
    in_process.clear()


@sio.event
def set_ser_no(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Set BMS serial number requested")
    handler.handle_ser_no(data)
    in_process.clear()


@sio.event
def get_bootloader_info(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    print("Bootloader information requested")
    handler.get_bootloader_info()
    in_process.clear()


@sio.event
def connect_CAN(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    global handler
    # MessageListener.connect()
    pause_event.set()
    # running.set()
    sio.emit(
        "message_rerouter",
        {"target": "jsroom", "handler": "ConnectionStatus", "data": "Connected"},
    )
    time.sleep(1)
    handler = UpstreamEventHandler()
    # UpstreamEventHandler.update_config()
    handler.handle_get_config(ini=True)
    time.sleep(0.2)
    handler.handle_rtc_sync()
    print("RTC_sync requested")
    print("\nMessageListner thread is started connect can")
    in_process.clear()


@sio.event
def disconnect_CAN(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    # MessageListener.connect()
    pause_event.clear()
    CanBusSingleton.shutdown()
    sio.emit(
        "message_rerouter",
        {"target": "jsroom", "handler": "ConnectionStatus", "data": "Disconnected"},
    )
    print("MessageListner thread is started after stopping")
    in_process.clear()


stop_thread = Event()


@sio.event
def logging(data):
    # print("logging state changed", data)
    global stop_thread
    if data[2] == "Start":
        if float(data[1]) <= 0.105:
            thread = Thread(
                target=handler.handle_logging, args=(data[0], stop_thread, 0.08)
            )
        else:
            thread = Thread(
                target=handler.handle_logging, args=(data[0], stop_thread, data[1])
            )
        stop_thread.clear()
        thread.start()
    elif data[2] == "Stop":
        stop_thread.set()


@sio.event
def rtc_sync(data):
    if in_process.is_set():
        print("Action blocked")
        return
    else:
        in_process.set()
    handler.handle_rtc_sync()
    print("RTC_sync requested")
    in_process.clear()


@sio.event
def remove_access(data):
    handler.erase_access()
    print("Remove access requested")


@sio.event
def convert_trace_to_csv(data):
    handler.convert_trace_to_csv(data[0], data[1])


@sio.event
def export_template(data):
    handler.template_export(data)


def start_client():
    sio.emit(
        "message_rerouter",
        {"target": "jsroom", "handler": "ConnectionStatus", "data": "Disconnected"},
    )

    time.sleep(0.1)

    sio.emit(
        "message_rerouter",
        {
            "target": "pyroom",
            "handler": "get_schema",
            "data": "hello get bmsschema",
        },
    )


# while True:
#     try:
#         sio.connect("http://127.0.0.1:56789")
#         sio.emit("join_room", "pyroom")
#         sio.emit(
#             "message_rerouter",
#             {"target": "jsroom", "handler": "ConnectionStatus", "data": "Disconnected"},
#         )
#         time.sleep(1)
#         sio.emit(
#             "message_rerouter",
#             {
#                 "target": "pyroom",
#                 "handler": "get_schema",
#                 "data": "hello get bmsschema",
#             },
#         )
#         break
#     except Exception:
#         pass
#     time.sleep(3)
