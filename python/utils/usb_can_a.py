import time
import struct
import serial
from can import Message
import serial.tools.list_ports


class USB_CAN_A:
    def __init__(self) -> None:
        """Initiating USB-CAN-A object which controls all receiving and transmitting functions."""
        self.bus = None
        self.connection()

    def connection(self) -> None:
        """This function iterates through all available ports and looks for one which
            matches the vendor ID and product ID for USB-CAN-A and makes the connection.

        Raises:
            ConnectionError: Raised when there are no serial ports connected.
            ConnectionError: Raised when there is an issue in making connection to the device.
        """
        ports = serial.tools.list_ports.comports()
        if not ports:
            print("No serial devices found.")
            raise ConnectionError()

        for port in ports:
            if port.vid == 0x1A86 and port.pid == 0x7523:
                try:
                    self.bus = serial.Serial(str(port.device), 2000000, timeout=1)
                except Exception:
                    raise ConnectionError()

    def recv(self, timeout: float) -> None:
        t_now = time.time()
        while time.time() - t_now < timeout:
            try:
                rx_byte = self.bus.read()
                # print(rx_byte)
                if rx_byte and ord(rx_byte) == 0xAA:
                    ss = self.bus.read()
                    s = ss[0]
                    # print(s)
                    extended, dlc = (s >> 4) & 0xF, s & 0xF
                    if extended == 0x0E:
                        is_extended = True
                    elif extended == 0x0C:
                        is_extended = False
                    timestamp = 0
                    if is_extended:
                        arb = self.bus.read(4)
                        arbitration_id = struct.unpack("<I", arb)[0]
                    else:
                        arb = self.bus.read(2)
                        arbitration_id = struct.unpack("<H", arb)[0]
                    data = self.bus.read(dlc)
                    delb = self.bus.read()
                    # print(hex(arbitration_id))
                    # delimiter_byte = ord(delb)
                    # if delimiter_byte == 0x55:
                    # received message data okay
                    msg = Message(
                        # TODO: We are only guessing that they are milliseconds
                        timestamp=timestamp,
                        arbitration_id=arbitration_id,
                        is_extended_id=is_extended,
                        dlc=dlc,
                        data=data,
                    )
                    if msg.arbitration_id in [0x710, 0x711]:
                        print(msg)
                        print(rx_byte + ss + arb + data + delb)
                    return msg
                    # else:
                    #     # print(rx_byte, s, arb, dlc, data, len(data), delimiter_byte)
                    #     return None
            except Exception as e:
                print(e)
                print(extended)
                return None
        return None

    def send(self, message: Message):
        # ser.write(b"\xaa\xe4\xe5P\xff\x13\x01\x00\x00\x00U")
        if message.arbitration_id >> 8 == 7:
            print(message)
        id_format = "<I" if message.is_extended_id else "<H"
        size_info = struct.pack(
            "B", (0x0E if message.is_extended_id else 0x0C) << 4 | message.dlc
        )
        # Pack CAN ID
        can_id = struct.pack(id_format, message.arbitration_id)
        data = bytes(message.data)
        self.bus.write(b"\xaa" + size_info + can_id + data + b"U")

    def reset(self):
        pass


if __name__ == "__main__":
    bus = USB_CAN_A()
    while True:
        bus.recv(timeout=1)
