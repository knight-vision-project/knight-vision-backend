import socketio
from flask import Flask
import logging

from engineio.payload import Payload

Payload.max_decode_packets = 2000

logging.getLogger("werkzeug").disabled = True
# import flask

# from eventlet import wsgi, listen
# Create a new Socket.IO server
# sio = socketio.Server()

# eventlet.monkey_patch()
sio = socketio.Server(cors_allowed_origins="*", async_mode="threading")
# socketio_app = socketio.WSGIApp(sio)

# socketio_server = sio


# Define an event handler for the 'message' event
@sio.event
def message_rerouter(sid, data: dict) -> None:
    """Main function used to reroute all messages coming from frontend to backend and vice-versa

    Args:
        sid (Any): SID
        data (dict): Dictionary containing the information to be rerouted and where to reroute it.
    """
    # if data[1] != 'broadcast_state':
    #   print(data[1])
    # if data["handler"] == "get_config":
    #     print("\n")
    #     print(data)
    try:
        sio.emit(data["handler"], data["data"], room=data["target"])
    except Exception:
        try:
            sio.emit(data["handler"], data["data"], room=data["target"])
        except Exception:
            pass
    # print(f"Message from client {sid}: {data}")


@sio.event
def join_room(sid: str | int, room_name: str) -> None:
    """Every client when joining server should join either pyroom or jsroom.

    Args:
        sid (str | int): SID of the client
        room_name (str): The name of the room they will join
    """
    sio.enter_room(sid, room_name)
    # print(f"Client {sid} joined {room_name}")
    sio.emit("message", f"Client {sid} joined the room {room_name}", room=sid)
    print("connected to ", room_name, sid)
    # sio.emit(
    #     "message_rerouter",
    #     {"target": "pyroom", "handler": "get_schema", "data": "hello get bmsconfig"},
    # )


# Define an event handler for the 'connect' event
@sio.event
def connect(sid, environ):
    global socketio_server
    socketio_server = sio
    sio.emit("message", "this is test message from server")


# print(f"Client connected: {sid}")


# Define an event handler for the 'disconnect' event
@sio.event
def disconnect(sid):
    print("")
    # print(f"Client disconnected: {sid}")


# Create a Socket.IO app with the server instance

# @sio.event
# def message_rerouter(sid, data):
#   print(data)
#   sio.emit(data[1], data[2], room=data[0])

app = Flask(__name__)
app.wsgi_app = socketio.WSGIApp(sio, app.wsgi_app)


def start_sync_server(host="localhost", port=56789):
    app.run(host=host, port=port)


#     eventlet.wsgi.server(eventlet.listen((host, port)), socketio_app)


# Run the server on localhost, port 5000 using eventlet
# if __name__ == '__main__':
#     # Use eventlet to run the server
#     eventlet.wsgi.server(eventlet.listen(('localhost', 56789)), socketio_app)
#     sio.wait()
